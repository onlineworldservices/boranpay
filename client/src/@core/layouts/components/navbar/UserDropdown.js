// ** React Imports
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import { authuserInfo } from '@components/service'
// ** Custom Components
import Avatar from '@components/avatar'

// ** Utils
import { isUserLoggedIn } from '@utils'

// ** Store & Actions
import { useDispatch } from 'react-redux'
import { handleLogout } from '@store/actions/auth'

// ** Third Party Components
import { UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import { User, HelpCircle, Power, Sun, Moon } from 'react-feather'

// ** Default Avatar Image
import defaultAvatar from '@src/assets/images/portrait/small/avatar-s-11.jpg'

const UserDropdown = props => {
  const [userSite] = useState(authuserInfo())
  // ** Store Vars
  const dispatch = useDispatch()

  // ** State
  const [userData, setUserData] = useState(null)

  //** ComponentDidMount
  useEffect(() => {
    if (isUserLoggedIn() !== null) {
      setUserData(JSON.parse(localStorage.getItem(process.env.REACT_APP_BASE_NAMETOKEN)))
    }
  }, [])

  // ** Props
  const { skin, setSkin } = props

  // ** Function to toggle Theme (Light/Dark)
  const ThemeToggler = () => {
    if (skin === 'dark') {
      return (
        <DropdownItem tag={'a'} onClick={() => setSkin('semi-dark')}>
          <Sun size={14} className='mr-75' />
          <span className='align-middle'>Theme</span>
        </DropdownItem>)
    } else {
      return (
        <DropdownItem tag={'a'} onClick={() => setSkin('dark')}>
          <Moon size={14} className='mr-75' />
          <span className='align-middle'>Theme</span>
        </DropdownItem>)
    }
  }

  //** Vars
  const userAvatar = (userSite && userSite.avatar) || defaultAvatar

  return (
    <UncontrolledDropdown tag='li' className='dropdown-user nav-item'>
      <DropdownToggle href='/' tag='a' className='nav-link dropdown-user-link' onClick={e => e.preventDefault()}>
        <div className='user-nav d-sm-flex d-none'>
          <span className='user-name font-weight-bold'>{(userSite && (`${userSite.firstName} ${userSite.lastName}`)) || 'Boclair Temgoua'}</span>
          <span className='user-status'>{(userSite && userSite.role) || 'Professional'}</span>
        </div>
        <Avatar img={userAvatar} imgHeight='40' imgWidth='40' status='online' />
      </DropdownToggle>
      <DropdownMenu right>
        <ThemeToggler />
        <DropdownItem tag={Link} to='/user/profile/'>
          <User size={14} className='mr-75' />
          <span className='align-middle'>Profil</span>
        </DropdownItem>
        {/**
         * 
        <DropdownItem tag={Link} to='/apps/email'>
          <Mail size={14} className='mr-75' />
          <span className='align-middle'>Inbox</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/apps/todo'>
          <CheckSquare size={14} className='mr-75' />
          <span className='align-middle'>Tasks</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/apps/chat'>
          <MessageSquare size={14} className='mr-75' />
          <span className='align-middle'>Chats</span>
        </DropdownItem>
         */}
        <DropdownItem divider />
        {/**
         * 
        <DropdownItem tag={Link} to='/pages/account-settings'>
          <Settings size={14} className='mr-75' />
          <span className='align-middle'>Settings</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/pages/pricing'>
          <CreditCard size={14} className='mr-75' />
          <span className='align-middle'>Pricing</span>
        </DropdownItem>
         */}
        <DropdownItem tag={Link} to='/faq/'>
          <HelpCircle size={14} className='mr-75' />
          <span className='align-middle'>FAQ</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/login' onClick={() => dispatch(handleLogout())}>
          <Power size={14} className='mr-75' />
          <span className='align-middle'>Logout</span>
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default UserDropdown
