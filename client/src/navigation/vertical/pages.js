import {
    PlusSquare,
    Users,
    Circle,
    Octagon,
    User
} from 'react-feather'
export default [
    {
        header: 'Découverte',
        action: 'read',
        resource: 'ACL'
    },
    {
        id: "services",
        title: "Services",
        icon: <Octagon size={20} />,
        action: 'read',
        resource: 'ACL',
        children: [
            {
                id: "donations",
                title: "Donations",
                icon: <Circle size={12} />,
                action: 'read',
                resource: 'ACL',
                navLink: "/donations/"
            },
            {
                id: "mycagnotes",
                title: "Cagnotes",
                icon: <Circle size={12} />,
                action: 'read',
                resource: 'ACL',
                navLink: "/mycagnotes/"
            },
            {
                id: "marketplaces",
                title: "Marketplaces",
                icon: <Circle size={12} />,
                action: 'read',
                resource: 'ACL',
                navLink: "/marketplaces/"
            }
        ]
    },
    {
        header: 'Sections',
        action: 'read',
        resource: 'ACL'
    },
    {
        id: "directories",
        title: "Répertoire",
        icon: <Users size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/directories/"
    },
    {
        id: "groupes",
        title: "Groupes",
        icon: <PlusSquare size={20} />,
        action: 'read',
        resource: 'ACL',
        navLink: "/groupes/"
    }
    /** 
    {
        id: "marketplaces",
        title: "Marketplace",
        icon: <ShoppingBag size={20} />,
        navLink: "/marketplaces/"
    }
    */
]
