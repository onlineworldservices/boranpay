import { Settings, Circle } from 'react-feather'

export default [

  {
    header: 'Intégration',
    action: 'read',
    resource: 'ACL'
  },
  {
    id: 'developpeur',
    title: 'Développeur',
    icon: <Settings size={20} />,
    action: 'read',
    resource: 'ACL',
    children: [
      {
        id: 'applications',
        title: 'Mes applications',
        icon: <Circle size={12} />,
        action: 'read',
        resource: 'ACL',
        navLink: '/developpeur/app/'
      },
      {
        id: 'sandbox',
        title: 'Sandbox',
        icon: <Circle size={12} />,
        action: 'read',
        resource: 'ACL',
        children: [
          {
            id: 'compte',
            title: 'Compte',
            action: 'read',
            resource: 'ACL',
            navLink: '/developpeur/accounts/'
          }
        ]
      }
    ]
  }
]