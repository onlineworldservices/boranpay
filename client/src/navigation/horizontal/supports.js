import {
    Circle,
    Info
  } from 'react-feather'
  export default [
    {
      id: "support",
      title: "Support",
      icon: <Info size={20} />,
      action: 'read',
      resource: 'ACL',
      children: [
        {
          id: "faq",
          title: "FAQ",
          icon: <Circle size={12} />,
          action: 'read',
          resource: 'ACL',
          navLink: "/faq/"
        },
        {
          id: "about",
          title: "A propos",
          icon: <Circle size={12} />,
          action: 'read',
          resource: 'ACL',
          navLink: "/about/"
        },
        {
          id: "contact",
          title: "Contact",
          icon: <Circle size={12} />,
          action: 'read',
          resource: 'ACL',
          navLink: "/contact/"
        }
      ]
    }
  ]
  