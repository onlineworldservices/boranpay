import React, { useState } from 'react'
import AuthContext from '@src/utility/context/authContext'
import jwt from 'jsonwebtoken'

// ** Get token
import { isUserLoggedIn } from '@utils'

// ** Router Import
import Router from './router/Router'

const App = props => {
    const [userData, setUserData] = useState(isUserLoggedIn())
    console.log('userData =================================', userData?.accessToken)
    const userSite = jwt.decode(userData?.accessToken) 
    return (
        <AuthContext.Provider value={{ userSite, setUserData}} >
            <Router />
        </AuthContext.Provider>
    )
}
export default App
