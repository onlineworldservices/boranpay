import {
    GET_SHOW_GROUPE,
    GET_GROUPES_USER,
    GET_SHOW_USERGROUPE,
    GET_USERGROUPES_USER,
    DELETE_USERGROUPE_USER,
    DELETE_GROUPE_USER,
    GET_SHOW_USERGROUPESDIRECTORY_USER
} from '../index'
import axios from 'axios'
import { authHeader } from '@components/service'
import Swal from 'sweetalert2'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

export const loadShowGroupUser = (slugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes/${slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_GROUPE,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowUserGroupUser = (slugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/ugs/${slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_USERGROUPE,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllGroupeUser = (props) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/groupe/u/${props.slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_GROUPES_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllUserDirectory = (props) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/ug_by_user/${props.slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_USERGROUPESDIRECTORY_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadAllUserGroupeUser = (slugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/ug/${slugin}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_USERGROUPES_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const deleteusergrpItem = (props) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: true
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/ugs/${props.slugin}`, { headers: authHeader() })
                .then(() => (

                    dispatch({
                        type: DELETE_USERGROUPE_USER,
                        payload: props.slugin
                    }),
                    toast.success(
                        <SuccessToast name={'Success'} description={'Contact supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                )).catch(error => console.error(error))
        }
    })
}

export const deletegroupeItem = (props) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes/${props.slugin}`, { headers: authHeader() })
                .then(() => (

                    dispatch({
                        type: DELETE_GROUPE_USER,
                        payload: props.slugin
                    }),
                    toast.success(
                        <SuccessToast name={'Success'} description={'Groupe supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                )).catch(error => console.error(error))
        }
    })
}