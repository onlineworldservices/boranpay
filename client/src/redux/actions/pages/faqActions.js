import {
    GET_ALL_FAQS,
} from "../index";
import axios from "axios"



export const loadfaqs = ()  => async (dispatch) => {

     await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL }/faqs`).then(response =>
        dispatch({
            type: GET_ALL_FAQS,
            payload: response.data
        })
    ).catch(error => console.error(error))
}