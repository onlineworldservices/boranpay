import {
    GET_RECLAMATIONS_USER,
    GET_SHOW_RECLAMATION,
    DELETE_RECLAMATION_USER
} from '../index'
import axios from 'axios'
import { authHeader } from '@components/service'
import Swal from 'sweetalert2'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'


export const loadReclamationUser = (user) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamation/u/${user}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_RECLAMATIONS_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadShowReclamationUser = (reclamation) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamations/${reclamation}`, { headers: authHeader() })
        .then((response) => (
            dispatch({
                type: GET_SHOW_RECLAMATION,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}
export const deleteItem = (props) => async (dispatch) => {

    Swal.fire({
        title: 'Suppression?',
        text: "Êtes-vous sûr de vouloir executer cette action?",
        confirmButtonText: 'Oui, supprimer',
        cancelButtonText: 'Non, annuler',
        buttonsStyling: false,
        confirmButtonClass: "btn btn-primary",
        cancelButtonClass: 'btn btn-outline-default',
        showCancelButton: true,
        reverseButtons: false
    }).then(async (result) => {
        if (result.value) {

            //Envoyer la requet au server
            await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/reclamations/${props.slugin}/`, { headers: authHeader() })
                .then(() => {

                    dispatch({
                        type: DELETE_RECLAMATION_USER,
                        payload: props.slugin
                    })

                    toast.success(
                        <SuccessToast name={'Success'} description={'Reclamation supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })

                }).catch(error => console.error(error))
        }
    })
}