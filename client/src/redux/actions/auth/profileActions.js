import {
    GET_DATA_PROFILE_USER,
    GET_DATA_PROFILE_USER_PUBLIC,
    GET_DATA_PROFILE_USER_PRIVATE
} from '../index'
import axios from 'axios'
import { authHeader } from '@components/service'

export const loadDataProfileUser = (props) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/users/${props.slugin}`, { headers: authHeader() })
        .then(response => (
            dispatch({
                type: GET_DATA_PROFILE_USER,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}

export const loadDataProfileUserPublic = (slugin) => {
    return async dispatch => {
        return await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/users/${slugin}`, { headers: authHeader() })
            .then(response => {
                dispatch({
                    type: GET_DATA_PROFILE_USER_PUBLIC,
                    payload: response.data
                })
            }).catch(error => console.error(error))
    }
}

export const loadDataProfileUserPrivate = (slugin) => async (dispatch) => {

    await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/userinfo/${slugin}`, { headers: authHeader() })
        .then(response => (
            dispatch({
                type: GET_DATA_PROFILE_USER_PRIVATE,
                payload: response.data
            }))

        ).catch(error => console.error(error))
}