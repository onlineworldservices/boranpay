import produce from 'immer'

const initialState = {
    transaction: { category: [], amount: [], user: { profile: { country: [] } }, userto: { profile: { country: [] } } },
    transactions: [],
    transactionsretraits: [],
    transactioncagnotes: [],
    transactionservices: [],
    transactiondonations: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_SHOW_TRANSACTION':
            draft.transaction = action.payload
            return
        case 'GET_SHOW_TRANSACTION_SERVICE':
            draft.transaction = action.payload
            return
        case 'GET_ALL_TRANSACTIONS_USER':
            draft.transactions = action.payload
            return
        case 'GET_ALL_TRANSACTION_USER_RETRAIT':
            draft.transactionsretraits = action.payload
            return
        case 'GET_ALL_TRANSACTIONS_CAGNOTES_USER':
            draft.transactioncagnotes = action.payload
            return
        case 'GET_ALL_TRANSACTIONS_SERVICES_USER':
            draft.transactionservices = action.payload
            return
        case 'GET_ALL_TRANSACTIONS_DONATIONS_USER':
            draft.transactiondonations = action.payload
            return
        case 'GET_ALL_TRANSACTION_USER_SEND':
            draft.transactions = action.payload
            return
        case 'GET_ALL_TRANSACTION_USER_SEND_USER':
            draft.transactions = action.payload
            return
        case 'GET_ALL_TRANSACTION_USER_RECEV':
            draft.transactions = action.payload
        default:
    }
},
    initialState
)
