import produce from "immer"

const initialState = {
    countries: [],
    payementmethods: [],
    payementmethodsusers: []
}

export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_ALL_COUNTRIES':
            draft.countries = action.payload
            return
        case 'GET_ALL_COUNTRIES_RETRAIT':
            draft.countries = action.payload
            return
        case 'GET_ALL_PAYEMENTMETHODSUSER_RETRAIT':
            draft.payementmethodsusers = action.payload
            return
        case 'GET_ALL_PAYEMENTMETHODS_RECHARGE':
            draft.payementmethods = action.payload
            return
        case 'GET_ALL_PAYEMENTMETHODS_RETRAIT':
            draft.payementmethods = action.payload
            return
        case 'DELETE_PAYEMENTMETHOD_USER':
            const datadelete = draft.payementmethodsusers.rows.findIndex(i => i.slugin === action.payload)
            if (datadelete !== -1) draft.payementmethodsusers.rows.splice(datadelete, 1)
            return draft
        default:
    }
},
    initialState
)
