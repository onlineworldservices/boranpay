import produce from "immer"

const initialState = {
    donation: { user: { profile: { currency: [] } }, contributes: [], amountdonations: [] },
    donations: [],
    contributes: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_DONATIONS_USER':
            draft.donations = action.payload
            return
        case 'GET_SHOW_DONATION':
            draft.donation = action.payload
            return
        case 'GET_SHOW_DONATION_USER':
            draft.donation = action.payload
            return
        case 'GET_SHOW_DONATION_CONTRIBUTES':
            draft.contributes = action.payload
            return
        case 'DELETE_DONATION_USER':
            const datadelete = draft.donations.findIndex(i => i.slugin === action.payload)
            if (datadelete !== -1) draft.donations.splice(datadelete, 1)
            return draft
        default:
    }
},
    initialState
)
