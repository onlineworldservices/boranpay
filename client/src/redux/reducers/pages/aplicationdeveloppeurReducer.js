import produce from "immer"

const initialState = {
    applications: [],
    application: { transactionservices: { amount: [] }, developeruser: [], applicationsecretekey: [], amountservices: [], user: { profile: { currency: [] } } }
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_ALL_APPLICATIONS_USER':
            draft.applications = action.payload
            return
        case 'GET_SHOW_APPLICATIONS_USER':
            draft.application = action.payload
            return
        case 'CHANGE_UNACTIVE_STATUS_APPLICATION':
            const datatiresove = draft.applications.findIndex(i => i.slugin === action.payload)
            if (datatiresove !== -1) draft.applications[datatiresove].statusApplication = !action.payload
            return draft
        case 'CHANGE_ACTIVE_STATUS_APPLICATION':
            const dataresove = draft.applications.findIndex(i => i.slugin === action.payload)
            if (dataresove !== -1) draft.applications[dataresove].statusApplication = action.payload
            return draft
        case 'DELETE_APPLICATIONS_USER':
            const datadelete = draft.applications.findIndex(i => i.slugin === action.payload)
            if (datadelete !== -1) draft.applications.splice(datadelete, 1)
            return draft
        default:
    }
},
    initialState
)
