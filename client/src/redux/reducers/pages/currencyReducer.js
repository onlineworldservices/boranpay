import produce from "immer"

const initialState = {
    currencies: [],
    abilitipermissions: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_ALL_CURRENCIES':
            draft.currencies = action.payload
            return
        case 'GET_ALL_ABILITY_FORUSER_PERMISSIONS':
            draft.abilitipermissions = action.payload
        default:
    }
},
    initialState
)
