import produce from "immer"

const initialState = {
    dataItem: { transactions: [], transactioncagnoterecevs: [], transactionsends: [], transactionrecevs: [], tasks: [], profile: { currency: [] }, amountusers: [], amountcagnotes: [], amountservices: [], amountdonations: [], amountservices: [] },
    transactions: [],
    user: []
}


export default produce((draft, action = {}) => {
    switch (action.type) {
        case 'GET_DATA_PROFILE_USER':
            draft.dataItem = action.payload
            return
        case 'GET_DATA_PROFILE_USER_PRIVATE':
            draft.user = action.payload
            return
        case 'GET_DATA_PROFILE_USER_PUBLIC':
            draft.user = action.payload
        default:
    }
},
    initialState
)