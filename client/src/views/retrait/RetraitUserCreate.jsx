import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import RetraitUserForm from './inc/RetraitUserForm'

const RetraitUserCreate = () => {
    const [userSite] = useState(authuserInfo())
    return (
        <>
            <HelmetSite title={`Nouveau retrait`} />
            <Breadcrumbs
                breadCrumbTitle="Retrait"
                breadCrumbParent="Funtion"
                breadCrumbActive="Nouveau retrait"
            />
            <VerificationInfoUser userItem={userSite} />
            <UndoRedo />

            <RetraitUserForm userSite={userSite} />
        </>
    )
}

export default RetraitUserCreate