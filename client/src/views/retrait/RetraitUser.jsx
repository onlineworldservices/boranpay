import React, { useEffect, useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import { Row, Col, Button } from 'reactstrap'
import { Plus } from 'react-feather'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadShowTransactionUserRetrait } from '../../redux/actions/pages/transactionAction'
import RetraitTable from './inc/RetraitTable'

const RetraitUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())
    const retraits = useSelector(state => state?.transactions?.transactionsretraits)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowTransactionUserRetrait(userSite.slugin))
            window.scroll({ top: 0, left: 0, behavior: 'smooth' })
        }
        loadItems()
    }, [userSite.slugin])

    return (
        <>
            <HelmetSite title={`Transactions retrait - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Retraits"
                breadCrumbParent="Funtion"
                breadCrumbActive={`Retraits pour - ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/retraits/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouveau retrait</b>
            </Button.Ripple>
            <RetraitTable retraits={retraits} />
        </>
    )
}

export default RetraitUser