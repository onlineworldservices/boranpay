import React, { useState, useEffect } from 'react'
import {
  Card,
  CardBody,
  Alert,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { Link, useHistory } from 'react-router-dom'
import { authHeader } from '@components/service'
import { useDispatch, useSelector } from 'react-redux'
import userServices from '../../../services/userServices'
import { loadAllPayementmethodsuserretrait } from '../../../redux/actions/pages/countryAction'
import Swal from 'sweetalert2'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const RetraitUserForm = (props) => {
  const initialItemState = {
    payementmethodId: '',
    total: '',
    password: ''
  }
  const { userSite } = props
  const history = useHistory()
  const [errormessage, setErrormessage] = useState('')
  /** Get countries with redux */
  const [statusSend, setStatusSend] = useState(false)
  const [currentItem, setCurrentItem] = useState(initialItemState)

  const payementmethodsusers = useSelector(state => state?.countries?.payementmethodsusers.rows)
  const dispatch = useDispatch()

  const handleInputChange = e => {
    const { name, value } = e.target
    setCurrentItem({ ...currentItem, [name]: value })
  }

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllPayementmethodsuserretrait(userSite))
    }
    loadItems()
  }, [userSite])

  const toggleStatus = () => { setStatusSend(!statusSend) }

  const handleSubmit = async (e) => {
    e.preventDefault()
    const { payementmethodId, total, password } = currentItem
    const data = { payementmethodId, total, password }

    Swal.fire({
      title: 'Confirmez-vous le retrait ?',
      text: "Êtes-vous sûr de vouloir executer cette action?",
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Non, annuler',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-primary",
      cancelButtonClass: 'btn btn-outline-default',
      showCancelButton: true,
      reverseButtons: false,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return userServices.userCheckValidate(userSite, data)
          .then(async () => {

            try {
              const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/retraits`, data, { headers: authHeader() })
              if (response) {
                toast.success(
                  <SuccessToast name={'Success'} description={'Retrait éffectué avec succès veuillez patienter 2 jours ouvrable'} />, {
                  position: toast.POSITION.TOP_RIGHT,
                  hideProgressBar: true
                })
                history.push('/retraits/')
              }
            } catch (error) {
              toast.error(
                <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
              })
              setErrormessage(error.response.data.message)
            }

          }).catch(error => {
            Swal.showValidationMessage(`${error.response.data.message}`)
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }

  return (
    <Card>
      <CardBody>
        <div className="col-md-10 mx-auto">
          <div className="tasks-info">
            <h3 className="mb-75">
              <strong>{`Nouveau retrait`}</strong>
            </h3>
          </div>
          <Form className="mt-2" onSubmit={handleSubmit}>

            {errormessage && (
              <Alert color='danger'>
                <div className='alert-body text-center'>
                  <AlertCircle size={15} />{' '}
                  <span className='ml-1'>
                    {errormessage}
                  </span>
                </div>
              </Alert>
            )}

            <br />
            <Row>
              <Col md="4" sm="12">
                <FormGroup>
                  <Label><b>Montant de votre retrait</b></Label>
                  <input className={`form-control`}
                    type="number"
                    pattern="[0-9]*"
                    inputMode="numeric"
                    min="1" step="1"
                    id="total"
                    name="total"
                    value={currentItem.total}
                    onChange={handleInputChange}
                    placeholder="Montant de votre retrait"
                    autoComplete="false"
                    required
                  />
                  <small ><Label><Link to={`/reception_contact/`} className="text-right">En savoir plus sur l'<strong>identifiant de réception</strong></Link></Label></small>
                </FormGroup>
              </Col>
              <Col md="8" sm="12">
                <FormGroup>
                  <Label ><b>Selectioner un moyen de retrait</b></Label>
                  <select className={`form-control`}
                    name="payementmethodId"
                    value={currentItem.payementmethodId}
                    onChange={handleInputChange}
                    id="payementmethodId" required>
                    <option value="" disabled>Choisissez le type de payement</option>
                    {payementmethodsusers?.length && (
                      payementmethodsusers.map(item => (
                        <option key={item.id} value={item.id}>{item.payementmethod.name} - {item.contactRecev} - {item.fullName}</option>
                      ))
                    )}
                  </select>
                  <small ><Label><Link to={`/mypayementmethods/new/`} className="text-right">Ajouter un <strong>nouveau moyen de retrait</strong></Link></Label></small>
                </FormGroup>
              </Col>
              {statusSend && (
                <Col sm={12}>
                  <FormGroup>
                    <Label for="password"><strong>Veuillez saisir votre mot de passe</strong></Label>
                    <input className={`form-control`}
                      type="password"
                      id="password"
                      name="password"
                      value={currentItem.password}
                      onChange={handleInputChange}
                      placeholder="Mot de passe pour valider la transaction"
                      autoComplete="off"
                      required />
                  </FormGroup>
                </Col>
              )}
              <div className="col-md-12 text-center">
                <FormGroup className="form-label-group">
                  {statusSend ? (
                    <Button.Ripple
                      type="submit"
                      className="btn-block"
                      color="primary"
                      block
                    >
                      Terminer
                    </Button.Ripple>
                  ) : (
                    <Button.Ripple
                      className="btn-block"
                      color="primary"
                      onClick={toggleStatus}
                      block
                    >
                      Continuer
                    </Button.Ripple>
                  )}
                </FormGroup>
              </div>
              <br />
              <div className="col-md-12 text-center">
                <Link to="/retraits/" ><b>Annuler</b></Link>
              </div>
              <div className="col-md-12 text-center">
                Prenez soin de bien renseigner les champs <strong>moyen de retrait</strong> et <strong>l'identifiant
                  de réception</strong> car vous disposez de 24hr uniquement pour les modifiers
              </div>

            </Row>
          </Form>
        </div>
      </CardBody>
    </Card >
  )
}
export default RetraitUserForm
