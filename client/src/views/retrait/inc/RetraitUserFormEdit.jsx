import React, { useState, useEffect } from 'react'
import {
  Card,
  CardBody,
  Alert,
  FormGroup,
  CardHeader,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { Link, useHistory, useParams } from 'react-router-dom'
import { authHeader } from '@components/service'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCountries, loadAllPayementmethodsretrait } from '../../../redux/actions/pages/countryAction'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  // payementmethodId: yup.number().required().positive().integer(),
  contactRecev: yup.string().required().min(3).max(200)
})

const RetraitUserFormEdit = () => {
  const { retrait } = useParams()
  const history = useHistory()
  const [errormessage, setErrormessage] = useState('')
  const {
    register,
    handleSubmit,
    setValue,
    formState
  } = useForm({ resolver: yupResolver(schema), mode: "onChange" })
  const { errors, isSubmitting } = formState
  /** Get countries with redux */
  const [statusSend, setStatusSend] = useState(false)
  const [continent, setContinent] = useState('')
  const [country, setCountry] = useState('')
  const [payementmethodId, setPayementmethodId] = useState('')
  //const continents = ["Africa", "America", "Asia", "Europe", "Oceania"]
  const continents = ['Selectioner le continent', 'Africa', 'Europe']
  const countries = useSelector(state => state?.countries?.countries)
  const payementmethods = useSelector(state => state?.countries?.payementmethods)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllCountries())
      await dispatch(loadAllPayementmethodsretrait())
    }
    loadItems()
  }, [])

  useEffect(() => {
    const loadItems = async () => {
      await axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/retraits/${retrait}`, { headers: authHeader() })
        .then(retraitItem => {
          const fields = ['contactRecev', 'payementmethodId']
          fields.forEach(field => setValue(field, retraitItem.data[field]))
        })
    }
    loadItems()
  }, [retrait])

  const onSubmit = async (data) => {
    await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/retraits/${retrait}`, data, { headers: authHeader() })
      .then((res) => {
        toast.success(
          <SuccessToast name={'Success'} description={'Données du retrait éditées avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        history.push('/retraits/')
      }).catch((error) => {
        toast.error(
          <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        setErrormessage(error.response.data.message)
      })
  }

  return (
    <Card>
      <CardBody>
        <div className="col-md-10 mx-auto">
          <div className="tasks-info">
            <h3 className="mb-75">
              <strong>{`Edition du retrait`}</strong>
            </h3>
          </div>
          <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

            {errormessage && (
              <Alert color='danger'>
                <div className='alert-body text-center'>
                  <AlertCircle size={15} />{' '}
                  <span className='ml-1'>
                    {errormessage}
                  </span>
                </div>
              </Alert>
            )}

            <br />
            <FormGroup>
              <Label><b>Continent</b></Label>
              <select className={`form-control`} value={continent}
                onChange={(e) => setContinent(e.target.value)} required>
                {continents.map((item) => (
                  <option key={item} value={item}>{item}</option>
                ))}
              </select>
            </FormGroup>
            <Row>


            {continent && (
                <Col md={`${country}` ? '7' : '12'} sm={`${country}` ? '7' : '12'}>
                  <FormGroup>
                    <Label><b>Pays</b></Label>
                    <select className={`form-control`} name='country' id='country'
                      value={country}
                      onChange={e => setCountry(e.target.value)} >
                      <option value="" disabled>Choisissez le pays</option>
                      {countries.filter(item => !continent || item.region.includes(continent))
                        .map((item) => (
                          <option key={item.id} value={item.name} >{item.name}</option>
                        ))}
                    </select>
                  </FormGroup>
                </Col>
              )}

              {country && (
                <Col md="5" sm="5">
                  <FormGroup>
                    <Label><b>Moyen de retrait</b></Label>
                    <select className={`form-control`}
                      {...register('payementmethodId')}
                      value={payementmethodId}
                      onChange={(e) => setPayementmethodId(e.target.value)}
                      id="payementmethodId" required>
                      <option value="" disabled>Choisissez le moyen de retrait</option>
                      {payementmethods.filter(item => !country || item.country.name.includes(country))
                        .map((item) => (
                          <option key={item.id} value={item.id} >{item.name}</option>
                        ))}
                    </select>
                  </FormGroup>
                </Col>
              )}

              <Col sm="12">
                <FormGroup>
                  <Label><b>Identifiant de réception</b></Label>
                  <input className={`form-control ${errors.contactRecev ? 'is-invalid' : ''}`}
                    type="text"
                    id="contactRecev"
                    placeholder="Numéro de Tél, email, Iban, Nom complet ..."
                    {...register('contactRecev')}
                    autoComplete="off"
                    required
                  />
                  <small ><Label><Link to={`/ugs/`} className="text-right">En savoir plus sur l'<strong>identifiant de réception</strong> pour plus d'informations</Link></Label></small>
                  <span className='invalid-feedback'>
                    <strong>{errors.contactRecev?.message}</strong>
                  </span>
                </FormGroup>
              </Col>

              <div className="col-md-12 text-center">
                <FormGroup className="form-label-group">
                  <Button.Ripple
                    disabled={isSubmitting}
                    type="submit"
                    className="btn-block"
                    color="primary"
                    block
                  >
                    Terminer
                      </Button.Ripple>
                </FormGroup>
              </div>
              <br />
              <div className="col-md-12 text-center">
                <Link to="/retraits/" ><b>Annuler</b></Link>
              </div>
              <div className="col-md-12 text-center">
                S'il vous plait donner des informations juste avant de confirmer le retrait
                  </div>
            </Row>
          </Form>
        </div>
      </CardBody>
    </Card>
  )
}
export default RetraitUserFormEdit
