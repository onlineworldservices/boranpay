import React, { useEffect, Fragment } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Card, CardBody } from 'reactstrap'
import { useHistory } from 'react-router-dom'
import { loadDataProfileUser } from '../../../redux/actions/auth/profileActions'

const SoldsCagnoteCard = (props) => {
  const history = useHistory()
  const user = useSelector(state => state.profile.dataItem)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadDataProfileUser(props))
    }
    loadItems()
  }, [props])

  const currencyUser = user.profile.currency.symbol
  return (
    <Card className="bg-analytics">
      <CardBody className="text-center">
        <div className="d-flex justify-content-between">
          <h4 className="text-dark"><b>Compte cagnotes</b></h4>
          <h4 className="text-dark"><b>Collectés</b></h4>
        </div>
        <br />
        <div className="text-center">
          <h1 className='font-weight-bolder mb-0'>
            {user.amountcagnotes.length > 0 ? <>
              <p className="text-success" >{user.amountcagnotes.map((lk, index) => (<Fragment key={index} >+ {(lk.totalAmountcagnote * user.profile.currency.currencyNumber).formatMoney(2, '.', ',')} </Fragment>))}
                {currencyUser}</p></> : <>0,00 {currencyUser}</>}
          </h1>
        </div>
        <div className="text-center mt-2">
          <Button.Ripple
            onClick={() => history.push('/virement_cagnotes/new/')}
            className="mr-1 mb-1"
            color="primary"
          >
            Virement
          </Button.Ripple>
          <Button.Ripple
            onClick={() => history.push('/transaction/cagnotes/')}
            className="mr-1 mb-1"
            color="info"
            outline
          >
            Transactions
          </Button.Ripple>
        </div>
      </CardBody>
    </Card>
  )

}
export default SoldsCagnoteCard