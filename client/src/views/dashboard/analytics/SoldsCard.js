import React, { useEffect, Fragment } from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Card, CardBody } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { loadDataProfileUser } from '../../../redux/actions/auth/profileActions'

const SoldsCard = (props) => {
  const history = useHistory()
  const user = useSelector(state => state?.profile?.dataItem)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadDataProfileUser(props))
    }
    loadItems()
  }, [props])

  const currencyUser = user?.profile?.currency?.symbol
  return (
    <Card className="bg-analytics">
      <CardBody className="text-center">
        <div className="d-flex justify-content-between">
          <h4 className="text-dark"><b>Compte principal</b></h4>
          <h4 className="text-dark"><b>Mon solde</b></h4>
        </div>
        <br />
        <div className="text-center">
          <h1 className='font-weight-bolder mb-0'>
            {user.amountusers.length > 0 ? <>
              <p className="text-dark" >{user.amountusers.map((lk, index) => (<Fragment key={index} >{(lk.totalAmountuser * user.profile.currency.currencyNumber).formatMoney(2, '.', ',')} </Fragment>))}
                {currencyUser}</p></> : <>0,00 {currencyUser}</>}
          </h1>
        </div>
        <div className="text-center mt-2">
          <Button.Ripple
            onClick={() => history.push("/transfert/new/")}
            className="mr-1 mb-1"
            color="primary"
          >
            Transferts
          </Button.Ripple>
          <Button.Ripple
            onClick={() => history.push("/retraits/new/")}
            className="mr-1 mb-1"
            color="info"
          >
            Retraits
          </Button.Ripple>
          <Button.Ripple
            onClick={() => history.push("/recharge/")}
            className="mr-1 mb-1"
            color="success"
          >
            Recharges
          </Button.Ripple>
        </div>
      </CardBody>
    </Card>
  )
}

export default SoldsCard