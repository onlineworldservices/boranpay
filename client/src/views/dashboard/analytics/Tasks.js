import React from "react"
import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Button
} from "reactstrap"
import { Check, Edit2, Trash } from "react-feather"
import { Plus } from "react-feather"
import { Link } from "react-router-dom"

class Tasks extends React.Component {
  state = {
    isVisible: false
  }
  handleHover = () => {
    this.setState({
      isVisible: !this.state.isVisible
    })
  }
  render() {
    return (
      <Card>
        <CardHeader className="border-bottom pb-1">
          <div className="tasks-info">
            <p className="mb-75">
              <strong>Last Tasks </strong>
            </p>
          </div>
        <Button.Ripple className="mr-1 mb-1" color="primary">
              <Plus size={14} />
        </Button.Ripple>
        </CardHeader>
        <ListGroup className="analytics-list">
          <ListGroupItem
            className="d-lg-flex justify-content-between align-items-center py-1 border-0"
            onMouseEnter={this.handleHover}
            onMouseLeave={this.handleHover}
          >
            <div className="float-left">
              <p className="text-bold-600 font-medium-2 mb-0 mt-25">
                Refactor button 
              </p>
              <small>16 Feb 2020 - 2 hrs</small>
            </div>
            <div className="float-right">
              <Button
                color="primary"
                outline
                className="btn-icon rounded-circle mr-50"
                size="sm"
              >
                <Check />
              </Button>
              <Button
                color="primary"
                outline
                className="btn-icon rounded-circle mr-50"
                size="sm"
              >
                <Edit2 />
              </Button>
              <Button
                color="primary"
                outline
                className="btn-icon rounded-circle mr-50"
                size="sm"
              >
                <Trash />
              </Button>
            </div>
          </ListGroupItem>
          <ListGroupItem
            className="d-lg-flex justify-content-between align-items-center py-1 border-0"
            onMouseEnter={this.handleHover}
            onMouseLeave={this.handleHover}
          >
            <div className="float-left">
              <p className="text-bold-600 font-medium-2 mb-0 mt-25">
                Submit report
              </p>
              <small>16 Feb 2020 - 2 hrs</small>
            </div>
            <div className="float-right">
              <Button
                color="primary"
                outline
                className="btn-icon rounded-circle mr-50"
                size="sm"
              >
                <Check />
              </Button>
              <Button
                color="primary"
                outline
                className="btn-icon rounded-circle mr-50"
                size="sm"
              >
                <Edit2 />
              </Button>
              <Button
                color="primary"
                outline
                className="btn-icon rounded-circle mr-50"
                size="sm"
              >
                <Trash />
              </Button>
            </div>
          </ListGroupItem>

          <Link to="/tasks/" className="text-center" >
            <p><b>See more</b></p>
          </Link>
        </ListGroup>
      </Card>
    )
  }
}
export default Tasks
