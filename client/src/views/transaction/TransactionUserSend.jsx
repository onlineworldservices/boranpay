import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { ArrowUp, ArrowDown, ArrowRight } from 'react-feather'
import { Card, Col, CardHeader, Button, Row } from 'reactstrap'
import { useHistory, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadShowTransactionUserSend } from "../../redux/actions/pages/transactionAction"
import TransactionTable from './inc/TransactionTable'

const TransactionUserSend = () => {
    const [userSite] = useState(authuserInfo())
    const history = useHistory()
    const { user } = useParams()
    const items = useSelector(state => state?.transactions?.transactions)
    const dispatch = useDispatch()
  
  
    useEffect(() => {
      const loadItems = async () => {
        if (userSite.slugin === user) {
          await dispatch(loadShowTransactionUserSend(user))
        }
      }
      loadItems()
    }, [user])

    return (
        <>
            <HelmetSite title={"Movements sortants"} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent="Pages"
                breadCrumbActive="Transactions sortants"
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/transfert/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <b>Nouveau transfert</b> <ArrowRight size={14} />
            </Button.Ripple>
            <Card>
                <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                    <div className="tasks-info">
                        <h4 className="mb-75">
                            <strong>Transactions  sortants</strong>
                        </h4>
                    </div>
                    <Row>
                        <Button.Ripple onClick={() => history.push(`/transaction/recev/${userSite.slugin}/`)} className="mr-1 mb-1" color="success" size="sm">
                            <ArrowUp size={14} />
                        </Button.Ripple>
                        <Button.Ripple onClick={() => history.push(`/transaction/send/${userSite.slugin}/`)} className="mr-1 mb-1" color="danger" size="sm">
                            <ArrowDown size={14} />
                        </Button.Ripple>
                    </Row>
                </CardHeader>
                <TransactionTable items={items} />
            </Card>
        </>
    )
}

export default TransactionUserSend