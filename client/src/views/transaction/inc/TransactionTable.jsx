import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { ChevronDown } from 'react-feather'
import ReactPaginate from 'react-paginate'
import { CardBody } from 'reactstrap'
import DataTable from 'react-data-table-component'
import moment from 'moment'
import Avatar from '@components/avatar'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.userto?.avatar) {
        return <Avatar className='mr-50' img={item?.userto?.avatar || 'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={`${item?.userto?.firstName} ${item?.userto?.lastName}`} initials />
    }
}
const TransactionTable = ({ items }) => {
    const [currentPage, setCurrentPage] = useState(0)

    const columns = [
        {
            name: <h5>Profile</h5>,
            selector: "profile_user",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Amount</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Motif</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 3
        }
    ]

    const data = items?.length >= 0 && (
        items.map(item => (
            {
                profile_user: (
                    <>
                        <div className='d-flex justify-content-left align-items-center'>
                            {renderClient(item)}
                            <div className='d-flex flex-column'>
                                <h6 className='user-name text-truncate mb-0'>{item?.userto?.firstName} {item?.userto?.lastName}</h6>
                                <small className='text-truncate text-muted mb-0'>{item?.userto?.email}</small>
                            </div>
                        </div>
                    </>
                ),
                price_transaction: <Link to={`/transactions/${item.slugin}/`}><h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {(item.amount.total).formatMoney(2, '.', ',')}</> : <>- {(item.amount.total).formatMoney(2, '.', ',')}</>} <small>{item.amount.currency}</small></h4></Link>,
                date: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>,
                title_transaction: <Link to={`/transactions/${item.slugin}/`} className="text-dark"><b>{item.title}</b></Link>
            }
        ))
    )

    // ** Function to handle Pagination
    const handlePagination = page => {
        setCurrentPage(page.selected)
    }
    // ** Custom Pagination
    const CustomPagination = () => (
        <ReactPaginate
            previousLabel=''
            nextLabel=''
            forcePage={currentPage}
            onPageChange={page => handlePagination(page)}
            pageCount={data.length / 7 || 1}
            breakLabel='...'
            pageRangeDisplayed={2}
            marginPagesDisplayed={2}
            activeClassName='active'
            pageClassName='page-item'
            breakClassName='page-item'
            breakLinkClassName='page-link'
            nextLinkClassName='page-link'
            nextClassName='page-item next'
            previousClassName='page-item prev'
            previousLinkClassName='page-link'
            pageLinkClassName='page-link'
            breakClassName='page-item'
            breakLinkClassName='page-link'
            containerClassName='pagination react-paginate separated-pagination pagination-sm justify-content-end pr-1 mt-1'
        />
    )
    return (
        <CardBody>
            <br />
            <DataTable
                noHeader
                pagination
                columns={columns}
                defaultSortField="createdAt"
                defaultSortAsc={false}
                paginationPerPage={7}
                className='react-dataTable'
                sortIcon={<ChevronDown size={10} />}
                paginationDefaultPage={currentPage + 1}
                paginationComponent={CustomPagination}
                data={data}
            />
        </CardBody>
    )
}

export default TransactionTable

