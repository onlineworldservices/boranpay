import { Card, CardBody, CardHeader } from 'reactstrap'
import DataTable from 'react-data-table-component'
import moment from 'moment'

const TransactionCagnoteTable = ({ items }) => {

    const columns = [
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Motif | Identifiant</h5>,
            selector: "title_transaction",
            sortable: true,
            grow: 2
        }

    ]

    const data = items?.length >= 0 ? (
        items.map(item => (
            {
                date: <b className="text-dark">{moment(item.createdAt).format('ll')}</b>,
                price_transaction: <h4 className={`${item.statusSend ? "text-success" : "text-danger"}`}>{item.statusSend ? <>+ {(item.amount.total).formatMoney(2, '.', ',')}</> : <>- {(item.amount.total).formatMoney(2, '.', ',')}</>} <small>{item.amount.currency}</small></h4>,
                title_transaction: <b className="text-dark">{item.title} | {item?.user?.firstName} </b>
            }
        )
        )
    ) : (
            <></>
        )

    return (
        <Card>

            <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                <div className="tasks-info">
                    <h3 className="mb-75">
                        <strong>Transactions Cagnotes </strong>
                    </h3>
                </div>
            </CardHeader>

            <CardBody>
                <br />
                <DataTable
                    data={data}
                    columns={columns}
                    defaultSortField="createdAt"
                    className='react-dataTable'
                    defaultSortAsc={false}
                    overflowY={true}
                    pagination={true}
                    noHeader />
            </CardBody>
        </Card>
    )
}

export default TransactionCagnoteTable

