import React, { useEffect } from "react"
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { Row, Col } from "reactstrap"
import TransactionserviceUserShowCard from './inc/TransactionserviceUserShowCard'
import { loadShowTransactionserviceUser } from "../../redux/actions/pages/transactionAction"

const TransactionserviceUserShow = () => {
    const { transactionservice } = useParams()
    const item = useSelector(state => state?.transactions?.transaction)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowTransactionserviceUser(transactionservice))
            window.scroll({ top: 0, left: 0, behavior: 'smooth' })
        }
        loadItems()
    }, [transactionservice])


    return (
        <>
            <HelmetSite title={`${item.title || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent={process.env.REACT_APP_NAME}
                breadCrumbActive={item.title || process.env.REACT_APP_NAME}
            />
            {/** Undo redo */}
            <UndoRedo />
            <div className='invoice-preview-wrapper'>
                <Row className='invoice-preview'>
                    <Col xl={12} md={12} sm={12}>
                        <TransactionserviceUserShowCard
                            transaction={item}
                        />
                    </Col>
                </Row>
            </div>
        </>
    )
}

export default TransactionserviceUserShow