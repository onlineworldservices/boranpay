import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { Col, Card, Row, CardHeader } from 'reactstrap'
import { useHistory } from 'react-router-dom'
import SalesCard from '../dashboard/analytics/SalesCard'
import SoldsServicesCard from '../dashboard/analytics/SoldsServicesCard'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllTransactionServicesUser } from '../../redux/actions/pages/transactionAction'
import TransactionServiceTable from './inc/TransactionServiceTable'

const TransactionServiceUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())
    const items = useSelector(state => state?.transactions?.transactionservices)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllTransactionServicesUser(userSite.slugin))
        }
        loadItems()
    }, [userSite.slugin])

    return (
        <>
            <HelmetSite title={`Transactions - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent="Funtion"
                breadCrumbActive={`Transactions ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            <Row className="match-height">
                <Col lg="7" md="6" sm="6">
                    <SoldsServicesCard {...userSite} />
                </Col>

                <Col lg="5" md="6" sm="6">
                    <SalesCard {...userSite} />
                </Col>
            </Row>
            {/** Undo redo */}
            <UndoRedo />

            <Card>
                <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                    <div className="tasks-info">
                        <h3 className="mb-75">
                            <strong>Transactions MarketPlace </strong>
                        </h3>
                    </div>
                </CardHeader>
                <TransactionServiceTable items={items} />
            </Card>
        </>
    )
}

export default TransactionServiceUser