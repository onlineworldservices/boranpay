import React, { useState } from 'react'
import { authuserInfo } from '@components/service'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import Breadcrumbs from '@components/breadcrumbs'
import TransfertUserForm from "./inc/TransfertUserForm"

const TransfertUserCreate = () => {
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Nouveau Transfert`} />
            <Breadcrumbs
                breadCrumbTitle="Transfert"
                breadCrumbParent="Function"
                breadCrumbActive="Nouveau transfert"
            />
            <VerificationInfoUser userItem={userSite} />

            <UndoRedo />
            <TransfertUserForm userSite={userSite} />
        </>
    )
}

export default TransfertUserCreate