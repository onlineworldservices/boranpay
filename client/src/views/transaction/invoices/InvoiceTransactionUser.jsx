import React, { useState, useEffect } from "react"
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { Row, Col } from "reactstrap"
import InviceTransactionUserCard from './inc/InviceTransactionUserCard'
import { loadShowTransactionUser } from "../../../redux/actions/pages/transactionAction"

const InvoiceTransactionUser = () => {
    const { transaction } = useParams()
    const [userSite] = useState(authuserInfo())
    const item = useSelector(state => state?.transactions?.transaction)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowTransactionUser(transaction))
            window.scroll({ top: 0, left: 0, behavior: 'smooth' })
        }
        loadItems()
    }, [transaction])


    return (
        <>
            <HelmetSite title={`${item.title || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Transactions"
                breadCrumbParent={process.env.REACT_APP_NAME}
                breadCrumbActive={item.title || process.env.REACT_APP_NAME}
            />
            {/** Undo redo */}
            <div className='invoice-preview-wrapper'>
                <Row className='invoice-preview'>
                    <div className="col-md-12 mx-auto">
                        <UndoRedo />
                        <InviceTransactionUserCard transaction={item} />
                    </div>
                </Row>
            </div>
        </>
    )
}

export default InvoiceTransactionUser