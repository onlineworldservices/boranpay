import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { Row, Col, Button } from 'reactstrap'
import { Plus  } from 'react-feather'
import { useHistory } from 'react-router-dom'
import ReclamationTable from './inc/ReclamationTable'

const RequestUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Reclamations - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Reclamations"
                breadCrumbParent="Funtion"
                breadCrumbActive={`Reclamations - ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/requests/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouvelle reclamation</b>
            </Button.Ripple>
            <ReclamationTable {...userSite} />
        </>
    )
}

export default RequestUser