import React, { useState, useEffect } from 'react'
import HelmetSite from '@components/helmet/HelmetSite'
import { useDispatch, useSelector } from 'react-redux'
import {
  Card,
  Alert,
  CardBody,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { authHeader, authuserInfo } from '@components/service'
import { useParams, useHistory, Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import Swal from 'sweetalert2'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { loadShowReclamationUser } from '../../redux/actions/pages/reclamationAction'
import { toast } from 'react-toastify'
import userServices from '../../services/userServices'

const schema = yup.object().shape({
  password: yup.string().required().min(8).max(200)
})

const ReclamationShow = () => {
  const history = useHistory()
  const { reclamation } = useParams()
  const [userSite] = useState(authuserInfo())
  const [statusSend, setStatusSend] = useState(false)
  const [errormessage, setErrormessage] = useState('')
  const {
    register,
    handleSubmit,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState
  const reclamationData = useSelector(state => state.reclamations?.reclamation)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadShowReclamationUser(reclamation))
    }
    loadItems()
  }, [reclamation])

  const toggleStatus = () => { setStatusSend(!statusSend) }

  const onSubmit = async (data, e) => {
    Swal.fire({
      title: 'Confirmez vous la tansaction ?',
      text: "Êtes-vous sûr de vouloir executer cette action?",
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Non, annuler',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-primary",
      cancelButtonClass: 'btn btn-outline-default',
      showCancelButton: true,
      reverseButtons: false,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return userServices.userCheckValidate(userSite, data)
          .then(async () => {

            try {
              const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/transfert_reclamation/${reclamation}`, data, { headers: authHeader() })
              if (response) {
                toast.success(
                  <SuccessToast name={'Success'} description={'Transfert éffectué avec succès'} />, {
                  position: toast.POSITION.TOP_RIGHT,
                  hideProgressBar: true
                })
                history.push('/dashboard/')
              }
            } catch (error) {
              toast.error(
                <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
              })
              setErrormessage(error.response.data.message)
            }

          })
          .catch(error => {
            Swal.showValidationMessage(`${error.response.data.message}`)
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }

  return (
    <>
      <HelmetSite title={`Reclamation ${reclamationData.user.firstName || ''}`} />
      <Card>

        <CardBody>
          <div className="col-md-8 mx-auto">
            <div className="tasks-info">
              <h4 className="mb-75">

              </h4>
            </div>
            <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

              {errormessage && (
                <Alert color='danger'>
                  <div className='alert-body text-center'>
                    <AlertCircle size={15} />{' '}
                    <span className='ml-1'>
                      {errormessage}
                    </span>
                  </div>
                </Alert>
              )}

              <div className="col-md-12 text-center">
                {reclamationData.total ? (
                  <>
                    <h5 className='font-large-2 font-weight-bolder mt-2 mb-0'><strong>{reclamationData.total.formatMoney(2, '.', ',')} {reclamationData.currency}</strong></h5>
                    <p>{reclamationData.title}</p>
                    <p>{reclamationData.content}</p>
                  </>
                ) : (
                    <>{/**<Skeleton /> */}</>
                  )}
              </div>

              <Row>
                <Col sm={12}>
                  <FormGroup>
                    <Label for="content">Message</Label>
                    <input className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                      type="text"
                      name="content"
                      id="content"
                      {...register('content')}
                      placeholder="Laisser un message" />
                    <span className='invalid-feedback'>
                      <strong>{errors.content?.message}</strong>
                    </span>
                  </FormGroup>
                </Col>
                {statusSend && (
                  <Col sm={12}>
                    <FormGroup>
                      <Label for="password">Veuillez saisir votre mot de passe</Label>
                      <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                        type="password"
                        name="password"
                        id="password"
                        {...register('password')}
                        placeholder="Mot de passe pour valider la transaction" required />
                      <span className='invalid-feedback'>
                        <strong>{errors.password?.message}</strong>
                      </span>
                    </FormGroup>
                  </Col>
                )}
                <div className="col-md-12 text-center">
                  <FormGroup className="form-label-group">
                    {statusSend ? (
                      <Button.Ripple
                        disabled={isSubmitting}
                        type="submit"
                        className="btn-block"
                        color="primary"
                        block
                      >
                        Terminer
                      </Button.Ripple>
                    ) : (
                        <Button.Ripple
                          className="btn-block"
                          color="primary"
                          onClick={toggleStatus}
                          block
                        >
                          Continuer
                        </Button.Ripple>
                      )}
                  </FormGroup>
                </div>
                <br />
                <div className="col-md-12 text-center">
                  <Link to="/requests/" ><b>Annuler</b></Link>
                </div>
              </Row>
              <br />
              <div className="col-md-12 text-center">
                Vous etes sur le point de faire un transfert de <strong>{reclamationData.total} {reclamationData.currency}</strong> à <strong>{reclamationData.user.firstName}</strong>.
              </div>
            </Form>
          </div>
        </CardBody>
      </Card>
    </>
  )
}

export default ReclamationShow