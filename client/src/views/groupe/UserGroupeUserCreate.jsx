import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authHeader } from '@components/service'
import {
    Card,
    CardBody,
    Alert,
    FormGroup,
    CardHeader,
    Row,
    Col,
    Form,
    Button,
    Label
} from "reactstrap"
import { AlertCircle } from 'react-feather'
import { useParams, useHistory } from "react-router-dom"
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import { loadShowGroupUser } from "../../redux/actions/pages/groupeAction"
import { loadAllCurrencies } from "../../redux/actions/pages/currencyAction"
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    fullName: yup.string().required().min(3).max(200),
    email: yup.string().email().required().min(3).max(200),
    district: yup.string().required().min(3).max(200),
    phone: yup.number().required()
})

const UserGroupeUserCreate = () => {
    const history = useHistory()
    const { groupe } = useParams()
    const {
        register,
        handleSubmit,
        reset,
        formState
    } = useForm({ resolver: yupResolver(schema) })
    const { isSubmitting, errors } = formState
    /** Get currencies with redux */
    const [errormessage, setErrormessage] = useState('')

    const currencies = useSelector(state => state.currencies.currencies)
    const groupedata = useSelector(state => state.groupes.groupe)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
            await dispatch(loadShowGroupUser(groupe))
        }
        loadItems()
    }, [])

    const onSubmit = async (data, e) => {
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/ug/${groupe}/save`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Membre ajouté avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                e.target.reset()
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    return (
        <>
            <HelmetSite title={`Nouveau membre - ${groupedata.name}`} />
            <Breadcrumbs
                breadCrumbTitle="Groupe"
                breadCrumbParent="Sections"
                breadCrumbActive={`Nouveau membre - ${groupedata.name}`}
            />
            {/** Undo redo */}
            <UndoRedo />

            <Card>
                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <div className="tasks-info">
                            <h4 className="mb-75">
                                <strong>{groupedata.name}</strong>
                            </h4>
                        </div>

                        <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}

                            <br />
                            <Row>

                                <Col md="8" sm="8">
                                    <FormGroup>
                                        <Label for="fullName">Nom complet</Label>
                                        <input className={`form-control ${errors.fullName ? 'is-invalid' : ''}`}
                                            type="text"
                                            {...register('fullName')}
                                            placeholder="Nom complet..."
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.fullName?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>

                                <Col md="4" sm="4">
                                    <FormGroup>
                                        <Label for="currency">Devise</Label>
                                        <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`} {...register('currency')} required="required">
                                            <option value="" disabled >Choisissez la devise</option>
                                            {currencies.map((item, index) => (
                                                <option key={index} value={item.code} >{item.code}</option>
                                            ))}
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.currency?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label for="phone">Phone</Label>
                                        <input className={`form-control ${errors.phone ? 'is-invalid' : ''}`}                                    
                                            pattern="[0-9]*"
                                            inputMode="numeric"
                                            {...register('phone')}
                                            minLength="8"
                                            maxLength="20"
                                            placeholder="Phone..."
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.phone?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label for="district">Ville - Localite</Label>
                                        <input className={`form-control ${errors.district ? 'is-invalid' : ''}`}
                                            type="text"
                                            {...register('district')}
                                            placeholder="City..."
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.district?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>

                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="email">E-mail utilisateur</Label>
                                        <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                            type="email"
                                            {...register('email')}
                                            placeholder="Email contact..."
                                            autoComplete="on" required />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.email?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>


                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            type="submit"
                                            className="btn-block"
                                            color="primary"
                                        >
                                            Sauvegarder
                                        </Button.Ripple>
                                        <Button.Ripple
                                            onClick={history.goBack}
                                            className="btn-block"
                                            color="default"
                                        >
                                            Annuler
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default UserGroupeUserCreate