import React, { Component } from "react"
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo, authHeader } from '@components/service'
import { Link } from "react-router-dom"
import Avatar from '@components/avatar'
import {
    Eye,
    MoreVertical,
    ChevronDown,
    Edit,
    Trash,
    Send,
    Plus
} from 'react-feather'
import PropTypes from "prop-types"
import { connect } from "react-redux"
import DataTable from "react-data-table-component"
import { Col, Card, CardBody, CardHeader, Button, Row, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import { loadAllUserGroupeUser, loadShowGroupUser, deleteusergrpItem } from "../../redux/actions/pages/groupeAction"
import Swal from 'sweetalert2'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'


const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.user?.avatar) {
        return <Avatar className='mr-50' img={item?.user?.avatar || 'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={item?.user ? item?.user?.firstName : 'Boclair Temgoua'} initials />
    }
}

class UserGroupeUser extends Component {
    constructor(props) {
        super(props)
        this.deleteItemGroupe = this.deleteItemGroupe.bind(this)
        this.state = {
            userItem: authuserInfo()
        }
    }

    deleteItemGroupe(groupedata) {

        Swal.fire({
            title: 'Suppression?',
            text: "Êtes-vous sûr de vouloir exécuter cette action?",
            confirmButtonText: 'Oui, supprimer',
            cancelButtonText: 'Non, annuler',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: 'btn btn-outline-default',
            showCancelButton: true,
            reverseButtons: true
        }).then(async (result) => {
            if (result.value) {

                //Envoyer la requet au server
                await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes/${groupedata.slugin}`, { headers: authHeader() }).then(() => {
                    toast.success(
                        <SuccessToast name={'Success'} description={'Groupe supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                    this.props.history.push('/groupes/')
                }).catch(() => {
                    toast.error(
                        <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                })
            }
        })
    }

    async componentDidMount() {
        await this.props.loadAllUserGroupeUser(this.props.match.params.groupe)
        await this.props.loadShowGroupUser(this.props.match.params.groupe)
    }


    render() {
        const { items, groupedata } = this.props

        const columns = [
            {
                name: <h5>Profil</h5>,
                selector: "avatar_name",
                sortable: true,
                grow: 4
            },
            {
                name: <h5>Nom | devise</h5>,
                selector: "full_name",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Téléphone</h5>,
                selector: "phone",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Ville - Localite</h5>,
                selector: "district_name",
                sortable: true,
                grow: 2
            },
            {
                name: "",
                selector: "row_button",
                ignoreRowClick: true,
                allowOverflow: true,
                button: true
            }
        ]

        const data = items.length >= 0 ? (
            items.map((item) => (
                {
                    avatar_name: (
                        <>
                            <div className='d-flex justify-content-left align-items-center'>
                                {renderClient(item)}
                                <div className='d-flex flex-column'>
                                    <h6 className='user-name text-truncate mb-0'>{item?.user.firstName} {item?.user?.lastName}</h6>
                                    <small className='text-truncate text-muted mb-0'>{item?.user.email}</small>
                                </div>
                            </div>
                        </>
                    ),
                    full_name: <Link to={`/transaction/send/${this.state.userItem.slugin}/u/${item?.user?.slugin}/`} className="text-dark"><b>{item.fullName} | {item.currency}</b></Link>,
                    phone: <b>{item.phone}</b>,
                    district_name: <Link to={`/transaction/send/${this.state.userItem.slugin}/u/${item?.user?.slugin}/`} className="text-dark"><b>{item.district}</b></Link>,
                    row_button: (
                        <>
                            <div className='column-action d-flex align-items-center'>
                                <Link to={`/ug_transfert/${item.slugin}/transfert`}>
                                    <Send size={17} />
                                </Link>
                                <Link to={`/transaction/send/${this.state.userItem.slugin}/u/${item?.user?.slugin}/`}>
                                    <Eye size={17} className='mx-1' />
                                </Link>
                                <UncontrolledDropdown>
                                    <DropdownToggle tag='span'>
                                        <MoreVertical size={17} className='cursor-pointer' />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem tag={Link} to={`/ugs/${item.slugin}/edit`} className='w-100'>
                                            <Edit size={14} className='mr-50' />
                                            <span className='align-middle'>Editer</span>
                                        </DropdownItem>
                                        <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => this.props.deleteusergrpItem(item)}>
                                            <Trash size={14} className='mr-50' />
                                            <span className='align-middle'>Supprimer</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </>
                    )
                }
            )
            )
        ) : (
                <></>
            )

        return (
            <>
                <HelmetSite title={`${groupedata.name || process.env.REACT_APP_NAME}`} />

                <Breadcrumbs
                    breadCrumbTitle="Groupe"
                    breadCrumbParent="Sections"
                    breadCrumbActive={groupedata.name || process.env.REACT_APP_NAME}
                />
                {/** Undo redo */}
                <UndoRedo />
                <Button.Ripple onClick={() => this.props.history.push(`/ug/${groupedata.slugin}/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                    <Plus size={14} /> <b>Nouveau membre {groupedata.name}</b>
                </Button.Ripple>
                <Row>
                    <Col sm="12">
                        <Card>
                            <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                                <div className="tasks-info">
                                    <h3 className="mb-75">
                                        <strong>{groupedata.name} </strong>&nbsp;
                                        <Edit size={15} onClick={() => this.props.history.push(`/groupes/${groupedata.slugin}/edit/`)} className='cursor-pointer mr-50' />&nbsp;
                                        <Trash size={15} onClick={() => this.deleteItemGroupe(groupedata)} className='cursor-pointer mr-50' />
                                    </h3>
                                </div>
                            </CardHeader>
                            <CardBody>
                                <br />
                                <DataTable
                                    className='react-dataTable'
                                    sortIcon={<ChevronDown size={10} />}
                                    paginationRowsPerPageOptions={[10, 25, 50, 100]}
                                    data={data}
                                    columns={columns}
                                    defaultSortField="updatedAt"
                                    defaultSortAsc={false}
                                    overflowY={true}
                                    pagination={true}
                                    noHeader />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </>
        )
    }

}

UserGroupeUser.propTypes = {
    loadAllUserGroupeUser: PropTypes.func.isRequired,
    loadShowGroupUser: PropTypes.func.isRequired
}

const mapStoreToProps = store => ({
    groupedata: store.groupes.groupe,
    items: store.groupes.usergroupes
})

export default connect(mapStoreToProps,
    { loadAllUserGroupeUser, loadShowGroupUser, deleteusergrpItem }
)(UserGroupeUser)
