import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import React, { useState, useEffect } from 'react'
import {
    Card,
    CardBody,
    FormGroup,
    Alert,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import { Plus, AlertCircle } from 'react-feather'
import { authHeader } from '@components/service'
import { useParams, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import Swal from 'sweetalert2'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    name: yup.string().required().min(3).max(200),
    content: yup.string().required().min(3).max(6000)
})

const GroupeUserCreateAndEdit = () => {
    const { slugin } = useParams()
    const history = useHistory()
    const isAddMode = !slugin
    const {
        register,
        handleSubmit,
        reset,
        setValue,
        formState
    } = useForm({ resolver: yupResolver(schema) })
    const { isSubmitting, errors } = formState
    /** Get currencies with redux */
    const [errormessage, setErrormessage] = useState("")

    useEffect(() => {
        const loadItems = async () => {
            if (!isAddMode) {
                axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes/${slugin}`, { headers: authHeader() })
                    .then(groupe => {
                        const fields = ['name', 'content']
                        fields.forEach(field => setValue(field, groupe.data[field]))
                    })
            }
        }
        loadItems()
    }, [])

    const deleteItem = (slugin) => {

        Swal.fire({
            title: 'Suppression?',
            text: "Êtes-vous sûr de vouloir executer cette action?",
            confirmButtonText: 'Oui, supprimer',
            cancelButtonText: 'Non, annuler',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: 'btn btn-outline-default',
            showCancelButton: true,
            reverseButtons: false
        }).then(async (result) => {
            if (result.value) {

                //Envoyer la requet au server
                await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes/${slugin}`, { headers: authHeader() }).then(() => {
                    toast.success(
                        <SuccessToast name={'Success'} description={'Groupe supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                    history.push('/groupes/')
                }).catch(() => {
                    toast.error(
                        <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                })
            }
        })
    }

    const updateItem = async (slugin, data) => {
        await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes/${slugin}`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Groupe mis à jour avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.goBack()
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const createItem = async (data, e) => {
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/groupes`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Groupe crée avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/groupes/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const onSubmit = (data) => {
        return isAddMode ? createItem(data) : updateItem(slugin, data)
    }

    return (
        <>
            <HelmetSite title={`${isAddMode ? `Nouveau groupe` : `Editer ce groupe`}`} />
            <Breadcrumbs
                breadCrumbTitle="Groupe"
                breadCrumbParent="Sections"
                breadCrumbActive={isAddMode ? `Nouveau groupe` : `Editer ce groupe`}
            />
            {/** Undo redo */}
            <UndoRedo />

            {!isAddMode && (
                <Button.Ripple onClick={() => history.push(`/groupes/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                    <Plus size={14} /> <b>Nouveau Groupe</b>
                </Button.Ripple>
            )}
            <Card>
                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <div className="tasks-info">
                            <h4 className="mb-75">
                                <strong>{`${isAddMode ? `Nouveau groupe` : `Editer ce groupe`}`}</strong>
                            </h4>
                        </div>
                        <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}

                            <br />
                            <Row>

                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="name"><strong>Nom</strong></Label>
                                        <input className={`form-control ${errors.name ? 'is-invalid' : ''}`}
                                            type="text"
                                            {...register("name")}
                                            placeholder="nom du groupe"
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.name?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>

                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="content"><strong>Description du groupe</strong></Label>
                                        <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                                            type="text"
                                            rows="3"
                                            maxLength="6000"
                                            {...register("content")}
                                            placeholder="Décrivez le groupe ..."
                                            required="required"
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.content?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            type="submit"
                                            className="btn-block"
                                            color="primary"
                                        >
                                            Sauvegarder
                                        </Button.Ripple>
                                        <Button.Ripple
                                            onClick={() => history.goBack()}
                                            className="btn-block"
                                            color="default"
                                        >
                                            Annuler
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>
                                {!isAddMode && (
                                    <div className="col-md-12 text-center">
                                        <a href={void (0)} onClick={() => deleteItem(slugin)} className="nav-link text-danger" ><b>Supprimer</b></a>
                                    </div>
                                )}
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default GroupeUserCreateAndEdit