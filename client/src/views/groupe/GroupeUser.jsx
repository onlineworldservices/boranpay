import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import UndoRedo from '@components/undoredo'
import { Plus } from 'react-feather'
import { authuserInfo } from '@components/service'
import { useHistory } from 'react-router-dom'
import { Col, Row, Button } from 'reactstrap'
import GroupeTable from './inc/GroupeTable'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllGroupeUser } from '../../redux/actions/pages/groupeAction'

const GroupeUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())
    const items = useSelector(state => state?.groupes?.groupes)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllGroupeUser(userSite))
        }
        loadItems()
    }, [userSite])

    return (
        <>
            <HelmetSite title={`Groupe - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Groupe"
                breadCrumbParent="Sections"
                breadCrumbActive={`Groupe ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/groupes/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouveau Groupe</b>
            </Button.Ripple>

            <Row>
                <Col sm='12'>
                    <GroupeTable groupes={items} />
                </Col>
            </Row>
        </>
    )
}

export default GroupeUser