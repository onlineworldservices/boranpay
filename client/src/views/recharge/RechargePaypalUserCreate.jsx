import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import {
  Card,
  CardBody,
  FormGroup,
  CardHeader,
  Row,
  Col,
  Form,
  Button,
  Alert,
  Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { Link, useHistory } from "react-router-dom"
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import { authHeader } from '@components/service'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  total: yup.number().required().positive().integer()
})

const RechargePaypalUserCreate = () => {
  const history = useHistory()
  const [errormessage, setErrormessage] = useState('')
  const {
    register,
    handleSubmit,
    errors,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { isSubmitting } = formState

  const onSubmit = async (data, e) => {
    await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/recharges/coupons`, data, { headers: authHeader() })
      .then((res) => {
        toast.success(
          <SuccessToast name={'Success'} description={'Recharge éffectuée avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        history.push('/dashboard/')
      }).catch((error) => {
        toast.error(
          <ErrorToast name={'Opp error'} description={'Quelque chose ne va pas ...'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        setErrormessage(error.response.data.message)
      })
  }

  return (
    <>
      <HelmetSite title={`Nouvelle recharge paypal`} />
      <Breadcrumbs
        breadCrumbTitle="Recharge paypal"
        breadCrumbParent="Function"
        breadCrumbActive="Nouvelle recharge paypal"
      />

      <UndoRedo />

      <Row>
        <Col sm="12">
          <Card>
            <CardHeader>
              <div className="tasks-info">
                <h3 className="mb-75">
                  <strong>{`Nouvelle recharge`}</strong>
                </h3>
              </div>
            </CardHeader>
            <CardBody>

              <div className="col-md-8 mx-auto">
                <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

                  {errormessage && (
                    <Alert color='danger'>
                      <div className='alert-body text-center'>
                        <AlertCircle size={15} />{' '}
                        <span className='ml-1'>
                          {errormessage}
                        </span>
                      </div>
                    </Alert>
                  )}

                  <br />
                  <Row>

                    <Col md="6" sm="6">
                      <FormGroup>
                        <Label for="total">Montant</Label>
                        <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                          type="number"
                          min="1" step="1"
                          name="total"
                          id="total"
                          placeholder="Insérer le montant"
                          ref={register}
                          autoComplete="false"
                          required
                        />
                        <span className='invalid-feedback'>
                          <strong>{errors.total?.message}</strong>
                        </span>
                      </FormGroup>
                    </Col>

                    <Col md="6" sm="6">
                      <FormGroup>
                        <Label for="total">Devise</Label>
                        <input className={`form-control`}
                          type="text"
                          name="currency"
                          id="currency"
                          defaultValue={'EUR'}
                          placeholder="Currency"
                          ref={register}
                          required="required"
                          disabled
                        />
                      </FormGroup>
                    </Col>

                  </Row>
                  <Button.Ripple color='primary' type="submit" block>
                    <b>Recharger le compte</b>
                  </Button.Ripple>
                  <br />
                  <div className="col-md-12 text-center">
                    <Link to="/recharge/" ><b>Annuler</b></Link>
                  </div>

                </Form>
              </div>

            </CardBody>
          </Card>
        </Col>
      </Row>
    </>
  )
}

export default RechargePaypalUserCreate