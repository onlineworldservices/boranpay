import React, { useEffect, useState } from 'react'
import {
  Row,
  FormGroup,
  Label
} from 'reactstrap'
import RechargeCouponForm from './RechargeCouponForm'
import StripeContainer from './StripeContainer'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllPayementmethodsrechage } from '../../../redux/actions/pages/countryAction'


const SelectedMethodeCharge = () => {
  const [selectedMethode, setSelectedMethode] = useState()
  const payementmethods = useSelector(state => state?.countries?.payementmethods)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllPayementmethodsrechage())
    }
    loadItems()
  }, [])

  return (
    <>
      <div className="col-md-10 mx-auto">
        <div className="tasks-info">
          <h3 className="mb-75">
            <strong>{`Nouvelle recharge`}</strong>
          </h3>
        </div>
        <FormGroup>
          <Label><b>Methode</b></Label>
          <select className={`form-control`} name={'methode'} value={selectedMethode} onChange={(e) => setSelectedMethode(e.target.value)}>
            <option value='' >Choose methode</option>
            {payementmethods.map((item, index) => (
              <option key={index} value={item.slug}>{item.name}</option>
            ))}
          </select>
        </FormGroup>
        {selectedMethode === 'coupons' && (
          <RechargeCouponForm />
        )}

        {selectedMethode === 'stripes' && (
          <StripeContainer />
        )}

        <Row>
          {/**
                   * <Col md={`${selectedType}` ? "7" : "12"} sm={`${selectedType}` ? "7" : "12"}>
                    <FormGroup>
                      <Label><b>Methode</b></Label>
                      <select className={`form-control`}
                        value={selectedType} onChange={(e) => setSelectedType(e.target.value)}
                        name="country" id="country" required="required">
                        <option value="" disabled>Choose m</option>
                        {types.filter((item) => item.includes(selectedContinent))
                          .map((item) => (
                            <option key={item.id} value={item.name} >{item.name}</option>
                          ))}
                      </select>
                    </FormGroup>
                  </Col>

                  {selectedType && (
                    <Col md="5" sm="5">
                      <FormGroup>
                        <Label><b>Payment method</b></Label>
                      </FormGroup>
                    </Col>
                  )}
                   */}

        </Row>

      </div>
    </>
  )
}

export default SelectedMethodeCharge