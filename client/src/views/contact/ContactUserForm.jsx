import React, { useState, useEffect } from 'react'
import {
    Alert,
    Col,
    Form,
    Button,
    Label
} from "reactstrap"
import { AlertCircle } from 'react-feather'
import { authHeader } from '@components/service'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCountries } from '../../redux/actions/pages/countryAction'
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import * as yup from 'yup'
import axios from "axios"
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    countryId: yup.number().required(),
    content: yup.string().required().min(5).max(65000)
})

const ContactUserForm = (props) => {
    const { dataItem } = props
    const [errormessage, setErrormessage] = useState('')
    const {
        register,
        handleSubmit,
        formState
    } = useForm({ resolver: yupResolver(schema), mode: "onChange" })
    const { errors, isSubmitting } = formState
    const countries = useSelector(state => state?.countries?.countries)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCountries())
        }
        loadItems()
    }, [])

    const onSubmit = async (data, e) => {
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/contact/${dataItem.user.slugin}/user`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Message envoyée avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                e.target.reset()
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    return (
        <>
            <Col sm="12" className="text-center">
                <h4>Contacter {dataItem.user.firstName} {dataItem.user.lastName}</h4>
            </Col>

            <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

                <fieldset className="form-label-group mb-50">
                    {errormessage && (
                        <Alert color='danger'>
                        <div className='alert-body text-center'>
                          <AlertCircle size={15} />{' '}
                          <span className='ml-1'>
                            {errormessage}
                          </span>
                        </div>
                      </Alert>
                    )}
                </fieldset>
                <fieldset>
                    <Label className='form-label' htmlFor='countryId'>
                        <strong>Pays residence</strong>
                    </Label>
                    <select className={`form-control ${errors.countryId ? 'is-invalid' : ''}`}
                        {...register('countryId')} id="countryId" required="required">
                        <option value={''}>Selectioner votre pays</option>
                        {countries.map((item, index) => <option key={index} value={item.id} >{item.name}</option>)}
                    </select>
                    <span className='invalid-feedback'>
                        <strong>{errors.countryId?.message}</strong>
                    </span>
                </fieldset>
                <fieldset>
                    <Label htmlFor="content"><strong>Message</strong></Label>
                    <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                        type="text"
                        id="content"
                        rows={'3'}
                        {...register('content')}
                        maxLength="65000"
                        placeholder={`Laisser un message à ${dataItem.user.firstName} ...`}
                        required
                    />
                    <span className='invalid-feedback'>
                        <strong>{errors.content?.message}</strong>
                    </span>
                </fieldset>
                <br />
                <fieldset>
                    <Button.Ripple
                        block
                        disabled={isSubmitting}
                        type="submit"
                        className={`${isSubmitting ? "btn-loading" : ""
                            } btn-load`} color="primary">
                        Contacter
                    </Button.Ripple>
                </fieldset>
            </Form>
        </>
    )
}
export default ContactUserForm
