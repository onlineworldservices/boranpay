import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { Link } from 'react-router-dom'
import { authuserInfo } from '@components/service'
import { Table, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'
import { MoreVertical, Edit, Trash, Plus } from 'react-feather'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllPayementmethodsuserretrait, deletePMUItem } from '../../redux/actions/pages/countryAction'

const PayementmethodUser = () => {
    const [userSite] = useState(authuserInfo())
    const items = useSelector(state => state?.countries?.payementmethodsusers.rows)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllPayementmethodsuserretrait(userSite))
        }
        loadItems()
    }, [userSite])


    return (
        <>
            <HelmetSite title={`Methode de payement`} />
            <Breadcrumbs
                breadCrumbTitle="Methode de payement "
                breadCrumbParent="Sections"
                breadCrumbActive={'Methode de payement'}
            />
            {/** Undo redo */}
            <UndoRedo />
            <Link to={`/mypayementmethods/new/`} className="btn btn-primary btn-sm mr-1 mb-1" >
                <Plus size={14} />
            </Link>

            <Table size='sm' responsive>
                <thead>
                    <tr>
                        <th>Pays</th>
                        <th>Contact</th>
                        <th>Methode</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {items?.length && items.map(item => (
                        <tr key={item.id} >
                            <td>{item?.countryName}</td>
                            <td>{item?.contactRecev}</td>
                            <td>{item?.payementmethod?.name}</td>
                            <td>
                                <UncontrolledDropdown>
                                    <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                                        <MoreVertical size={15} />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem tag={Link} to={`/mypayementmethods/${item?.slugin}/edit/`}>
                                            <Edit className='mr-50' size={15} /> <span className='align-middle'>Edit</span>
                                        </DropdownItem>
                                        <DropdownItem tag='a' href={void (0)} onClick={() => dispatch(deletePMUItem(item))}>
                                            <Trash className='mr-50' size={15} /> <span className='align-middle'>Supprimer</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>

        </>
    )

}

export default PayementmethodUser