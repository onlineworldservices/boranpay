import React, { useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup'
import { Alert, Form, Label, FormGroup, Row, Col, Button } from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { useForm } from 'react-hook-form'
import { authHeader } from '@components/service'
import * as yup from 'yup'
import axios from 'axios'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const schema = yup.object().shape({
  oldpassword: yup.string().required().min(8).max(200),
  password: yup.string().required().min(8).max(200),
  confirmPassword: yup.string().required().min(8).max(200)
    .oneOf([yup.ref(`password`), null], 'Passwords must match')
})

const PasswordTab = ({ userItem }) => {
  const {
    register,
    handleSubmit,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState
  const [errormessage, setErrormessage] = useState('')

  const onSubmit = async (data, e) => {

    try {
      const response = await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/password/update/${userItem.slugin}`, data, { headers: authHeader() })
      if (response) {
        toast.success(
          <SuccessToast name={'Success'} description={'Password mise à jour avec succès'} />, {
          position: toast.POSITION.TOP_RIGHT,
          hideProgressBar: true
        })
        e.target.reset()
      }
    } catch (error) {
      toast.error(
        <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
        position: toast.POSITION.TOP_RIGHT,
        hideProgressBar: true
      })
      setErrormessage(error.response.data.message)
    }
  }

  return (

    <Form className='mt-2' onSubmit={handleSubmit(onSubmit)}>
      {errormessage && (
        <Alert color='danger'>
          <div className='alert-body text-center'>
            <AlertCircle size={15} />{' '}
            <span className='ml-1'>
              {errormessage}
            </span>
          </div>
        </Alert>
      )}
      <Row>
        <Col sm='12'>
          <FormGroup>
            <Label for="oldpassword">Mot de passe actuel</Label>
            <input className={`form-control ${errors.oldpassword ? 'is-invalid' : ''}`}
              type="password"
              id="oldpassword"
              placeholder="Mot de passe actuel"
              autoComplete="off"
              {...register("oldpassword")}
              required />
            <span className='invalid-feedback'>
              <strong>{errors.oldpassword?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col sm='12'>
          <FormGroup>
            <Label for="password">Nouveau mot de passe</Label>
            <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
              type="password"
              id="password"
              placeholder="Nouveau mot de passe "
              {...register('password')}
              required="required" />
            <span className='invalid-feedback'>
              <strong>{errors.password?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col sm='12'>
          <FormGroup>
            <Label for="confirmPassword">Confirmer le mot de passe</Label>
            <input className={`form-control ${errors.confirmPassword ? 'is-invalid' : ''}`}
              type="password"
              id="confirmPassword"
              placeholder="Confirmer votre nouveau mot de passe"
              {...register('confirmPassword')}
              required />
            <span className='invalid-feedback'>
              <strong>{errors.confirmPassword?.message}</strong>
            </span>
          </FormGroup>
        </Col>
        <Col className='mt-2 text-center' sm='12'>
          <Button.Ripple
            type='submit'
            className='mr-1'
            color='primary'
            disabled={isSubmitting} >
            Sauvegarder
            </Button.Ripple>
        </Col>
      </Row>
    </Form>
  )
}

export default PasswordTab
