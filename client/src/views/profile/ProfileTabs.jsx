import { Nav, NavItem, NavLink } from 'reactstrap'
import { User, Lock, Info} from 'react-feather'

const ProfileTabs = ({ activeTab, toggleTab }) => {
  return (
    <Nav className='nav-left' pills vertical>
      <NavItem>
        <NavLink active={activeTab === '1'} onClick={() => toggleTab('1')}>
          <User size={18} className='mr-1' />
          <span className='font-weight-bold'>Generale</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === '2'} onClick={() => toggleTab('2')}>
          <Info size={18} className='mr-1' />
          <span className='font-weight-bold'>Informations</span>
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink active={activeTab === '3'} onClick={() => toggleTab('3')}>
          <Lock size={18} className='mr-1' />
          <span className='font-weight-bold'>Changer mot de passe</span>
        </NavLink>
      </NavItem>
      {/**
      <NavItem>
        <NavLink active={activeTab === '4'} onClick={() => toggleTab('4')}>
          <Bell size={18} className='mr-1' />
          <span className='font-weight-bold'>Notifications</span>
        </NavLink>
      </NavItem>
       */}
    </Nav>
  )
}

export default ProfileTabs
