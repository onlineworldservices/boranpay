import React, { useState } from 'react'
import HelmetSite from '../../@core/components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import Breadcrumbs from '@components/breadcrumbs'
import VirementCagnoteUserForm from "./inc/VirementCagnoteUserForm"

const VirementCagnoteUserCreate = () => {
    const [userSite] = useState(authuserInfo())


    return (
        <>
            <HelmetSite title={`Nouveau Virement`} />
            <Breadcrumbs
                breadCrumbTitle="Virement"
                breadCrumbParent="Function"
                breadCrumbActive="Nouveau virement"
            />

            <UndoRedo />

            <VirementCagnoteUserForm userSite={userSite} />
        </>
    )
}

export default VirementCagnoteUserCreate