import React, { useState, useEffect } from 'react'
import {
    Card,
    CardBody,
    FormGroup,
    Row,
    Col,
    Form,
    Button,
    Alert,
    Label
} from 'reactstrap'
import ReactQuill from 'react-quill'
import { Plus, AlertCircle } from 'react-feather'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import { useParams, useHistory } from 'react-router-dom'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import Swal from 'sweetalert2'
import axios from 'axios'
import { authuserInfo, authHeader } from '@components/service'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import donationApiServices from '../../services/donationApiServices'

const schema = yup.object().shape({
    title: yup.string().required().min(3).max(200)
})

const DonationCreateAndEdit = () => {
    const history = useHistory()
    const { slugin } = useParams()
    const [userSite] = useState(authuserInfo())
    const isAddMode = !slugin
    const {
        register,
        handleSubmit,
        reset,
        setValue,
        control,
        formState
    } = useForm({ resolver: yupResolver(schema), mode: "onChange" })
    const { errors, isSubmitting } = formState
    /** Get currencies with redux */
    const [errormessage, setErrormessage] = useState('')

    useEffect(() => {
        const loadItems = async () => {
            if (!isAddMode) {
                await donationApiServices.getShowEditDonation(slugin, userSite)
                    .then(groupe => {
                        const fields = ['title', 'content', 'status']
                        fields.forEach(field => setValue(field, groupe.data[field]))
                    })
            }
        }
        loadItems()
    }, [slugin, userSite.slugin])

    const deleteItem = (slugin) => {

        Swal.fire({
            title: 'Suppression?',
            text: "Êtes-vous sûr de vouloir exécuter cette action?",
            confirmButtonText: 'Oui, supprimer',
            cancelButtonText: 'Non, annuler',
            buttonsStyling: false,
            confirmButtonClass: "btn btn-primary",
            cancelButtonClass: 'btn btn-outline-default',
            showCancelButton: true,
            reverseButtons: false
        }).then(async (result) => {
            if (result.value) {

                //Envoyer la requet au server
                await donationApiServices.updateStatusDonation(slugin, data).then(() => {
                    toast.success(
                        <SuccessToast name={'Success'} description={'Don supprimé avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                    history.push('/donations/')
                }).catch(() => {
                    toast.error(
                        <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                })
            }
        })
    }


    const updateItem = async (slugin, data) => {
        await donationApiServices.updateDonation(slugin, data)
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Données mis à jour avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/donations/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const createItem = async (data, e) => {
        await donationApiServices.postDonation(data)
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Campagne créée avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/donations/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const onSubmit = (data) => {
        return isAddMode ? createItem(data) : updateItem(slugin, data)
    }

    return (
        <>
            <HelmetSite title={`${isAddMode ? `Nouvelle donation` : `Editer la donation`}`} />
            <Breadcrumbs
                breadCrumbTitle='Donations'
                breadCrumbParent='Discover'
                breadCrumbActive={isAddMode ? `Nouvelle campagne de donations` : `Editer la campagne`}
            />
            <VerificationInfoUser userItem={userSite} />
            {/**
             * 
            <Alert color='danger'>
                <div className='alert-body'>
                    <span className='font-weight-bold'>Info: </span>
                    <span>
                        Please check the{' '}
                        <a
                            href='https://pixinvent.com/demo/vuexy-react-admin-dashboard-template/documentation/development/page-layouts'
                            target='_blank'
                        >
                            layout boxed documentation
                        </a>{' '}
                            for more details.
                    </span>
                </div>
            </Alert>
             */}
            <UndoRedo />
            {!isAddMode && (
                <Button.Ripple onClick={() => history.push(`/donation/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                    <Plus size={14} /> <b>Nouvelle campagne de dons</b>
                </Button.Ripple>
            )}
            <Card>
                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <div className="tasks-info">
                            <h3 className="mb-75">
                                <strong>{`${isAddMode ? `Nouvelle campagne` : `Editer la campagne`}`}</strong>
                            </h3>
                        </div>
                        <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}

                            <br />
                            <Row>

                                <Col sm="12">
                                    <FormGroup>
                                        <Label className='form-label' for='title'>
                                            <strong>Donner un titre à cette campagne</strong>
                                        </Label>
                                        <input className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="title"
                                            minLength="3"
                                            maxLength="200"
                                            {...register('title')}
                                            placeholder="Donnez un titre"
                                            autoComplete="off"
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.title?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="12" sm="12">
                                    <FormGroup>
                                        <Label className='form-label' for='content'>
                                            <strong>Description de la campagne</strong>
                                        </Label>
                                        <Controller
                                            name="content"
                                            control={control}
                                            defaultValue={''}
                                            render={({ field }) => (
                                                <ReactQuill
                                                    {...field}
                                                    theme="snow"
                                                    initialValue={`${process.env.REACT_APP_NAME}`}
                                                    modules={{
                                                        toolbar: [
                                                            ['bold', 'italic', 'underline'],
                                                            [{ list: 'ordered' }, { list: 'bullet' }],
                                                            [{ align: [] }],
                                                            ['link'],
                                                            [{ color: [] }, { background: [] }]
                                                        ]
                                                    }}
                                                />
                                            )}
                                        />
                                        {errors.content && (
                                            <span className='invalid-feedback'>
                                                <strong>{errors.content?.message}</strong>
                                            </span>
                                        )}
                                    </FormGroup>
                                </Col>
                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            type="submit"
                                            className="btn-block"
                                            color="primary"
                                        >
                                            Sauvegarder
                                        </Button.Ripple>
                                        <Button.Ripple
                                            onClick={() => history.push(`/donations/`)}
                                            className="btn-block"
                                            color="default"
                                        >
                                            Annuler
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>
                                <div className="col-md-12 text-center">
                                    Rassurez-vous de bien décrire la campagne à fin de faire comprendre le bien fondé de celle-ci
                                    et faciliter la communication pour susciter de l'intéret
                                </div>
                                <div className="col-md-12 text-center">
                                    <a href={void (0)} onClick={() => deleteItem(slugin)} className="nav-link text-danger" ><b>Supprimer</b></a>
                                </div>
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default DonationCreateAndEdit