import { Button, Row, Col, Card, CardBody } from 'reactstrap'
import SoldsDonationCard from '../../dashboard/analytics/SoldsDonationCard'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const DonationCard = (props) => {
    const { userData } = props

    const onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès partager a present'} />, {
            position: toast.POSITION.TOP_RIGHT,
            hideProgressBar: true
        })
    }

    return (
        <Row className="match-height">
            <Col lg="7" md="6" sm="6">
                <SoldsDonationCard {...userData} />
            </Col>

            <Col lg="5" md="6" sm="6">
                <Card className='card-congratulations'>
                    <CardBody className='text-center'>
                        <div className='award-info text-center'>
                            <h2 className="mb-2 text-white"> {userData.sex === "male" ? "Mr" : "Mme"} {userData.firstName} {userData.lastName} </h2>
                            <p className="m-auto mb-0 w-75">
                                Pour vos dons personnel
                                copier le lien et partager avec vos ami(e)s
                            </p>
                            <br />
                            <CopyToClipboard
                                onCopy={onCopy}
                                text={`${process.env.REACT_APP_LINK}/donations/${userData.slugin}/new/`}
                            >
                                <Button color="info">
                                    copier le lien
                                </Button>
                            </CopyToClipboard>
                        </div>
                    </CardBody>
                </Card>
            </Col>
        </Row>
    )
}
export default DonationCard