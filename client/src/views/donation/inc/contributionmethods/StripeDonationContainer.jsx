import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import StripeDonationForm from "./StripeDonationForm"

const PUBLIC_KEY = `${process.env.REACT_APP_STRIPE_PUBLIC_KEY}`
const stripeTestPromise = loadStripe(PUBLIC_KEY)
const StripeCagnoteContainer = ({ donation }) => {
    return (
        <Elements stripe={stripeTestPromise}>
            <StripeDonationForm donation={donation} />
        </Elements>
    )
}
export default StripeCagnoteContainer