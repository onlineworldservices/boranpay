import React, { useEffect, useState } from 'react'
import {
  FormGroup,
  Label
} from 'reactstrap'
import CouponDonationForm from './CouponDonationForm'
import StripeDonationContainer from './StripeDonationContainer'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllPayementmethodsrechage } from '../../../../redux/actions/pages/countryAction'


const SelectedContributionMethods = ({ donation }) => {
  const [selectedMethode, setSelectedMethode] = useState()
  const payementmethods = useSelector(state => state?.countries?.payementmethods)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllPayementmethodsrechage())
    }
    loadItems()
  }, [])

  return (
    <>
      <div className="col-lg-12 mx-auto">
        <FormGroup>
          <Label><b>Sélectioner une methode</b></Label>
          <select className={`form-control`} name={'methode'} value={selectedMethode} onChange={(e) => setSelectedMethode(e.target.value)}>
            <option value='' >Choose method</option>
            {payementmethods.map((item, index) => (
              <option key={index} value={item.slug}>{item.name}</option>
            ))}
          </select>
        </FormGroup>

        {selectedMethode === 'coupons' && (
          <CouponDonationForm donation={donation} />
        )}

        {selectedMethode === 'stripes' && (
          <StripeDonationContainer donation={donation} />
        )}

      </div>
    </>
  )
}

export default SelectedContributionMethods