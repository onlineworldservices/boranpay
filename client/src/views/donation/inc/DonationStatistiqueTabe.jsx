import React from 'react'
import { Card, CardBody, CardHeader } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import {
    ChevronDown
} from 'react-feather'
import moment from "moment"
import Avatar from '@components/avatar'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.user?.avatar) {
        return <Avatar className='mr-50' img={item?.user?.avatar || 'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={`${item?.user?.firstName} ${item?.user?.lastName}`} initials />
    }
}
const DonationStatistiqueTabe = (props) => {
    const { donation } = props

    const columns = [
        
        {
            name: <h5>Donateur</h5>,
            selector: "donateur_transaction",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true,
            grow: 2
        }
    ]

    const data = donation?.contributes?.length >= 0 ? (
        donation?.contributes.map(item => (
            {
                donateur_transaction: (
                    <>
                        <div className='d-flex justify-content-left align-items-center'>
                            {renderClient(item)}
                            <div className='d-flex flex-column'>
                                <h6 className='user-name text-truncate mb-0'>{item?.user?.firstName} {item?.user?.lastName}</h6>
                                <small className='text-truncate text-muted mb-0'>{item?.user?.email}</small>
                            </div>
                        </div>
                    </>
                ),
                price_transaction: <Link to={`${item.transactiondonationId !== null ? `/transactions_donation/${item?.transactiondonation?.slugin}/` : '/dashboard/'}`} className="text-dark"><h4 className={`text-success`}>{item.total && (<>+ {(item.total + item.taxeContribute).formatMoney(2, '.', ',')} <small>{item.currency}</small></>)}</h4></Link>,
                date: <Link to={`${item.transactiondonationId !== null ? `/transactions_donation/${item?.transactiondonation?.slugin}/` : '/dashboard/'}`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link>
            }
        )
        )
    ) : (
            <></>
        )
    return (


        <>
            <Card>
                <CardHeader>
                    <div className="tasks-info">
                        <h3 className="mb-75">
                            <strong>{donation.title}</strong>
                        </h3>
                    </div>
                </CardHeader>

                <CardBody>
                    <DataTable
                        data={data}
                        columns={columns}
                        defaultSortField="createdAt"
                        defaultSortAsc={false}
                        overflowY={true}
                        pagination={true}
                        sortIcon={<ChevronDown size={10} />}
                        className='react-dataTable'
                        noHeader />
                </CardBody>
            </Card>
        </>
    )

}


export default DonationStatistiqueTabe

