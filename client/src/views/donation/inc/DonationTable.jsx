import React, { Fragment, Component } from 'react'
import { Card, CardBody, CardHeader, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import {
    Eye,
    Copy,
    MoreVertical,
    Edit,
    ChevronDown,
    Users,
    Activity,
    Trash
} from 'react-feather'
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { loadDonationUser, deleteItem } from '../../../redux/actions/pages/donationAction'
import moment from "moment"
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'


class DonationTable extends Component {

    onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès partager a present'} />, {
            position: toast.POSITION.TOP_RIGHT,
            hideProgressBar: true
        })
    }

    async componentDidMount() {
        await this.props.loadDonationUser(this.props.slugin)
    }

    render() {
        const { items } = this.props
        const columns = [
            {
                name: <h5>Date</h5>,
                selector: "date",
                sortable: true
            },
            {
                name: <h5>Titre</h5>,
                selector: "title_data",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Collectés</h5>,
                selector: "price_collecte",
                sortable: true,
                grow: 2
            },
            {
                name: <h5>Donateurs</h5>,
                selector: "contributors",
                sortable: true,
                grow: 2
            },
            {
                name: "",
                selector: "row_button",
                ignoreRowClick: true,
                allowOverflow: true,
                button: true
            }
        ]

        const data = items?.length >= 0 ? (
            items.map(item => (
                {
                    date: <p><Link to={`/donation/${item.slugin}/statistique/`} className="text-dark"><b>{moment(item.createdAt).format('ll')}</b></Link></p>,
                    title_data: <p><Link to={`/donation/${item.slugin}/statistique/`} className="text-dark">{(item.title.length > 50 ? item.title.substring(0, 50) : item.title)}</Link></p>,
                    price_collecte: <Link to={`/donation/${item.slugin}/statistique/`}><h4 className="text-success"> {item.amountdonations.length > 0 ? <><p className="text-success">
                        {/** Ici c'est pour capturer et convertire */}
                        + {(item.amountdonations[0].totalAmountdonation * item.user.profile.currency.currencyNumber).formatMoney(2, '.', ',')}
                        <small> {item.user.profile.currency.code}</small></p></> : <>0,00 <small>{item.user.profile.currency.code}</small></>}</h4></Link>,
                    contributors: <h4 className="text-dark">{item.contributes.length ? <>{(item.contributes[0].contributes_count - 1)}</> : 0} <Users size={20} /> </h4>,
                    row_button: (
                        <>
                            <div className='column-action d-flex align-items-center'>
                                <CopyToClipboard
                                    onCopy={this.onCopy}
                                    text={`${process.env.REACT_APP_LINK}/donation/${item.slugin}/contribution/`}
                                >
                                    <Copy size={17} className='cursor-pointer' />
                                </CopyToClipboard>
                                <Link to={`/donation/${item.slugin}/contribution/`}>
                                    <Eye size={17} className='mx-1' />
                                </Link>
                                <UncontrolledDropdown>
                                    <DropdownToggle tag='span'>
                                        <MoreVertical size={17} className='cursor-pointer' />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem tag={Link} to={`/donation/${item.slugin}/statistique/`} className='w-100'>
                                            <Activity size={14} className='mr-50' />
                                            <span className='align-middle'>Statistique</span>
                                        </DropdownItem>
                                        {/**
                                        <DropdownItem tag={Link} to={`/donation/${item.slugin}/statistique/`} className='w-100'>
                                            <Activity size={14} className='mr-50' />
                                            <span className='align-middle'>Transactions</span>
                                        </DropdownItem> */}
                                        <DropdownItem tag={Link} to={`/donation/${item.slugin}/edit/`} className='w-100'>
                                            <Edit size={14} className='mr-50' />
                                            <span className='align-middle'>Editer</span>
                                        </DropdownItem>
                                        <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => this.props.deleteItem(item)}>
                                            <Trash size={14} className='mr-50' />
                                            <span className='align-middle'>Supprimer</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </>
                    )
                }
            )
            )
        ) : (
                <></>
            )

        return (
            <>
                <Card>

                    <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                        <div className="tasks-info">
                            <h4 className="mb-75">
                                <strong>Donations</strong>
                            </h4>
                        </div>
                    </CardHeader>

                    <CardBody>
                        <br />
                        <DataTable
                            className='react-dataTable'
                            sortIcon={<ChevronDown size={10} />}
                            paginationRowsPerPageOptions={[10, 25, 50, 100]}
                            data={data}
                            columns={columns}
                            defaultSortField="createdAt"
                            defaultSortAsc={false}
                            overflowY={true}
                            pagination={true}
                            noHeader />
                    </CardBody>
                </Card>
            </>
        )

    }
}
DonationTable.propTypes = {
    loadDonationUser: PropTypes.func.isRequired
}

const mapStoreToProps = store => ({
    items: store?.donations?.donations
})

export default connect(mapStoreToProps, {
    loadDonationUser, deleteItem
})(DonationTable)

