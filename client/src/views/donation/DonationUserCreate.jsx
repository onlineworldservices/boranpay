import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadDataProfileUserPublic } from '../../redux/actions/auth/profileActions'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import DonationUserForm from './inc/DonationUserForm'

const DonationUserCreate = () => {
    const { user } = useParams()
    const userItem = useSelector(state => state?.profile?.user)
    const dispatch = useDispatch()
    useEffect(() => {
        const loadItems = () => {
            dispatch(loadDataProfileUserPublic(user))
        }
        loadItems()
    }, [user])
    return (
        <>
            <HelmetSite title={`New donation - ${userItem.firstName || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Donations"
                breadCrumbParent="Pages"
                breadCrumbActive={`Nouvelle donation pour - ${userItem.firstName || process.env.REACT_APP_NAME}`}
            />

            <DonationUserForm userData={userItem} />
        </>
    )
}

export default DonationUserCreate