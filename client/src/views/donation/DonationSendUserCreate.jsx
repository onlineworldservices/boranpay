import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import { authuserInfo } from '@components/service'
import HelmetSite from '@components/helmet/HelmetSite'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadShowDonationUser } from '../../redux/actions/pages/donationAction'
import DonationSendUserForm from "./inc/DonationSendUserForm"

const DonationSendUserCreate = () => {
    const { slugin } = useParams()
    const [userSite] = useState(authuserInfo())
    const item = useSelector(state => state.donations.donation)
    const dispatch = useDispatch()
    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowDonationUser(slugin))
        }
        loadItems()
    }, [slugin])
    return (
        <>
            <HelmetSite title={`Nouvelle contribution - ${item.title || process.env.REACT_APP_NAME}`} />
            <Breadcrumbs
                breadCrumbTitle="Contribution"
                breadCrumbParent="Sections"
                breadCrumbActive={`Contribution`}
            />
            <VerificationInfoUser userItem={userSite} />
            <DonationSendUserForm donation={item} userSite={userSite} />

        </>
    )
}

export default DonationSendUserCreate