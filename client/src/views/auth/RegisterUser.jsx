import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { AlertCircle } from 'react-feather'
import HelmetSite from '../../@core/components/helmet/HelmetSite'
import { Card, CardBody, CardTitle, Form, FormGroup, Label, Row, Col, Button, Alert } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCurrencies, loadAllAbilityforuserPermissions } from '../../redux/actions/pages/currencyAction'
import { loadAllCountries } from '../../redux/actions/pages/countryAction'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import '@styles/base/pages/page-auth.scss'
import { toast, Slide } from 'react-toastify'


const schema = yup.object().shape({
    firstName: yup.string().required().min(3).max(200),
    lastName: yup.string().required().min(3).max(200),
    currencyId: yup.number().required(),
    countryId: yup.number().required(),
    name: yup.string().required(),
    sex: yup.string().required().min(3).max(10),
    email: yup.string().email().required().min(3).max(200),
    password: yup.string().required().min(8).max(200),
    confirmPassword: yup.string().required().min(8).max(200)
        .oneOf([yup.ref('password'), null], 'Passwords must match')
})

const RegisterUser = () => {
    const history = useHistory()
    const { register, handleSubmit, formState } = useForm({
        resolver: yupResolver(schema)
    })
    const { errors, isSubmitting } = formState
    const [message, setMessage] = useState('')
    const currencies = useSelector(state => state.currencies.currencies)
    const countries = useSelector(state => state?.countries?.countries)
    const abilitipermissions = useSelector(state => state?.currencies?.abilitipermissions)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
            await dispatch(loadAllCountries())
            await dispatch(loadAllAbilityforuserPermissions())
        }
        loadItems()
    }, [])

    const onSubmit = async (data, e) => {

        try {
            const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/register`, data)
            if (response) {
                history.push("/login/")
                //window.location.reload()
            }
        } catch (error) { setMessage(error.response.data.message) }
    }

    return (
        <>
            <HelmetSite title={'Inscription'} />
            <Card className='mb-0'>
                <CardBody>
                    <Link className='brand-logo' to='/'>
                        <h2 className='brand-text text-primary ml-1'>{process.env.REACT_APP_NAME}</h2>
                    </Link>
                    <CardTitle tag='h4' className='text-center mb-1'>
                        Inscription
                    </CardTitle>
                    <div className="col-md-7 mx-auto">

                        <Form className='auth-login-form mt-2' onSubmit={handleSubmit(onSubmit)}>
                            {message && (
                                <Alert color='danger'>
                                    <div className='alert-body'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {message}
                                        </span>
                                    </div>
                                </Alert>
                            )}
                            <Row>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label className='form-label' for='lastName'>
                                            Prénom
                                        </Label>
                                        <input className={`form-control ${errors.lastName ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="lastName"
                                            placeholder="Prénom"
                                            {...register('lastName')}
                                            required="required" />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.lastName?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label className='form-label' for='firstName'>
                                            Nom
                                        </Label>
                                        <input className={`form-control ${errors.firstName ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="firstName"
                                            placeholder="Name"
                                            {...register('firstName')}
                                            required="required" />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.firstName?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label className='form-label' for='name'>
                                            Statut profil
                                        </Label>
                                        <select className={`form-control ${errors.name ? 'is-invalid' : ''}`} {...register('name')} id="name" required="required">
                                            <option value={''}>Choose status</option>
                                            {abilitipermissions.map((item, index) => <option key={index} value={item.name} >{item.label}</option>)}
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.name?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label className='form-label' for='currencyId'>
                                            Devise
                                            </Label>
                                        <select className={`form-control ${errors.currencyId ? 'is-invalid' : ''}`} {...register('currencyId')} id="currencyId" required="required">
                                            <option value={''}>Selectioner un devise</option>
                                            {currencies.map((item, index) => <option key={index} value={item.id} >{item.code}</option>)}
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.currencyId?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label className='form-label' for='sex'>
                                            Sexe
                                        </Label>
                                        <select className={`form-control ${errors.sex ? 'is-invalid' : ''}`} {...register('sex')} id="sex" required="required">
                                            <option value={''}>Sex</option>
                                            <option value="male" >Male</option>
                                            <option value="female" >Female</option>
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.sex?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label className='form-label' for='countryId'>
                                            Pays de residence
                                        </Label>
                                        <select className={`form-control ${errors.countryId ? 'is-invalid' : ''}`} {...register('countryId')} id="countryId" required="required">
                                            <option value={''}>Selectioner le pays</option>
                                            {countries.map((item, index) => <option key={index} value={item.id} >{item.name}</option>)}
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.countryId?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <FormGroup>
                                <Label className='form-label' for='email'>
                                    Adresse e-mail
                                        </Label>
                                <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                    type="email"
                                    id="email"
                                    placeholder="E-mail"
                                    {...register('email')}
                                    required="required" />
                                <span className='invalid-feedback'>
                                    <strong>{errors.email?.message}</strong>
                                </span>
                            </FormGroup>
                            <Row>
                                <Col md={'6'} sm={'6'}>
                                    <FormGroup>
                                        <div className='d-flex justify-content-between'>
                                            <Label className='form-label' for='password'>
                                                Mot de passe
                                            </Label>
                                        </div>
                                        <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                                            type="password"
                                            id="password"
                                            {...register('password')}
                                            placeholder="Password"
                                            autoComplete="off"
                                            required="required" />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.password?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md={'6'} sm={'6'}>
                                    <FormGroup>
                                        <div className='d-flex justify-content-between'>
                                            <Label className='form-label' for='confirmPassword'>
                                                Confirmer mot de passe
                                            </Label>
                                        </div>
                                        <input className={`form-control ${errors.confirmPassword ? 'is-invalid' : ''}`}
                                            type="password"
                                            {...register('confirmPassword')}
                                            id="confirmPassword"
                                            placeholder="Confirm password"
                                            required="required" />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.confirmPassword?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <br />
                            <Button.Ripple disabled={isSubmitting} type="submit" color='primary' block>
                                Inscription
                            </Button.Ripple>
                        </Form>
                        <p className='text-center mt-2'>
                            <span className='mr-25'>Déjà membre?</span>
                            <Link to='/login/'>
                                <span>Connexion</span>
                            </Link>
                        </p>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default RegisterUser