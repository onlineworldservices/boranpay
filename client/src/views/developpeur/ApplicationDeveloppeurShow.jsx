import React, { useState, useEffect, Fragment } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import HelmetSite from '@components/helmet/HelmetSite'
import {
  Card,
  Button,
  Row,
  Col,
  CardBody
} from 'reactstrap'
import InfoAppSourceCode from './inc/InfoAppSourceCode'
import TransactionDeveloppeurTable from './inc/TransactionDeveloppeurTable'
import { Edit, Key, CheckCircle, XOctagon } from 'react-feather'
import { useParams, useHistory } from 'react-router-dom'
import { loadShowApplicationDevelopperUse } from '../../redux/actions/pages/aplicationdeveloppeurAction'

const ApplicationDeveloppeurShow = () => {
  const { application } = useParams()
  const [statusSend, setStatusSend] = useState(false)
  const history = useHistory()
  /** Get currencies with redux */
  const applicationData = useSelector(state => state.developpeurs.application)
  const dispatch = useDispatch()

  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadShowApplicationDevelopperUse(application))
    }
    loadItems()
  }, [application])

  const toggleStatus = () => { setStatusSend(!statusSend) }

  const currencyUser = applicationData.user.profile.currency.symbol
  return (
    <>
      <HelmetSite title={`${applicationData.name || process.env.REACT_APP_NAME}`} />

      <Row className="match-height">

        <Col lg="6" md="6" sm="6">
          <Card className="bg-analytics">
            <CardBody className="text-center">
              <div className="d-flex justify-content-between">
                <h4 className="text-dark"><b>Compte vendeur</b></h4>
                <h4 className="text-dark"><b>Solde test</b></h4>
              </div>
              <br />
              <div className="text-center">
                <h1 className='font-weight-bolder mb-0'>
                  {applicationData.amountservices.length > 0 ? <>
                    <p className="text-dark" >{applicationData.amountservices.map((lk, index) => (<Fragment key={index} >{(lk.totalAmountserviceTest * applicationData.user.profile.currency.currencyNumber).formatMoney(2, '.', ',')} </Fragment>))}
                      {currencyUser}</p></> : <>0,00 {currencyUser}</>}
                </h1>
              </div>
            </CardBody>
          </Card>
        </Col>
        <Col lg="6" md="6" sm="6">
          <Card className="bg-analytics">
            <CardBody className="text-center">
              <div className="award-info text-center">
                <div className="d-flex justify-content-between">
                  <h4 className="text-dark"><b>Compte acheteur</b></h4>
                  <h4 className="text-dark"><b>Solde test</b></h4>
                </div>
                <br />
                <div className="text-center">
                  <h1 className='font-weight-bolder mb-0'>
                    {applicationData.amountservices.length > 0 ? <>
                      <p className="text-dark" >{applicationData.amountservices.map((lk, index) => (<Fragment key={index} >{(lk.totalAmountserviceBeyer * applicationData.user.profile.currency.currencyNumber).formatMoney(2, '.', ',')} </Fragment>))}
                        {currencyUser}</p></> : <>0,00 {currencyUser}</>}
                  </h1>
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>

      </Row>

      <div className='text-right' >
        <Button.Ripple className="mr-1 mb-1" color={applicationData.statusApplication ? 'success' : 'danger'} size="sm">
          {applicationData.statusApplication ? (
            <><CheckCircle size={14} /> <b>Application online</b></>
          ) : (
              <><XOctagon size={14} /> <b>Application offline</b></>
            )}
        </Button.Ripple>
      </div>
      <Card>
        <CardBody>
          <div className="col-md-12 mx-auto">
            <div className="tasks-info">
              <h4 className="mb-75">
                <strong>{`${applicationData.name}`}</strong>
                {console.log('applicationData ====>', applicationData.name)}
                <Edit size={14} onClick={() => history.push(`/developpeur/app/${applicationData.slugin}/edit/`)} className='cursor-pointer ml-1' />
                <Key size={14} onClick={toggleStatus} className='cursor-pointer ml-1' />
              </h4>
            </div>
            {statusSend && (
              <InfoAppSourceCode application={applicationData} />
            )}
          </div>
          <TransactionDeveloppeurTable items={applicationData?.transactionservices} />
        </CardBody>
      </Card>

    </>
  )
}

export default ApplicationDeveloppeurShow