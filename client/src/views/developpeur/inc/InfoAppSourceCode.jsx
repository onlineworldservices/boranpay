
import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Button
} from 'reactstrap'
import Prism from 'prismjs'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { SuccessToast } from '@components/toastalert'
import { toast } from 'react-toastify'

const InfoAppSourceCode = ({ application }) => {
  const [showsecret, setShowsecret] = useState(false)

  useEffect(() => {
    const loadItems = async () => {
      await Prism.highlightAll()
    }
    loadItems()
  }, [])

  const toggleStatus = () => { setShowsecret(o => !o) }

  const onCopy = () => {
    toast.success(
      <SuccessToast name={'Success'} description={'Copy successfully'} />, {
      position: toast.POSITION.TOP_RIGHT,
      hideProgressBar: true
    })
  }
  return (
    <>
      {/**
     *<Row>
        <Col md="4" sm="4">
          <h5 className="mb-75"><b>Sandbox API</b></h5>
          <pre>
            <code className='language-jsx'>
              {application.developeruser.email}
            </code>
          </pre>
        </Col>

      </Row>
     */}
      <Row>
        <Col md="6" sm="12">
          <h5 className="mb-75">
            <b>
              Private client ID
              <CopyToClipboard onCopy={onCopy}
                text={application.clientId} >
                <Button color="link">
                  Copy
                </Button>
              </CopyToClipboard>
            </b>
          </h5>
          <pre>
            <code className='language-jsx'>
              {application.clientId}
            </code>
          </pre>
        </Col>
        <Col md="6" sm="12">
          <h5 className="mb-75">
            <b>
              Secret key
              <Button.Ripple color="link" onClick={toggleStatus} >
                {showsecret ? 'Hide' : 'Show'}
              </Button.Ripple>
              {showsecret && (
                <CopyToClipboard onCopy={onCopy}
                  text={application.applicationsecretekey.secretKey} >
                  <Button color="link">
                    Copy
                  </Button>
                </CopyToClipboard>
              )}
            </b>
          </h5>
          <pre>
            <code className='language-jsx'>
              {showsecret && (
                <>
                  {application.applicationsecretekey.secretKey}
                </>
              )}
            </code>
          </pre>
        </Col>
      </Row>
      <Row>
        <Col md="12" sm="12">
          <h5 className="mb-75">
            <b>
              Link checkout
              <CopyToClipboard onCopy={onCopy}
                text={`${process.env.REACT_APP_LINK}/checkout_externe/${application.clientId}/`} >
                <Button color="link">
                  Copy
                </Button>
              </CopyToClipboard>
            </b>
          </h5>
          <pre>
            <code className='language-jsx'>
              {application.clientId}
            </code>
          </pre>
        </Col>

      </Row>
    </>
  )

}
export default InfoAppSourceCode