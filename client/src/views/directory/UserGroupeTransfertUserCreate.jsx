import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authHeader, authuserInfo } from '@components/service'
import {
  Card,
  CardBody,
  Alert,
  CardHeader,
  FormGroup,
  Row,
  Col,
  Form,
  Button,
  Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import { useParams, Link, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux"
import { loadShowUserGroupUser } from "../../redux/actions/pages/groupeAction"
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import Swal from 'sweetalert2'
import * as yup from 'yup'
import axios from "axios"
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import userServices from '../../services/userServices'

const schema = yup.object().shape({
  password: yup.string().required().min(8).max(200),
  total: yup.number().required().positive().integer()
})

const UserGroupeTransfertUserCreate = () => {
  const history = useHistory()
  const [userSite] = useState(authuserInfo())
  const [statusSend, setStatusSend] = useState(false)
  const { usergroupe } = useParams()
  const [errormessage, setErrormessage] = useState('')
  const {
    register,
    handleSubmit,
    formState
  } = useForm({ resolver: yupResolver(schema) })
  const { errors, isSubmitting } = formState
  { /** Get currencies with redux */ }
  const usergroupedata = useSelector(state => state?.groupes?.usergroupe)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadShowUserGroupUser(usergroupe))
    }
    loadItems()
  }, [])

  const toggleStatus = () => { setStatusSend(!statusSend) }

  const onSubmit = async (data, e) => {
    Swal.fire({
      title: 'Confirmez-vous la transaction ?',
      text: "Êtes-vous sûr de vouloir executer cette action?",
      confirmButtonText: 'Oui, Confirmer',
      cancelButtonText: 'Non, annuler',
      buttonsStyling: false,
      confirmButtonClass: "btn btn-primary",
      cancelButtonClass: 'btn btn-outline-default',
      showCancelButton: true,
      reverseButtons: false,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return userServices.userCheckValidate(userSite, data)
          .then(async () => {

            try {
              const response = await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/ug_transfert/${usergroupe}/transfert`, data, { headers: authHeader() })
              if (response) {
                toast.success(
                  <SuccessToast name={'Success'} description={`Transfert éffectué pour ${usergroupedata.fullName}`} />, {
                  position: toast.POSITION.TOP_RIGHT,
                  hideProgressBar: true
                })
                history.goBack()
              }
            } catch (error) {
              toast.error(
                <ErrorToast name={'Danger'} description={'Oops! Quelque chose ne va pas.'} />, {
                position: toast.POSITION.TOP_RIGHT,
                hideProgressBar: true
              })
              setErrormessage(error.response.data.message)
            }

          }).catch(error => {
            Swal.showValidationMessage(`${error.response.data.message}`)
          })
      },
      allowOutsideClick: () => !Swal.isLoading()
    })
  }

  return (
    <>

      <HelmetSite title={`Nouveau transfert - ${usergroupedata.fullName || process.env.REACT_APP_NAME}`} />
      <Breadcrumbs
        breadCrumbTitle="Transfert"
        breadCrumbParent="Sections"
        breadCrumbActive={`Nouveau transfert - ${usergroupedata.fullName || process.env.REACT_APP_NAME}`}
      />
      {/** Undo redo */}
      <UndoRedo />

      <Card>
        <CardBody>
          <div className="col-md-10 mx-auto">
            <div className="tasks-info">
              <h4 className="mb-75">
                <strong>Transfert pour - {usergroupedata.fullName}  </strong>
              </h4>
            </div>
            <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

              {errormessage && (
                <Alert color='danger'>
                  <div className='alert-body text-center'>
                    <AlertCircle size={15} />{' '}
                    <span className='ml-1'>
                      {errormessage}
                    </span>
                  </div>
                </Alert>
              )}

              <br />
              <Row>

                <Col md="8" sm="8">
                  <FormGroup>
                    <Label for="total"><strong>Montant de la transaction</strong></Label>
                    <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                      type="number"
                      pattern="[0-9]*"
                      inputMode="numeric"
                      min="1" step="1"
                      id="total"
                      placeholder="Montant de la transaction"
                      {...register('total')}
                      autoComplete="off"
                      required
                    />
                    <span className='invalid-feedback'>
                      <strong>{errors.total?.message}</strong>
                    </span>
                  </FormGroup>
                </Col>
                <Col md="4" sm="4">
                  <FormGroup>
                    <Label for="currency"><strong>Devise</strong></Label>
                    <input className={`form-control ${errors.currency ? 'is-invalid' : ''}`}
                      type="text"
                      name="currency"
                      id="currency"
                      defaultValue={usergroupedata.currency}
                      disabled
                    />
                    <small ><Label><Link to={`/ugs/${usergroupedata.slugin}/edit`} className="text-right">Changer la devise</Link></Label></small>
                  </FormGroup>
                </Col>

                <Col sm="12">
                  <FormGroup>
                    <Label for="content"><strong>Motif de l'envoie</strong></Label>
                    <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                      type="text"
                      rows="2"
                      id="content"
                      {...register('content')}
                      placeholder="Message privé ..."
                    />
                    <span className='invalid-feedback'>
                      <strong>{errors.content?.message}</strong>
                    </span>
                  </FormGroup>
                </Col>
                {statusSend && (
                  <Col sm={12}>
                    <FormGroup>
                      <Label for="password"><strong>Veuillez saisir votre mot de passe</strong></Label>
                      <input className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                        type="password"
                        id="password"
                        {...register('password')}
                        placeholder="Mot de passe pour valider la transaction" required />

                      <span className='invalid-feedback'>
                        <strong>{errors.password?.message}</strong>
                      </span>
                    </FormGroup>
                  </Col>
                )}
                <div className="col-md-12 text-center">
                  <FormGroup className="form-label-group">{statusSend ? (
                    <Button.Ripple
                      disabled={isSubmitting}
                      type="submit"
                      className="btn-block"
                      color="primary"
                      block
                    >
                      Terminer
                    </Button.Ripple>
                  ) : (
                      <Button.Ripple
                        className="btn-block"
                        color="primary"
                        onClick={toggleStatus}
                        block
                      >
                        Continuer
                      </Button.Ripple>
                    )}
                  </FormGroup>
                </div>
                <div className="col-md-12 text-center">
                  <a href={void (0)} onClick={history.goBack} className="nav-link"><b>Annuler</b></a>
                </div>
                <div className="col-md-12 text-center">
                  Vérifier attentivement <strong>les informations</strong> du destinataire
                     sans oublier la devise et le montant de la transaction avant de tranférer cette somme
                </div>

              </Row>
            </Form>
          </div>
        </CardBody>
      </Card>

    </>
  )
}
export default UserGroupeTransfertUserCreate
