import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { Plus } from 'react-feather'
import { authuserInfo } from '@components/service'
import { useHistory } from 'react-router-dom'
import DirectoryTable from './inc/DirectoryTable'
import { Col, Row, Button } from 'reactstrap'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'

const DirectoryUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Répertoire - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Répertoire"
                breadCrumbParent="Sections"
                breadCrumbActive={`Répertoire ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            {/** Undo redo */}
            <UndoRedo />

            <Button.Ripple onClick={() => history.push(`/ugs/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouveau contact</b>
            </Button.Ripple>
            <Row>
                <Col sm='12'>
                    <DirectoryTable {...userSite} />
                </Col>
            </Row>
        </>
    )
}

export default DirectoryUser