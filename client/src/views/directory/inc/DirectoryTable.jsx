import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Avatar from '@components/avatar'
import {
    Eye,
    Send,
    MoreVertical,
    Edit,
    Trash,
    Activity,
    ChevronDown
} from 'react-feather'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Card, CardBody, CardHeader, UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { loadAllUserDirectory, deleteusergrpItem } from '../../../redux/actions/pages/groupeAction'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item.user.avatar) {
        return <Avatar className='mr-50' img={item.user.avatar || 'https://ivemo-cm.s3.eu-central-1.amazonaws.com/img/locations/de2a5dc5106a1d9d1a250251f78436c16c7923b7.png'} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={(item.user && item.user.firstName) || 'Boclair Temgoua'} initials />
    }
}

class DirectoryTable extends Component {

    async componentDidMount() {
        await this.props.loadAllUserDirectory(this.props)
    }

    render() {
        const { items } = this.props
        const columns = [
            {
                name: <h5>Profil</h5>,
                selector: "avatar_name",
                sortable: true,
                grow: 4
            },
            {
                name: <h5>Nom | devise</h5>,
                selector: "full_name",
                sortable: true,
                grow: 3
            },
            {
                name: <h5>Téléphone</h5>,
                selector: "phone",
                sortable: true,
                grow: 3
            },
            {
                name: <h5>Ville</h5>,
                selector: "district",
                sortable: true,
                grow: 3
            },
            {
                name: "",
                selector: "row_button",
                ignoreRowClick: true,
                allowOverflow: true,
                button: true
            }
        ]

        const data = items?.length >= 0 ? (
            items.map(item => (
                {
                    avatar_name: (
                        <>
                            <div className='d-flex justify-content-left align-items-center'>
                                {renderClient(item)}
                                <div className='d-flex flex-column'>
                                    <h6 className='user-name text-truncate mb-0'>{item.user.firstName} {item.user.lastName}</h6>
                                    <small className='text-truncate text-muted mb-0'>{item.user.email}</small>
                                </div>
                            </div>
                        </>
                    ),
                    full_name: <Link to={`/transaction/send/${this.props.slugin}/u/${item.user.slugin}/`} className="text-dark"><b>{item.fullName} | {item.currency}</b></Link>,
                    phone: <Link to={`/transaction/send/${this.props.slugin}/u/${item.user.slugin}/`} className="text-dark"><b>{item.phone}</b></Link>,
                    district: <Link to={`/transaction/send/${this.props.slugin}/u/${item.user.slugin}/`} className="text-dark"><b>{item.district}</b></Link>,
                    row_button: (
                        <>
                            <div className='column-action d-flex align-items-center'>
                                <Link to={`/ug_transfert/${item.slugin}/transfert`}>
                                    <Send size={17} />
                                </Link>
                                <Link to={`/transaction/send/${this.props.slugin}/u/${item.user.slugin}/`}>
                                    <Eye size={17} className='mx-1' />
                                </Link>
                                <UncontrolledDropdown>
                                    <DropdownToggle tag='span'>
                                        <MoreVertical size={17} className='cursor-pointer' />
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem tag={Link} to={`/transaction/send/${this.props.slugin}/u/${item.user.slugin}/`} className='w-100'>
                                            <Activity size={14} className='mr-50' />
                                            <span className='align-middle'>Transactions</span>
                                        </DropdownItem>
                                        <DropdownItem tag={Link} to={`/ugs/${item.slugin}/edit`} className='w-100'>
                                            <Edit size={14} className='mr-50' />
                                            <span className='align-middle'>Editer</span>
                                        </DropdownItem>
                                        <DropdownItem tag='a' href={void (0)} className='w-100' onClick={() => this.props.deleteusergrpItem(item)}>
                                            <Trash size={14} className='mr-50' />
                                            <span className='align-middle'>Supprimer</span>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </div>
                        </>
                    )
                }
            )
            )
        ) : (
                <></>
            )

        return (
            <Card>

                <CardHeader className='flex-md-row flex-column align-md-items-center align-items-start border-bottom'>
                    <div className="tasks-info">
                        <h3 className="mb-75">
                            <strong>Répertoire  </strong>
                        </h3>
                    </div>
                </CardHeader>

                <CardBody>
                    <br />
                    <DataTable
                        className='react-dataTable'
                        sortIcon={<ChevronDown size={10} />}
                        paginationRowsPerPageOptions={[10, 25, 50, 100]}
                        data={data}
                        columns={columns}
                        defaultSortField="createdAt"
                        defaultSortAsc={false}
                        overflowY={true}
                        pagination={true}
                        noHeader />
                </CardBody>
            </Card>
        )
    }
}
DirectoryTable.propTypes = {
    loadAllUserDirectory: PropTypes.func.isRequired
}

const mapStoreToProps = store => ({
    items: store?.groupes?.usergroupes
})

export default connect(mapStoreToProps, {
    loadAllUserDirectory, deleteusergrpItem
})(DirectoryTable)

