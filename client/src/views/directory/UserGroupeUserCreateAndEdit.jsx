import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authHeader } from '@components/service'
import {
    Card,
    CardBody,
    Alert,
    FormGroup,
    Row,
    Col,
    Form,
    Button,
    Label
} from "reactstrap"
import { AlertCircle, Plus } from 'react-feather'
import { useParams, useHistory } from 'react-router-dom'
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import axios from "axios"
import { loadAllCurrencies } from "../../redux/actions/pages/currencyAction"
import { SuccessToast, ErrorToast } from '@components/toastalert'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { toast } from 'react-toastify'

const MySwal = withReactContent(Swal)
const schema = yup.object().shape({
    fullName: yup.string().required().min(3).max(200),
    email: yup.string().email().min(3).max(200),
    currency: yup.string().required().min(3).max(200),
    phone: yup.number().required()
})

const UserGroupeUserCreateAndEdit = () => {
    const { usergroupe } = useParams()
    const history = useHistory()
    const isAddMode = !usergroupe
    const {
        register,
        handleSubmit,
        reset,
        setValue,
        formState
    } = useForm({ resolver: yupResolver(schema) })
    const { errors, isSubmitting } = formState
    { /** Get currencies with redux */ }
    const [errormessage, setErrormessage] = useState('')

    const currencies = useSelector(state => state?.currencies?.currencies)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
        }
        loadItems()
    }, [])

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
            if (!isAddMode) {
                axios.get(`${process.env.REACT_APP_SERVER_NODE_URL}/ugs/${usergroupe}`, { headers: authHeader() })
                    .then(groupe => {
                        const fields = ['fullName', 'currency', 'phone', 'district']
                        fields.forEach(field => setValue(field, groupe.data[field]))
                    })
            }
        }
        loadItems()
    }, [usergroupe])


    const createItem = async (data, e) => {
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/ugs`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Contact sauvegardé avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                e.target.reset()
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Danger'} description={'Oops! Quelque chose ne va pas.'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const updateItem = async (usergroupe, data) => {
        await axios.put(`${process.env.REACT_APP_SERVER_NODE_URL}/ugs/${usergroupe}`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Contact àjourné avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                location.reload()
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Danger'} description={'Oops! Quelque chose ne va pas.'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const onSubmit = (data, e) => {
        return isAddMode ? createItem(data, e) : updateItem(usergroupe, data)
    }

    const deleteItem = (usergroupe) => {
        return MySwal.fire({
            title: 'Suppression?',
            text: "Êtes-vous sûr de vouloir exécuter cette action?",
            confirmButtonText: 'Oui, supprimer',
            cancelButtonText: 'Non, annuler',
            showCancelButton: true,
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-default ml-1'
            },
            reverseButtons: false,
            buttonsStyling: false
        }).then(async (result) => {
            if (result.value) {

                //Envoyer la requet au server
                await axios.delete(`${process.env.REACT_APP_SERVER_NODE_URL}/ugs/${usergroupe}`, { headers: authHeader() }).then(() => {
                    toast.success(
                        <SuccessToast name={'Success'} description={'Contact supprimer avec succès'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                    history.push('/directories/')
                }).catch(() => {
                    toast.error(
                        <ErrorToast name={'Opp error'} description={'Une érreur est survénue essayez plus tard ...'} />, {
                        position: toast.POSITION.TOP_RIGHT,
                        hideProgressBar: true
                    })
                })
            }
        })
    }

    return (
        <>
            <HelmetSite title={`${isAddMode ? `Nouveau contact` : `Editer contact`}`} />
            <Breadcrumbs
                breadCrumbTitle="Répertoire "
                breadCrumbParent="Sections"
                breadCrumbActive={isAddMode ? `Nouveau contact` : `Editer contact`}
            />
            {/** Undo redo */}
            <UndoRedo />

            {!isAddMode && (
                <Button.Ripple onClick={() => history.push(`/ugs/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                    <Plus size={14} /> <b>Nouveau contact</b>
                </Button.Ripple>
            )}
            <Card>

                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <div className="tasks-info">
                            <h4 className="mb-75">
                                <strong>{`${isAddMode ? `Nouveau contact` : `Editer contact`}`}</strong>
                            </h4>
                        </div>
                        <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

                            {errormessage && (
                                <Alert color='danger'>
                                    <div className='alert-body text-center'>
                                        <AlertCircle size={15} />{' '}
                                        <span className='ml-1'>
                                            {errormessage}
                                        </span>
                                    </div>
                                </Alert>
                            )}
                            <br />
                            <Row>
                                <Col md="8" sm="8">
                                    <FormGroup>
                                        <Label for="fullName"><strong>Nom complet</strong></Label>
                                        <input className={`form-control ${errors.fullName ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="fullName"
                                            {...register('fullName')}
                                            placeholder="Nom complet..."
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.fullName?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="4" sm="4">
                                    <FormGroup>
                                        <Label for="currency"><strong>Devise</strong></Label>
                                        <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`}
                                            {...register('currency')} id="currency" required="required">
                                            <option value="">Choisissez la devise</option>
                                            {currencies.map((item, index) => (
                                                <option key={index} value={item.code} >{item.code}</option>
                                            ))}
                                        </select>
                                        <span className='invalid-feedback'>
                                            <strong>{errors.currency?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label for="district"><strong>Numéro de téléphone</strong></Label>
                                        <input className={`form-control ${errors.phone ? 'is-invalid' : ''}`}
                                            type="tel"
                                            id="phone"
                                            minLength="8"
                                            maxLength="20"
                                            {...register('phone')}
                                            placeholder="Numéro de téléphone..."
                                            required
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.phone?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>
                                <Col md="6" sm="6">
                                    <FormGroup>
                                        <Label for="district"><strong>Ville - Localite</strong></Label>
                                        <input className={`form-control ${errors.district ? 'is-invalid' : ''}`}
                                            type="text"
                                            id="district"
                                            minLength="3"
                                            maxLength="200"
                                            {...register('district')}
                                            placeholder="Ville..."
                                        />
                                        <span className='invalid-feedback'>
                                            <strong>{errors.district?.message}</strong>
                                        </span>
                                    </FormGroup>
                                </Col>

                                {isAddMode && (
                                    <Col sm="12">
                                        <FormGroup className="form-label-group">
                                            <input className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                                                type="email"
                                                id="email"
                                                placeholder="Adresse e-mail..."
                                                {...register('email')}
                                                autoComplete="false" required />
                                            <Label for="email"><strong>Adresse e-mail</strong></Label>
                                            <span className='invalid-feedback'>
                                                <strong>{errors.email?.message}</strong>
                                            </span>
                                        </FormGroup>
                                    </Col>
                                )}

                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            disabled={isSubmitting}
                                            type="submit"
                                            className="btn-block"
                                            color="primary"
                                        >
                                            Sauvegarder
                                        </Button.Ripple>
                                        <Button.Ripple
                                            onClick={history.goBack}
                                            className="btn-block"
                                            color="default"
                                        >
                                            Annuler
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>
                                {!isAddMode && (
                                    <div className="col-md-12 text-center">
                                        <a href={void (0)} onClick={() => deleteItem(usergroupe)} className="nav-link text-danger" ><b>Supprimer</b></a>
                                    </div>
                                )}
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        </>
    )
}

export default UserGroupeUserCreateAndEdit