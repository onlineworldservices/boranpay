import React from 'react'
import UndoRedo from '@components/undoredo'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'

const FaqUser = () => {
    return (
        <>
            <HelmetSite title={`FAQS`} />
            <Breadcrumbs
                breadCrumbTitle="FAQS"
                breadCrumbParent="Support"
                breadCrumbActive={`FAQS`}
            />
            {/** Undo redo */}
            <UndoRedo />
        </>
    )
}

export default FaqUser