import React, { Component } from 'react'
import {
    Card,
    CardBody,
    FormGroup,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactQuill from 'react-quill'
import FieldInput from '@components/editor/FieldInput'
import { loadAllCurrencies } from "../../../redux/actions/pages/currencyAction"
import axios from "axios"
import { authHeader } from '@components/service'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import 'react-quill/dist/quill.snow.css'

class CagnoteUserForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            title: '',
            currency_id: '',
            total: '',
            content: '',
            errors: [],
            currencies: []
        }
        this.modules = {
            toolbar: [
                ['bold', 'italic', 'underline'],
                [{ list: 'ordered' }, { list: 'bullet' }],
                [{ align: [] }],
                ['link'],
                [{ color: [] }, { background: [] }]
            ]
        }
        this.formats = [
            'size',
            'bold', 'italic', 'underline',
            'list', 'bullet',
            'align',
            'link',
            'color', 'background'
        ]

        this.saveItem = this.saveItem.bind(this)
        this.handleFieldChange = this.handleFieldChange.bind(this)
        this.handleChangeBody = this.handleChangeBody.bind(this)
        this.hasErrorFor = this.hasErrorFor.bind(this)
        this.renderErrorFor = this.renderErrorFor.bind(this)


    }
    // Handle Change
    handleChangeBody(value) {
        this.setState({ content: value })
    }

    handleFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
        this.state.errors[event.target.name] = ''
    }
    // Handle Errors
    hasErrorFor(field) {
        return !!this.state.errors[field]
    }

    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.errors[field][0]}</strong>
                </span>
            )
        }
    }

    async saveItem(e) {
        e.preventDefault()

        const item = {
            title: this.state.title,
            currency_id: this.state.currency_id,
            total: this.state.total,
            content: this.state.content
        }
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/cagnotes`, item, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Cagnote créée avec succès'} />, {
                    position: toast.POSITION.TOP_CENTER,
                    hideProgressBar: true
                })
                window.location.replace(`${process.env.REACT_APP_LINK}/mycagnotes/`)
            }).catch((error) => {
                this.setState({
                    errors: error.response.data.errors
                })
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
            })
    }

    async componentDidMount() {
        await this.props.loadAllCurrencies()
    }

    render() {
        const { currencies } = this.props
        return (
            <Card>
                <CardBody>
                    <div className="col-md-10 mx-auto">
                        <Form className="mt-2" onSubmit={this.saveItem}>

                            <br />
                            <Row>

                                <Col sm="12">
                                    <FormGroup>
                                        <Label for="title"><strong>Donner un titre</strong></Label>
                                        <FieldInput name="title" type='text' minLength="5" maxLength="200" placeholder="Titre de la cagnote" value={this.state.title}
                                            handleFieldChange={this.handleFieldChange}
                                            hasErrorFor={this.hasErrorFor}
                                            renderErrorFor={this.renderErrorFor} required="required" />
                                    </FormGroup>
                                </Col>

                                <Col md="8" sm="6">
                                    <FormGroup>
                                        <Label for="total"><strong>Cible de la cagnote</strong></Label>
                                        <FieldInput name="total" type='number' min="1" step="1" placeholder="Donner une cible à atteindre" value={this.state.total}
                                            handleFieldChange={this.handleFieldChange}
                                            hasErrorFor={this.hasErrorFor}
                                            renderErrorFor={this.renderErrorFor} required="required" />
                                    </FormGroup>
                                </Col>

                                <Col md="4" sm="6">
                                    <FormGroup>
                                        <Label for="currencyId">Devise</Label>
                                        <select className={`form-control`} name="currency_id"
                                            onChange={this.handleFieldChange}
                                            value={this.state.currency_id}
                                            id="currency_id" required="required">
                                            <option value="" disabled>
                                                Sélectionner une devise
                                            </option>
                                            {currencies.map((item) => (
                                                <option key={item.id} value={item.id} >{item.code}</option>
                                            ))}
                                        </select>
                                        {this.renderErrorFor('currency_id')}
                                    </FormGroup>
                                </Col>

                                <Col sm='12'>
                                    <FormGroup className='mb-2'>
                                        <Label>Content</Label>
                                        <ReactQuill theme="snow" modules={this.modules}
                                            formats={this.formats}
                                            className={`editor-control ${this.hasErrorFor('content') ? 'is-invalid' : ''}`}
                                            value={this.state.content || ''}
                                            onChange={this.handleChangeBody} />
                                        {this.renderErrorFor('content')}
                                    </FormGroup>
                                </Col>
                                <div className="col-md-12 text-center">
                                    <FormGroup className="form-label-group">
                                        <Button.Ripple
                                            type="submit"
                                            className="btn-block"
                                            color="primary"
                                        >
                                            Sauvegarder
                                        </Button.Ripple>
                                        <Button.Ripple
                                            onClick={() => history.push(`/mycagnotes/`)}
                                            className="btn-block"
                                            color="default"
                                        >
                                            Annuler
                                        </Button.Ripple>
                                    </FormGroup>
                                </div>
                                <div className="col-md-12 text-center">
                                    Verifier attentivement <strong>la cible et la devise</strong> puis donner une bonne description à votre cagnote avant toute sauvegarde.
                                </div>
                            </Row>
                        </Form>
                    </div>
                </CardBody>
            </Card>
        )
    }
}
CagnoteUserForm.propTypes = {
    loadAllCurrencies: PropTypes.func.isRequired
}

const mapStoreToProps = store => ({
    currencies: store?.currencies.currencies
})

export default connect(mapStoreToProps, {
    loadAllCurrencies
})(CagnoteUserForm)