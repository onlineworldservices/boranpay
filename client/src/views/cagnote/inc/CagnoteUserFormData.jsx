import React, { useState, useEffect } from 'react'
import {
    Card,
    CardBody,
    Alert,
    FormGroup,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import ReactQuill from 'react-quill'
import { useHistory, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllCurrencies } from "../../../redux/actions/pages/currencyAction"
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { SuccessToast, ErrorToast } from '@components/toastalert'
import { toast } from 'react-toastify'
import cagnoteApiServices from '../../../services/cagnoteApiServices'

const schema = yup.object().shape({
    title: yup.string().required().min(3).max(200),
    total: yup.string().required(),
    currencyId: yup.number().required()
})

const CagnoteUserFormData = ({ userSite }) => {
    const history = useHistory()
    const { cagnoteslugin } = useParams()
    const isAddMode = !cagnoteslugin
    const {
        register,
        handleSubmit,
        reset,
        setValue,
        control,
        formState
    } = useForm({ resolver: yupResolver(schema), mode: "onChange" })
    const { errors, isSubmitting } = formState
    /** Get currencies with redux */
    const [errormessage, setErrormessage] = useState('')
    const [cagnote, setCagnote] = useState({})
    const currencies = useSelector(state => state.currencies.currencies)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
        }
        loadItems()
    }, [])

    useEffect(() => {
        const loadItems = async () => {
            if (!isAddMode) {
                // get user and set form fields
                await cagnoteApiServices.getShowEditCagnote(cagnoteslugin, userSite)
                    .then(cagnote => {
                        const fields = ['title', 'currencyId', 'total', 'content']
                        fields.forEach(field => setValue(field, cagnote.data[field]))
                        setCagnote(cagnote)
                    })
            }
        }
        loadItems()
    }, [cagnoteslugin, userSite.slugin])

    const updateItem = async (cagnoteslugin, data) => {
        await cagnoteApiServices.updateCagnote(cagnoteslugin, data)
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Données mis à jour avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/mycagnotes/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const createItem = async (data, e) => {
        //console.log(data)
        // const file = data.photo[0]
        // const storageRef = app.storage().ref()
        //const fileRef = storageRef.child(file.name)
        //fileRef.put(file).then(() => {
        //  console.log("Uploaded a file");
        //});

        await cagnoteApiServices.postCagnote(data)
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Cagnote créée avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                history.push('/mycagnotes/')
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    const onSubmit = (data) => {
        return isAddMode ? createItem(data) : updateItem(cagnoteslugin, data)
    }

    return (
        <Card>
            <CardBody>
                <div className="col-md-10 mx-auto">
                    <Form className="mt-2" onSubmit={handleSubmit(onSubmit)} onReset={reset}>

                        {errormessage && (
                            <Alert color='danger'>
                                <div className='alert-body'>
                                    <AlertCircle size={15} />{' '}
                                    <span className='ml-1'>
                                        {errormessage}
                                    </span>
                                </div>
                            </Alert>
                        )}

                        <br />
                        <Row>

                            <Col sm="12">
                                <FormGroup>
                                    <Label for="title"><strong>Donner un titre</strong></Label>
                                    <input className={`form-control ${errors.title ? 'is-invalid' : ''}`}
                                        type="text"
                                        id="title"
                                        {...register('title')}
                                        placeholder="titre"
                                        required
                                    />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.title?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>

                            <Col md="8" sm="6">
                                <FormGroup>
                                    <Label for="total"><strong>Montant cagnote</strong></Label>
                                    <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                                        type="number"
                                        pattern="[0-9]*"
                                        inputMode="numeric"
                                        min="1" step="1"
                                        id="total"
                                        placeholder="Donner une cible à atteindre..."
                                        {...register('total')}
                                        autoComplete="off"
                                    />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.total?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>
                            <Col md="4" sm="6">
                                <FormGroup>
                                    <Label for="currencyId"><strong>Devise</strong></Label>
                                    <select className={`form-control ${errors.currencyId ? 'is-invalid' : ''}`} {...register('currencyId')} id="currencyId" required="required">
                                        <option value=''>Selectioner une devise</option>
                                        {currencies.map((item) => (
                                            <option key={item.id} value={item.id} >{item.code}</option>
                                        ))}
                                    </select>
                                    <span className='invalid-feedback'>
                                        <strong>{errors.currencyId?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>
                            {/**
                             * 
                            <Col md="12" sm="12">
                                <FormGroup>
                                    <Label for="photo">Image:</Label>
                                    <input
                                        type="file"
                                        autoComplete="off"
                                        id="photo"
                                        {...register('photo')}
                                    />
                                    <span className='invalid-feedback'>
                                        <strong>{errors.photo?.message}</strong>
                                    </span>
                                </FormGroup>
                            </Col>
                             */}
                            <Col md="12" sm="12">
                                <FormGroup>
                                    <Label className='form-label' for='content'>
                                        <strong>Description de votre cagnote</strong>
                                    </Label>
                                    <Controller
                                        name="content"
                                        control={control}
                                        defaultValue={''}
                                        render={({ field }) => (
                                            <ReactQuill
                                                {...field}
                                                theme="snow"
                                                initialValue={`${process.env.REACT_APP_NAME}`}
                                                modules={{
                                                    toolbar: [
                                                        ['bold', 'italic', 'underline'],
                                                        [{ list: 'ordered' }, { list: 'bullet' }],
                                                        [{ align: [] }],
                                                        ['link'],
                                                        [{ color: [] }, { background: [] }]
                                                    ]
                                                }}
                                            />
                                        )}
                                    />
                                    {errors.content && (
                                        <span className='invalid-feedback'>
                                            <strong>{errors.content?.message}</strong>
                                        </span>
                                    )}
                                </FormGroup>
                            </Col>
                            <div className="col-md-12 text-center">
                                <FormGroup className="form-label-group">
                                    <Button.Ripple
                                        disabled={isSubmitting}
                                        type="submit"
                                        className="btn-block"
                                        color="primary">
                                        Sauvegarder
                                    </Button.Ripple>
                                    <Button.Ripple
                                        onClick={() => history.push(`/mycagnotes/`)}
                                        className="btn-block"
                                        color="default">
                                        Annuler
                                    </Button.Ripple>
                                </FormGroup>
                            </div>
                            <div className="col-md-12 text-center">
                                Verifier attentivement <strong>le montant de votre cagnote, la devise</strong> puis donner une bonne description à votre cagnote avant toute sauvegarde.
                            </div>
                        </Row>
                    </Form>
                </div>
            </CardBody>
        </Card>
    )
}
export default CagnoteUserFormData