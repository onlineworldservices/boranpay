import React, { useEffect, useState } from 'react'
import {
  FormGroup,
  Label
} from 'reactstrap'
import CouponCagnoteForm from './CouponCagnoteForm'
import StripeCagnoteContainer from './StripeCagnoteContainer'
import { useDispatch, useSelector } from 'react-redux'
import { loadAllPayementmethodsrechage } from '../../../../redux/actions/pages/countryAction'


const SelectedContributionMethods = ({ cagnote }) => {
  const [selectedMethode, setSelectedMethode] = useState()
  const payementmethods = useSelector(state => state?.countries?.payementmethods)
  const dispatch = useDispatch()
  useEffect(() => {
    const loadItems = async () => {
      await dispatch(loadAllPayementmethodsrechage())
    }
    loadItems()
  }, [])

  return (
    <>
      <div className="col-md-12 mx-auto">
        <FormGroup>
          <Label><b>Sélectioner une methode</b></Label>
          <select className={`form-control`} name={'methode'} value={selectedMethode} onChange={(e) => setSelectedMethode(e.target.value)}>
            <option value='' >Choose method</option>
            {payementmethods.map((item, index) => (
              <option key={index} value={item.slug}>{item.name}</option>
            ))}
          </select>
        </FormGroup>

        {selectedMethode === 'coupons' && (
          <CouponCagnoteForm cagnote={cagnote} />
        )}

        {selectedMethode === 'stripes' && (
          <StripeCagnoteContainer cagnote={cagnote} />
        )}


      </div>
    </>
  )
}

export default SelectedContributionMethods