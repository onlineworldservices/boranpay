import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'
import RechargeStripeForm from "./RechargeStripeForm"

const PUBLIC_KEY = `${process.env.REACT_APP_STRIPE_PUBLIC_KEY}`
const stripeTestPromise = loadStripe(PUBLIC_KEY)
const StripeCagnoteContainer = () => {
    return (
        <Elements stripe={stripeTestPromise}>
            <RechargeStripeForm />
        </Elements>
    )
}
export default StripeCagnoteContainer