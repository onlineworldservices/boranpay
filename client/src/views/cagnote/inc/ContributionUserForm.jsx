import React, { useState, useEffect } from 'react'
import {
    Alert,
    Row,
    Col,
    Form,
    Button,
    Label
} from 'reactstrap'
import { AlertCircle } from 'react-feather'
import SelectedContributionMethods from './contributionmethods/SelectedContributionMethods'
import { authHeader } from '@components/service'
import { useDispatch, useSelector } from "react-redux"
import { loadAllCurrencies } from "../../../redux/actions/pages/currencyAction"
import { useForm } from "react-hook-form"
import { yupResolver } from '@hookform/resolvers/yup'
import { SuccessToast, ErrorToast } from '@components/toastalert'
//import Checkbox from "../../../components/@vuexy/checkbox/CheckboxesHook";
//import { Check } from "react-feather";
import * as yup from 'yup'
import axios from "axios"
import { toast } from 'react-toastify'

const schema = yup.object().shape({
    // total: yup.number().required().positive().integer(),
    total: yup.string().required(),
    currency: yup.string().required().min(3).max(5),
    content: yup.string().max(5000)
})

const ContributionUserForm = ({ cagnoteItem }) => {
    const [statusPayement, setStatusPayement] = useState(false)
    const [errormessage, setErrormessage] = useState('')
    const {
        register,
        handleSubmit,
        formState
    } = useForm({ resolver: yupResolver(schema), mode: "onChange" })
    const { errors, isSubmitting } = formState
    /** Get currencies with redux */
    const currencies = useSelector(state => state.currencies.currencies)
    const dispatch = useDispatch()
    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadAllCurrencies())
        }
        loadItems()
    }, [])

    const toggleStatus = () => { setStatusPayement(!statusPayement) }

    const onSubmit = async (data, e) => {
        await axios.post(`${process.env.REACT_APP_SERVER_NODE_URL}/cagnote/${cagnoteItem.slug}/contribute`, data, { headers: authHeader() })
            .then((res) => {
                toast.success(
                    <SuccessToast name={'Success'} description={'Contribution envoyée avec succès'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                e.target.reset()
                window.scroll({ top: 0, left: 0, behavior: 'smooth' })
            }).catch((error) => {
                toast.error(
                    <ErrorToast name={'Opp error'} description={'Oops! Quelque chose ne va pas ...'} />, {
                    position: toast.POSITION.TOP_RIGHT,
                    hideProgressBar: true
                })
                setErrormessage(error.response.data.message)
            })
    }

    return (
        <>
            {!statusPayement && (
                <Form className="mt-2" onSubmit={handleSubmit(onSubmit)}>

                    <fieldset className="form-label-group mb-50">
                        {errormessage && (
                            <Alert color='danger'>
                                <div className='alert-body'>
                                    <AlertCircle size={15} />{' '}
                                    <span className='ml-1'>
                                        {errormessage}
                                    </span>
                                </div>
                            </Alert>
                        )}
                    </fieldset>
                    <Row>
                        <Col md={8} sm={6}>
                            <fieldset>
                                <Label htmlFor="total"><strong>Montant de la contribution</strong></Label>
                                <input className={`form-control ${errors.total ? 'is-invalid' : ''}`}
                                    type="number"
                                    pattern="[0-9]*"
                                    inputMode="numeric"
                                    min="1" step="1"
                                    id="total"
                                    placeholder="Montant de la contribution"
                                    {...register('total')}
                                    autoComplete="off"
                                    required
                                />
                                <span className='invalid-feedback'>
                                    <strong>{errors.total?.message}</strong>
                                </span>
                            </fieldset>
                        </Col>
                        <Col md={4} sm={6}>
                            <fieldset>
                                <Label htmlFor="currency"><strong>Devise</strong></Label>
                                <select className={`form-control ${errors.currency ? 'is-invalid' : ''}`} {...register('currency')} id="currency" required="required">
                                    <option value=''>Selectioner une devise</option>
                                    {currencies.map((item, index) => (
                                        <option key={index} >{item.code}</option>
                                    ))}
                                </select>
                                <span className='invalid-feedback'>
                                    <strong>{errors.currency?.message}</strong>
                                </span>
                            </fieldset>
                        </Col>
                    </Row>


                    <fieldset>
                        <Label htmlFor="content"><strong>Message</strong> (facultatif)</Label>
                        <textarea className={`form-control ${errors.content ? 'is-invalid' : ''}`}
                            type="text"
                            id="content"
                            {...register('content')}
                            maxLength="5000"
                            placeholder={`Laisser un message à ${cagnoteItem.user.firstName} ...`}
                        />
                        <span className='invalid-feedback'>
                            <strong>{errors.content?.message}</strong>
                        </span>
                    </fieldset>
                    <br />
                    <fieldset>
                        <div className="custom-checkbox custom-control custom-control-inline">
                            <input type="checkbox" id="statusUser"
                                {...register('statusUser')}
                                className={`custom-control-input ${errors.statusUser ? 'is-invalid' : ''}`} />
                            <label className="custom-control-label" htmlFor="statusUser">
                                Contribuer en anonymat
                            </label>
                            <span className='invalid-feedback'>
                                <strong>{errors.statusUser?.message}</strong>
                            </span>
                        </div>
                    </fieldset>
                    <br />
                    <Row>
                        <Col md="12" className="text-center">
                            <Button.Ripple
                                block
                                disabled={isSubmitting}
                                type="submit"
                                className={`${isSubmitting ? "btn-loading" : ""
                                    } btn-load`} color="primary">
                                Contribuer avec {process.env.REACT_APP_NAME}
                            </Button.Ripple>
                        </Col>
                    </Row>


                    <hr />
                    <Row>
                        <Col md="12" className="text-center">
                            <Button.Ripple
                                className="btn-block"
                                color="dark"
                                onClick={toggleStatus}
                                outline >
                                Sélectioner un autre moyen
                            </Button.Ripple>
                        </Col>
                    </Row>

                </Form>
            )}

            {statusPayement && (
                <>
                    <Row>
                        <SelectedContributionMethods cagnote={cagnoteItem} />
                        <div className="col-md-12 text-center">
                            <Button.Ripple color="link" onClick={toggleStatus} ><b>Contribuer avec {process.env.REACT_APP_NAME}</b></Button.Ripple>
                        </div>
                    </Row>
                </>
            )}
        </>
    )
}
export default ContributionUserForm
