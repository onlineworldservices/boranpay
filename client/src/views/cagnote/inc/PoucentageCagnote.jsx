import React from 'react'
import { Progress } from "reactstrap"
import { Users } from 'react-feather'

const PoucentageCagnote = (props) => {

    const Inputcurrency = props?.currency?.currencyNumber
    const AmountTotal = props?.total
    const AmountContributeCount = props?.contributes.length ? props?.contributes[0]?.contributes_count : 0
    const AmountTotalContribute = props?.contributes.length ? props?.contributes[0]?.totalAmountcontribute : 0
    const amoutcontribute = (AmountTotalContribute * Inputcurrency)
    const AmountTotalConverte = (AmountTotal / Inputcurrency)
    const poucentage = (AmountTotalContribute / AmountTotalConverte) * 100

    return (
        <>

            <div className="d-flex justify-content-start align-items-center mb-1">

                <div className="user-page-info">
                    <p className="mb-0"><b>Collectées</b></p>
                    <h3 className="text-success"><b>
                        {amoutcontribute === null ? "0,00" : (amoutcontribute).formatMoney(2, '.', ',')} {props?.currency?.symbol}
                    </b></h3>
                </div>
                <div className="ml-auto user-like">
                    <p className="mb-0"><b>Cible</b></p>
                    <h3><b>
                        {props?.total === null ? "0,00" : (props?.total).formatMoney(2, '.', ',')} {props?.currency?.symbol}
                    </b></h3>
                </div>
                
            </div>
            <div className="text-center"><b>{(poucentage).formatMoney(2, '.', ',')}% - {(AmountContributeCount) - 1} <Users size={18} /></b></div>
            <Progress color="success" value={poucentage} />

        </>
    )
}

export default PoucentageCagnote