import React, { useEffect } from "react"
import { Card, CardBody, CardHeader } from 'reactstrap'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom'
import {
    ChevronDown
} from 'react-feather'
import { useDispatch, useSelector } from "react-redux"
import { loadShowCagnoteUserContributes } from '../../../redux/actions/pages/cagnoteAction'
import moment from "moment"
import Avatar from '@components/avatar'

const renderClient = item => {
    const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]

    if (item?.user?.avatar) {
        return <Avatar className='mr-50' img={item?.user?.avatar} width='32' height='32' />
    } else {
        return <Avatar color={color} className='mr-50' content={`${item?.user?.firstName} ${item?.user?.lastName}`} initials />
    }
}
const CagnoteStatistiqueTabe = ({ cagnote }) => {
    const contributes = useSelector(state => state?.cagnotes?.contributes)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowCagnoteUserContributes(cagnote.slugin))
        }
        loadItems()
    }, [cagnote.slugin])

    const columns = [
        {
            name: <h5>Donateur</h5>,
            selector: "donateur_transaction",
            sortable: true,
            grow: 4
        },
        {
            name: <h5>Montant</h5>,
            selector: "price_transaction",
            sortable: true,
            grow: 2
        },
        {
            name: <h5>Date</h5>,
            selector: "date",
            sortable: true,
            grow: 2
        }
    ]

    const data = contributes?.length >= 0 ? (
        contributes.map(item => (
            {
                donateur_transaction: (
                    <>
                        <div className='d-flex justify-content-left align-items-center'>
                            {renderClient(item)}
                            <div className='d-flex flex-column'>
                                <h6 className='user-name text-truncate mb-0'>{item?.user?.firstName} {item?.user?.lastName}</h6>
                                <small className='text-truncate text-muted mb-0'>{item?.user?.email}</small>
                            </div>
                        </div>
                    </>
                ),
                price_transaction: <h4 className={`text-success`}>{item.total && (<>+ {(item.total + item.taxeContribute).formatMoney(2, '.', ',')} <small>{item.currency}</small></>)}</h4>,
                date: <b>{moment(item.createdAt).format('ll')}</b>
            }
        )
        )
    ) : (
        <></>
    )
    return (


        <>
            <Card>
                <CardHeader>
                    <div className="tasks-info">
                        <h3 className="mb-75">
                            <strong>{cagnote.title}</strong>
                        </h3>
                    </div>
                </CardHeader>

                <CardBody>
                    <DataTable
                        data={data}
                        columns={columns}
                        defaultSortField="createdAt"
                        defaultSortAsc={false}
                        overflowY={true}
                        pagination={true}
                        sortIcon={<ChevronDown size={10} />}
                        className='react-dataTable'
                        noHeader />
                </CardBody>
            </Card>
        </>
    )

}


export default CagnoteStatistiqueTabe

