import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { Row, Col, Button } from 'reactstrap'
import CagnoteTable from './inc/CagnoteTable'
import { Plus } from 'react-feather'
import SalesCard from '../dashboard/analytics/SalesCard'
import SoldsCagnoteCard from '../dashboard/analytics/SoldsCagnoteCard'
import { useHistory } from 'react-router-dom'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'

const CagnoteUser = () => {
    const history = useHistory()
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Cagnotes - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Cagnotes"
                breadCrumbParent="Discover"
                breadCrumbActive={`Cagnotes ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            <Row className="match-height">
                <Col lg="7" md="6" sm="6">
                    <SoldsCagnoteCard {...userSite} />
                </Col>

                <Col lg="5" md="6" sm="6">
                    <SalesCard {...userSite} />
                </Col>
            </Row>
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/mycagnotes/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouvelle cagnote</b>
            </Button.Ripple>
            <CagnoteTable {...userSite} />
        </>
    )
}

export default CagnoteUser