import React, { useState } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { CardBody, Card, Row, Col } from 'reactstrap'
import CagnoteUserFormData from "./inc/CagnoteUserFormData"
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'

const CagnoteUserCreate = () => {
    const [userSite] = useState(authuserInfo())

    return (
        <>
            <HelmetSite title={`Nouvelle Cagnote`} />
            <Breadcrumbs
                breadCrumbTitle="Cagnote"
                breadCrumbParent="Discover"
                breadCrumbActive="Nouvelle Cagnote"
            />
            <VerificationInfoUser userItem={userSite} />
            <UndoRedo />

            <Row>
                <Col sm="12">
                    <Card>
                        <CardBody>

                            <CagnoteUserFormData userSite={userSite} />

                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default CagnoteUserCreate