import React, { useState, useEffect } from 'react'
import Breadcrumbs from '@components/breadcrumbs'
import HelmetSite from '@components/helmet/HelmetSite'
import UndoRedo from '@components/undoredo'
import { authuserInfo } from '@components/service'
import { Row, Col, Button, Card, CardBody } from 'reactstrap'
import CagnoteStatistiqueTabe from './inc/CagnoteStatistiqueTabe'
import PoucentageCagnote from './inc/PoucentageCagnote'
import { Plus } from 'react-feather'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { useHistory, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux"
import { loadShowCagnoteUser } from '../../redux/actions/pages/cagnoteAction'
import VerificationInfoUser from '../profile/inc/VerificationInfoUser'

const CagnoteStatistiqueUser = () => {
    const history = useHistory()
    const { cagnoteslugin } = useParams()
    const [userSite] = useState(authuserInfo())
    const item = useSelector(state => state?.cagnotes?.cagnote)
    const dispatch = useDispatch()

    useEffect(() => {
        const loadItems = async () => {
            await dispatch(loadShowCagnoteUser(cagnoteslugin))
            window.scroll({ top: 0, left: 0, behavior: 'smooth' })
        }
        loadItems()
    }, [cagnoteslugin])

    /**
    const onCopy = () => {
        toast.success(
            <SuccessToast name={'Success'} description={'Lien copié avec succès'} />, {
            position: toast.POSITION.TOP_CENTER,
            hideProgressBar: true
        })
    }
    */

    return (
        <>
            <HelmetSite title={`Cagnotes - ${userSite.firstName}`} />
            <Breadcrumbs
                breadCrumbTitle="Cagnotes"
                breadCrumbParent="Discover"
                breadCrumbActive={`Cagnotes ${userSite.firstName}`}
            />
            <VerificationInfoUser userItem={userSite} />
            <Row className="match-height">
                <Col lg="12" md="12" sm="12">
                    <Card className="bg-analytics">
                        <CardBody className="text-center">
                            {item.total && (
                                <PoucentageCagnote {...item} />
                            )}
                            {/**
                             * 
                            <div className="text-center my-sm-75">
                                <CopyToClipboard
                                    onCopy={onCopy}
                                    text={`${process.env.REACT_APP_LINK}/cagnote/${item?.slug}`}
                                >
                                    <Button
                                        className="mr-1 mb-1"
                                        color="primary"
                                        size="sm">
                                        copier le lien
                                    </Button>
                                </CopyToClipboard>
                            </div>
                             */}
                        </CardBody>
                    </Card>
                </Col>
            </Row>
            {/** Undo redo */}
            <UndoRedo />
            <Button.Ripple onClick={() => history.push(`/mycagnotes/new/`)} className="mr-1 mb-1" color="primary" size="sm">
                <Plus size={14} /> <b>Nouvelle cagnote</b>
            </Button.Ripple>
            <CagnoteStatistiqueTabe cagnote={item} />
        </>
    )
}

export default CagnoteStatistiqueUser