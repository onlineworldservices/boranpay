import { lazy } from 'react'

const CheckoutRoutes = [
    {
        path: '/checkout_externe/:clientId',
        component: lazy(() => import('../../views/checkout/CheckoutExterneUser')),
        exact: true,
        meta: { action: 'read', resource: 'ACL' }
    },
    {
        path: '/checkout_externe_off_line/:clientId',
        component: lazy(() => import('../../views/checkout/CheckoutExterneUserNoLogin')),
        layout: 'BlankLayout',
        meta: { authRoute: true, action: 'read', resource: 'ACL' }
    }
]

export default CheckoutRoutes
