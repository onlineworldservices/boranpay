import { lazy } from 'react'

const CagnoteRoutes = [
    {
        path: '/mycagnotes/',
        component: lazy(() => import('../../views/cagnote/CagnoteUser')),
        exact: true,
        meta: { action: 'read', resource: 'ACL' }
    },
    {
        path: '/mycagnotes/new/',
        component: lazy(() => import('../../views/cagnote/CagnoteUserCreate')),
        exact: true,
        meta: { navLink: '/mycagnotes', action: 'read', resource: 'ACL' }
    },
    {
        path: '/mycagnotes/:cagnoteslugin/edit/',
        component: lazy(() => import('../../views/cagnote/CagnoteUserCreate')),
        exact: true,
        meta: { navLink: '/mycagnotes', action: 'read', resource: 'ACL' }
    },
    {
        path: '/cagnote/:cagnoteslugin/statistique/',
        component: lazy(() => import('../../views/cagnote/CagnoteStatistiqueUser')),
        exact: true,
        meta: { navLink: '/cagnote', action: 'read', resource: 'ACL' }
    },
    {
        path: '/cagnote/:cagnoteslugin/',
        component: lazy(() => import('../../views/cagnote/CagnoteUserShow')),
        exact: true,
        meta: { navLink: '/cagnote', action: 'read', resource: 'ACL' }
    }
]

export default CagnoteRoutes
