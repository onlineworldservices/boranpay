import { lazy } from 'react'

const SupportRoutes = [
  {
    path: '/faq/',
    component: lazy(() => import('../../views/pages/faq/FaqUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/contact/',
    component: lazy(() => import('../../views/contact/ContactUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/about/',
    component: lazy(() => import('../../views/pages/AboutUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  },
  {
    path: '/reception_contact/',
    component: lazy(() => import('../../views/infos/ReceptionContact')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  }
]

export default SupportRoutes
