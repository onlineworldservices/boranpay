import { lazy } from 'react'

const DeveloppeurRoutes = [
  {
    path: '/developpeur/app/',
    component: lazy(() => import('../../views/developpeur/ApplicationDeveloppeurUser')),
    exact: true,
    meta: { navLink: '/developpeur/', action: 'read', resource: 'ACL' }
  },
  {
    path: '/developpeur/app/create/',
    component: lazy(() => import('../../views/developpeur/ApplicationDeveloppeurCreateAndEdit')),
    exact: true,
    meta: { navLink: '/developpeur/app/', action: 'read', resource: 'ACL' }
  },
  {
    path: '/developpeur/app/:application/edit/',
    component: lazy(() => import('../../views/developpeur/ApplicationDeveloppeurCreateAndEdit')),
    exact: true,
    meta: { navLink: '/developpeur/app/', action: 'read', resource: 'ACL' }
  },
  {
    path: '/developpeur/app/:application/show/',
    component: lazy(() => import('../../views/developpeur/ApplicationDeveloppeurShow')),
    exact: true,
    meta: { navLink: '/developpeur/app/', action: 'read', resource: 'ACL' }
  },
  {
    path: '/developpeur/accounts/',
    component: lazy(() => import('../../views/developpeur/DeveloppeurUser')),
    exact: true,
    meta: { navLink: '/developpeur/', action: 'read', resource: 'ACL' }
  }
]

export default DeveloppeurRoutes
