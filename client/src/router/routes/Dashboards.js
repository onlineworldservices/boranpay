import { lazy } from 'react'

const DashboardRoutes = [
  // Dashboards
  {
    path: '/dashboard/',
    component: lazy(() => import('../../views/dashboard/DashboardUser')),
    exact: true,
    meta: { action: 'read', resource: 'ACL' }
  }
]

export default DashboardRoutes
