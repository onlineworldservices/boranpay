import { lazy } from 'react'

const AuthRoutes = [
    {
        path: '/login/',
        component: lazy(() => import('../../views/auth/LoginUser')),
        layout: 'BlankLayout',
        meta: { authRoute: true }
    },
    {
        path: '/verify_email/:user/',
        component: lazy(() => import('../../views/auth/VerifyEmailUser')),
        exact: true,
        meta: { action: 'read', resource: 'ACL' }
    },
    {
        path: '/password/reset/',
        component: lazy(() => import('../../views/auth/ResetPasswordUser')),
        layout: 'BlankLayout',
        meta: { authRoute: true }
    },
    {
        path: '/reset/password/:usertoken/',
        component: lazy(() => import('../../views/auth/PasswordRenitializeUser')),
        layout: 'BlankLayout',
        meta: { authRoute: true }
    },
    {
        path: '/register/',
        component: lazy(() => import('../../views/auth/RegisterUser')),
        layout: 'BlankLayout',
        meta: { authRoute: true }
    }
]

export default AuthRoutes
