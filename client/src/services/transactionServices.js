import dyaxios from './index'
import { authHeader } from '@components/service'

class transactionServices {

  getShowTransactionUser(slugin) {
    return dyaxios.get(`/transaction/show/${slugin}`, { headers: authHeader() })
  }
  getShowTransactionserviceUser() {
    return dyaxios.get(`/transaction_service/show/${slugin}`, { headers: authHeader() })
  }
  getAllTransactionUser(user) {
    return dyaxios.get(`/transaction/user/${user}`, { headers: authHeader() })
  }
  getAllTransactionUserRetrait(user) {
    return dyaxios.get(`/transaction/user/${user}/retraits`, { headers: authHeader() })
  }

  getAllTransactionUserSend(user) {
    return dyaxios.get(`/transaction/user/${user}/send`, { headers: authHeader() })
  }

  getAllTransactionUserSendUser(usersend, userrecev) {
    return dyaxios.get(`/transaction/user/${usersend}/send/${userrecev}`, { headers: authHeader() })
  }

  getAllTransactionServicesUser(user) {
    return dyaxios.get(`/transaction_service/user/${user}`, { headers: authHeader() })
  }

  getAllTransactionUserRecev(user) {
    return dyaxios.get(`/transaction/user/${user}/recev`, { headers: authHeader() })
  }

  getAllTransactionCagnotesUser(user) {
    return dyaxios.get(`/transaction_cagnote/user/${user}`, { headers: authHeader() })
  }

  getAllTransactionDonationsUser(user) {
    return dyaxios.get(`/transaction_donation/user/${user}`, { headers: authHeader() })
  }
}

export default new transactionServices()