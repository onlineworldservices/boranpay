import dyaxios from './index'
import { authHeader } from '@components/service'

class cagnoteApiServices {

  getShowEditCagnote(cagnoteslugin, userSite) {
    return dyaxios.get(`/cagnote/${cagnoteslugin}/${userSite.slugin}`, { headers: authHeader() })
  }

  /** Save new cagnote */
  postCagnote(data) {
    return dyaxios.post(`/cagnotes`, data, { headers: authHeader() })
  }

  /** Update cagnote */
  updateCagnote(cagnoteslugin, data) {
    return dyaxios.put(`/cagnotes/${cagnoteslugin}`, data, { headers: authHeader() })
  }

}

export default new cagnoteApiServices()