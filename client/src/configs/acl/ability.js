import { Ability } from '@casl/ability'
import { initialAbility } from './initialAbility'

//  Read ability from localStorage
// * Handles auto fetching previous abilities if already logged in user
// ? You can update this if you store user abilities to more secure place
// ! Anyone can update localStorage so be careful and please update this
const userItem = JSON.parse(localStorage.getItem(process.env.REACT_APP_BASE_NAMETOKEN))
const existingAbility = userItem ? userItem.ability : null

export default new Ability(existingAbility || initialAbility)
