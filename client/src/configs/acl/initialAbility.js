// ** Initial user ability
export const initialAbility = [
  {
    action: 'read',
    subject: 'ACL'
  }
]

export const _ = undefined
