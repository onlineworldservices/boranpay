/** Il est bon a savoir que c'est chiffre corrspond au tase des transaction
 * Donc elle doit etre modifier avec preccotion
 * es: 3 %
 */
const TAXE_TRANSACTION_SIMPLE = 2;
const TAXE_TRANSACTION_MARKETPLACE = 4;
// const TAXE_TRANSACTION_DONATION = 5;
// const TAXE_TRANSACTION_CAGNOTE = 2

module.exports = {
  TAXE_TRANSACTION_SIMPLE,
  TAXE_TRANSACTION_MARKETPLACE,
  // TAXE_TRANSACTION_DONATION,
  // TAXE_TRANSACTION_CAGNOTE
};
