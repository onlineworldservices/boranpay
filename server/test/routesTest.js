const chai = require('chai');
const request = require('supertest');

const { expect } = chai;

const server = require('../server');

// example functional tests of routes
describe('GET /api/v1/payementmethods_retrait', () => {
  it('responds with homepage', () => request('http://localhost:8080/')
    .get('/api/v1/payementmethods_retrait')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200)
    .then((response) => {
      expect(response.text).to.include(
        'You are running a Node.js microservice built for the IBM Cloud.',
      );
    }));
});

// describe('GET /health', () => {
//   it('responds with json', () => request(server)
//     .get('/health/')
//     .set('Accept', 'application/json')
//     .expect('Content-Type', 'application/json; charset=utf-8')
//     .expect(200, {
//       status: 'UP',
//     }));
// });
//
// describe('GET /swagger/api-docs', () => {
//   it('responds with swagger', () => request(server)
//     .get('/swagger/api-docs/')
//     .expect('Content-Type', 'text/html; charset=utf-8')
//     .expect(200)
//     .then((response) => {
//       expect(response.text).to.include('Swagger');
//     }));
// });

// describe('POST /fake/route', () => {
//   it('responds with not found page', () => request(server)
//     .post('/fake/route')
//     .expect('Content-Type', 'text/html; charset=UTF-8')
//     .expect(200)
//     .then((response) => {
//       expect(response.text).to.include(
//         'Whoops! Looks like you got lost or couldn\'t find your page.',
//       );
//     }));
// });
