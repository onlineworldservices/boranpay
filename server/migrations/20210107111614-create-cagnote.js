'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('cagnotes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: true,
        type: Sequelize.STRING
      },
      slug: {
        allowNull: true,
        type: Sequelize.STRING
      },
      slugin: {
        allowNull: true,
        type: Sequelize.STRING
      },
      content: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      image: {
        allowNull: true,
        type: Sequelize.STRING
      },
      total: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      currency: {
        allowNull: true,
        type: Sequelize.STRING
      },
      user_id: {
        type: Sequelize.INTEGER.UNSIGNED,
        references:{
          models: 'users',
          key: 'id'
        }
      },
      currency_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      country_id: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('cagnotes');
  }
};