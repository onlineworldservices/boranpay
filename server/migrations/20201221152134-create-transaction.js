module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      title: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      ip: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      content: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      statusSend: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN,
      },
      statusMovesold: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: 0,
      },
      slugin: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      tokenTransaction: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      paymentId: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      custom: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      invoiceNumber: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      categoryId: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      userId: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          models: 'users',
          key: 'id',
        },
      },
      usertoId: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('transactions');
  },
};
