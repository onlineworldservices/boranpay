module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      lastName: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      statusProfile: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      username: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      slugin: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
        unique: true,
      },
      sex: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      avatar: {
        type: Sequelize.STRING,
        unique: true,
      },
      avatarcover: {
        type: Sequelize.STRING,
        unique: true,
      },
      password: {
        type: Sequelize.STRING,
      },
      emailVerifiedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      birstday: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('users');
  },
};
