'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('transactioncagnotes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ip: {
        allowNull: true,
        type: Sequelize.STRING
      },
      content: {
        allowNull: true,
        type: Sequelize.STRING
      },
      status_send: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN,
      },
      status_movesold: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: 0
      },
      slugin: {
        allowNull: true,
        type: Sequelize.STRING
      },
      token_transaction: {
        allowNull: true,
        type: Sequelize.STRING
      },
      paymentId: {
        allowNull: true,
        type: Sequelize.STRING
      },
      custom: {
        allowNull: true,
        type: Sequelize.STRING
      },
      invoice_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      category_id: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      user_id: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
        references: {
          models: 'users',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('transactioncagnotes');
  }
};