module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('amounts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      total: {
        allowNull: true,
        type: Sequelize.FLOAT,
      },
      currency: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      transactioncagnoteId: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      transactionId: {
        allowNull: true,
        type: Sequelize.INTEGER.UNSIGNED,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('amounts');
  },
};
