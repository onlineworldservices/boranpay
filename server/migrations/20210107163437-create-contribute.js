'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('contributes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      cagnote_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      content: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      slugin: {
        allowNull: true,
        type: Sequelize.STRING
      },
      total: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      amount_contribute: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      currency: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ip: {
        allowNull: true,
        type: Sequelize.STRING
      },
      status_total: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      status_user: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      invoice_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('contributes');
  }
};