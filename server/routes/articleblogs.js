const express = require('express');
const ArticleblogController = require('../src/Controllers/ArticleblogController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/search/articleblogs', ArticleblogController.search);
router.get('/articleblogs', ArticleblogController.index);
router.post('/articleblogs', userAuthMiddleware.checkAuth, ArticleblogController.store);
router.get('/articleblogs/:articleblog/show', ArticleblogController.show);
router.put('/articleblogs/:articleblog', userAuthMiddleware.checkAuth, ArticleblogController.update);

module.exports = router;
