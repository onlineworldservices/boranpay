const express = require('express');
const AffiliationController = require('../src/Controllers/affiliations');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/affiliations', userAuthMiddleware.checkisSuperAdmin, AffiliationController.index);
router.get('/affiliations/:affiliation', userAuthMiddleware.checkisSuperAdmin, AffiliationController.show);
router.post('/affiliations', userAuthMiddleware.checkisSuperAdmin, AffiliationController.store);
router.put('/affiliations/:affiliation', userAuthMiddleware.checkisSuperAdmin, AffiliationController.update);
router.delete('/affiliations/:affiliation', userAuthMiddleware.checkisSuperAdmin, AffiliationController.destroy);
module.exports = router;
