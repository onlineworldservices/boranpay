const express = require('express');
const NotificationController = require('../src/Controllers/NotificationController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/notifications', NotificationController.index);
router.post('/notifications/:notification', userAuthMiddleware.checkAuth, NotificationController.show);
router.post('/notification/user/:user', userAuthMiddleware.checkAuth, NotificationController.notificationuser);

module.exports = router;
