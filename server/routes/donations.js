const express = require('express');
const DonationController = require('../src/Controllers/DonationController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/donations', userAuthMiddleware.checkisAdmin, DonationController.index);
router.post('/donation', userAuthMiddleware.checkAuth, DonationController.store);
router.get('/donation/:slugin/:userslugin', userAuthMiddleware.checkAuth, DonationController.show);
router.get('/donation/:slugin', DonationController.showPublic);
router.put('/donation/:slugin', userAuthMiddleware.checkAuth, DonationController.update);
router.put('/donation/:slugin/status', DonationController.changestatus);
router.get('/mydonations/:userslugin', userAuthMiddleware.checkAuth, DonationController.showDonationuser);

module.exports = router;
