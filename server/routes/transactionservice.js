const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const TransactionMarketplace = require('../src/Controllers/TransactionMarketplace');

const router = express.Router();

router.get('/application/:clientId/:clientSecret/check', TransactionMarketplace.applicationCheck);
router.post('/checkout_externe/:clientId', userAuthMiddleware.checkAuth, TransactionMarketplace.tranfertCheckout);

router.get('/transaction_service/show/:transactionservice', userAuthMiddleware.checkAuth, TransactionMarketplace.show);
router.post('/transfert_service', userAuthMiddleware.checkAuth, TransactionMarketplace.transfertservice);
router.get('/transaction_service/user/:user', userAuthMiddleware.checkAuth, TransactionMarketplace.showTransactionuser);

module.exports = router;
