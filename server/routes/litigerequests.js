const express = require('express');
const LitigerequestController = require('../src/Controllers/LitigerequestController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/litigerequests', userAuthMiddleware.checkisAdmin, LitigerequestController.index);
router.post('/litigerequests', userAuthMiddleware.checkAuth, LitigerequestController.store);
router.get('/litigerequests/:litigerequest', userAuthMiddleware.checkisAdmin, LitigerequestController.show);

module.exports = router;
