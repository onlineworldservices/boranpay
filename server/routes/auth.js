const express = require('express');
const AuthuserController = require('../src/Controllers/auth/AuthuserController');
const VerifyemailuserController = require('../src/Controllers/auth/VerifyemailuserController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const ResetpasswordController = require('../src/Controllers/auth/ResetpasswordController');

const router = express.Router();

router.post('/user_check/:slugin', userAuthMiddleware.checkAuth, AuthuserController.usercheck);
router.post('/verify_email/:slugin', userAuthMiddleware.checkAuth, VerifyemailuserController.verifyemail);
router.post('/resend_email/:slugin', userAuthMiddleware.checkAuth, VerifyemailuserController.resendemail);

router.post('/register', AuthuserController.register);
router.post('/login', AuthuserController.login);

router.post('/password/reset', ResetpasswordController.passwordreset);
router.put('/password/update/:user', userAuthMiddleware.checkAuth, ResetpasswordController.changeresetupdate);
router.put('/password/reset/:usertoken', ResetpasswordController.passwordresetupdate);

module.exports = router;
