const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const TransactionCagnote = require('../src/Controllers/TransactionCagnote');

const router = express.Router();

router.get('/transaction_cagnote/user/:user', userAuthMiddleware.checkAuth, TransactionCagnote.showTransactionuser);

router.post('/transfert_cagnote', userAuthMiddleware.checkAuth, TransactionCagnote.transfertcagnote);
router.post('/cagnote/:cagnote/contribute', userAuthMiddleware.checkAuth, TransactionCagnote.cagnotecontribute);

module.exports = router;
