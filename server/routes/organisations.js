const express = require('express');
const OrganisationController = require('../src/Controllers/Partial/OrganisationController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/organisations', userAuthMiddleware.checkisAdmin, OrganisationController.index);
router.post('/organisations', userAuthMiddleware.checkisAdmin, OrganisationController.store);
router.get('/organisations/:organisation', userAuthMiddleware.checkisAdmin, OrganisationController.show);
router.put('/organisations/:organisation', userAuthMiddleware.checkisAdmin, OrganisationController.update);
router.delete('/organisations/:organisation', userAuthMiddleware.checkisAdmin, OrganisationController.destroy);

module.exports = router;
