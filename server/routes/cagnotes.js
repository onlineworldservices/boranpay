const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const CagnoteController = require('../src/Controllers/CagnoteController');

const router = express.Router();

router.get('/cagnotes', userAuthMiddleware.checkisAdmin, CagnoteController.index);
router.post('/cagnotes', userAuthMiddleware.checkAuth, CagnoteController.store);
router.get('/cagnote/:cagnote/:userslugin', userAuthMiddleware.checkAuth, CagnoteController.show);
router.get('/cagnotes_show/:cagnote', userAuthMiddleware.checkAuth, CagnoteController.showPublic);
router.put('/cagnotes/:cagnote', userAuthMiddleware.checkAuth, CagnoteController.update);
router.put('/cagnotes/:cagnote/status', CagnoteController.changestatus);
router.put('/cagnotes/:cagnote/close', CagnoteController.closingstatus);
router.get('/mycagnotes/:userslugin', userAuthMiddleware.checkAuth, CagnoteController.showCagnotuser);
router.get('/cagnotes/:cagnote/contributes', CagnoteController.showcontributes);

module.exports = router;
