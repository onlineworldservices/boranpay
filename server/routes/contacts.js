const express = require('express');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');
const ContactController = require('../src/Controllers/ContactController');

const router = express.Router();

router.get('/contacts', ContactController.index);
router.post('/contacts', ContactController.store);
router.post('/contact/:user/user', userAuthMiddleware.checkAuth, ContactController.storeuser);
router.get('/mycontacts/:user', userAuthMiddleware.checkAuth, ContactController.mycontacts);
router.get('/mycontacts/:user/user/:userto', userAuthMiddleware.checkAuth, ContactController.mycontactsuser);
router.get('/contacts/:contact', userAuthMiddleware.checkAuth, ContactController.show);
router.delete('/contacts/:contact', userAuthMiddleware.checkAuth, ContactController.destroy);

module.exports = router;
