const express = require('express');
const CouponController = require('../src/Controllers/Partial/CouponController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/coupons', userAuthMiddleware.checkisSuperAdmin, CouponController.index);
router.post('/coupons', userAuthMiddleware.checkisSuperAdmin, CouponController.store);

module.exports = router;
