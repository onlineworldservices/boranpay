const express = require('express');
const GroupeController = require('../src/Controllers/GroupeController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/groupes', GroupeController.index);
router.post('/groupes', userAuthMiddleware.checkAuth, GroupeController.store);
router.get('/groupes/:groupe', userAuthMiddleware.checkAuth, GroupeController.show);
router.get('/groupe/u/:user', userAuthMiddleware.checkAuth, GroupeController.showGroupeuser);
router.put('/groupes/:groupe', userAuthMiddleware.checkAuth, GroupeController.update);
router.delete('/groupes/:groupe', userAuthMiddleware.checkAuth, GroupeController.destroy);

module.exports = router;
