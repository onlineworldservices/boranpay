const express = require('express');
const ReclamationController = require('../src/Controllers/ReclamationController');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/reclamations', userAuthMiddleware.checkisAdmin, ReclamationController.index);
router.post('/reclamations', userAuthMiddleware.checkAuth, ReclamationController.store);
router.get('/reclamations/:reclamation', userAuthMiddleware.checkAuth, ReclamationController.show);
router.get('/reclamations/:reclamation/:userslugin', userAuthMiddleware.checkAuth, ReclamationController.showanduser);
router.get('/reclamation/u/:user', userAuthMiddleware.checkAuth, ReclamationController.showDatauser);
router.put('/reclamations/:reclamation', userAuthMiddleware.checkAuth, ReclamationController.update);
router.delete('/reclamations/:reclamation', userAuthMiddleware.checkAuth, ReclamationController.destroy);

module.exports = router;
