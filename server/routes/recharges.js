const express = require('express');
const RechargeController = require('../src/Controllers/RechargeController');
const TransactionCoupon = require('../src/Controllers/TransactionCoupon');
const TransactionStripe = require('../src/Controllers/TransactionStripe');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.get('/success', RechargeController.paypalsucces);
// router.post('/recharges', userAuthMiddleware.checkAuth, RechargeController.store);
router.post('/recharges/coupons', userAuthMiddleware.checkAuth, TransactionCoupon.couponcharge);
router.post('/recharges/coupons/:cagnote/cagnote', userAuthMiddleware.checkAuth, TransactionCoupon.couponchargecagnote);
router.post('/recharges/coupons/:donation/donation', userAuthMiddleware.checkAuth, TransactionCoupon.couponchargedonation);
router.post('/recharges/stripe/:donation/donation', userAuthMiddleware.checkAuth, TransactionStripe.stripechargedonation);
router.post('/recharges/paypal', RechargeController.paypalcharge);
router.post('/recharges/stripe', userAuthMiddleware.checkAuth, RechargeController.stripecharge);
/** router.get('/recharges/:slugin', RechargeController.show); */

module.exports = router;
