const express = require('express');
const TransactionUser = require('../src/Controllers/TransactionUser');
const TransactionRetrait = require('../src/Controllers/TransactionRetrait');
const userAuthMiddleware = require('../src/Middleware/AuthuserMiddleware');

const router = express.Router();

router.post('/transfert', userAuthMiddleware.checkAuth, TransactionUser.transfertsend);
router.post('/transfert/user/:user', userAuthMiddleware.checkAuth, TransactionUser.transfertusersend);
router.post('/transfert_reclamation/:reclamation', userAuthMiddleware.checkAuth, TransactionUser.transfertreclamation);
router.post('/retraits', userAuthMiddleware.checkAuth, TransactionRetrait.retraits);
router.post('/ug_transfert/:usergroupe/transfert', userAuthMiddleware.checkAuth, TransactionUser.usergroupe);

module.exports = router;
