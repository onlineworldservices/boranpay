const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');
const transactiondonationService = require('./services/TransactiondonationService');
const transactionuserService = require('../TransactionUser/services/TransactionuserService');
const { currencyValidate } = require('../../../helper/validatorvalue');

const transfertdonation = async (req, res) => {
  const user = await models.user.findOne({ where: { email: req.body.email } });
  if (!user) {
    res.status(400).json({ message: 'Cet utilisateur est inexistant veuilez insérer un e-mail valide' });
  }

  const Title = `${req.body.title}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = user.id;
  const userSend = req.AuthUser;
  const userRecev = user;
  const userCurrency = req.body.currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactiondonationService.anountTransfertdonationUser(
        req,
        res,
        categoryTransation,
        Title,
        userCurrency,
        userIdSend,
        userIdRecev,
        userSend,
        userRecev,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const donationcontribute = async (req, res) => {
  const donation = await models.donation.findOne({ where: { slugin: req.params.donation } });
  const Title = `Contribution donation - ${donation.title}`;
  const TotalAmount = req.body.total;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = donation.userId;
  const userCurrency = req.body.currency;
  const categoryTransation = 4; // Ici c'est la category de la transaction

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactiondonationService.anountTraitementdonationUser(
        req,
        res,
        categoryTransation,
        Title,
        TotalAmount,
        userCurrency,
        userIdRecev,
        userIdSend,
        donation,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ce code ci dessous gere l'envoie des dons a l'utilisateur */
const donationsTouser = async (req, res) => {
  const user = await models.user.findOne({ where: { slugin: req.params.user } });
  if (!user) {
    res.status(400).json({ message: "Cet utilisateur n'existe pas veuilez svp insérer un e-mail valide" });
  }

  const Title = `Donation - ${user.firstName}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = user.id;
  const userSend = req.AuthUser;
  const Total = req.body.total;
  const userRecev = user;
  const userCurrency = req.body.currency;
  const categoryTransation = 4; // Id donation

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      if (user.id === req.AuthUser.userId) {
        res.status(400).json({ message: 'Vous ne pouvez pas vous faire de transfert copier le lien et partager à vos proches.' });
      } else {
        transactionuserService.anountTraitementUser(
          req,
          res,
          categoryTransation,
          Title,
          Total,
          userCurrency,
          userIdSend,
          userIdRecev,
          userSend,
          userRecev,
        );
      }
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const transactiondonation = await models.transactiondonation.findOne({
      where: { slugin: req.params.transactiondonation },
      include: [
        {
          model: models.user,
          as: 'userto',
          required: true,
          attributes: ['id', 'firstName', 'email', 'slugin', 'avatar', 'lastName', 'phone'],
          include: [
            {
              model: models.profile,
              required: true,
              include: [
                { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
                { model: models.country, attributes: ['code', 'name'] },
              ],
            },
          ],
        },
        {
          model: models.user,
          required: true,
          attributes: ['id', 'firstName', 'email', 'slugin', 'avatar', 'lastName', 'phone'],
          include: [
            {
              model: models.profile,
              required: true,
              include: [
                { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
                { model: models.country, attributes: ['code', 'name'] },
              ],
            },
          ],
        },
        {
          model: models.amount,
          required: true,
          attributes: ['id', 'total', 'totalNoTaxe', 'taxeTransaction', 'currency'],
          include: [{ model: models.amountdonation, attributes: ['amountId', 'amountDonation', 'amountDonationNoTaxe', 'donationId'] }],
        },
      ],
    });

    res.status(200).json(transactiondonation);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Get tous les transctions faites ou recus par l'utilisateur */
const showTransactionuser = async (req, res) => {
  try {
    const transactiondonations = await models.transactiondonation.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: req.params.user },
          attributes: ['id', 'firstName', 'slugin', 'avatar'],
        },
        { model: models.user, as: 'userto', attributes: ['firstName', 'email', 'slugin', 'avatar'] },
        { model: models.amount, attributes: ['total', 'currency'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactiondonations);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  showTransactionuser,
  show,
  transfertdonation,
  donationcontribute,
  donationsTouser,
};
