const models = require('../../../../models');
const rechargeService = require('./TransactiondonationServicesTraitement');

const anountTransfertdonationUser = async (
  req,
  res,
  categoryTransation,
  Title,
  userCurrency,
  userIdSend,
  userIdRecev,
  userSend,
  userRecev,
) => {
  // let taxeTransaction = (req.body.total * 0.4) / 100

  /** Ici je commence a verifier si l'utilisateur a un solde positif ou > 0 */
  const amountuserCheck = await models.amountdonation.sum('amountDonation', { where: { userId: userIdSend } });
  const newAmountSend = req.body.total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();

  const getCurrenciesTocheck = async (currencyData) => {
    const newAmountConverted = (newAmountSend / currencyData.currencyNumber);
    const newAmountSendConverted = newAmountConverted;
    const newAmountRecevConverted = newAmountConverted; // Ici c'est le montant a sauvegarder
    const validateAmountForSave = amountuserCheck > newAmountSendConverted;
    switch (Inputcurrency) {
      case 'CAD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionviremdoUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte de donation bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionviremdoUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte de donation bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'GBP':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionviremdoUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte de donation bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'USD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionviremdoUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte de donation bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XAF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionviremdoUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte de donation bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XOF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionviremdoUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte de donation bien vouloir recharger votre compte' });
          }
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      } // accolade ajoutée
    }
  };

  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

const anountTraitementdonationUser = async (
  req,
  res,
  categoryTransation,
  Title,
  TotalAmount,
  userCurrency,
  userIdRecev,
  userIdSend,
  donation,
) => {
  /** Ici je commence a verifier si l'utilisateur a un solde positif ou > 0 */
  const amountuserCheck = await models.amountuser.sum('amountUser', { where: { userId: userIdSend } });
  const newAmountSave = TotalAmount; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();

  const getCurrenciesTocheck = async (currencyData) => {
    const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
    const newAmountConvertedTaxe = (newAmountConverted * 2) / 100; // Ici je calcule les taxes
    const newAmountConvertedTaxeTotal = (newAmountConverted - newAmountConvertedTaxe);
    const newAmountSendConverted = newAmountConverted;
    const newAmountRecevConverted = newAmountConvertedTaxeTotal;
    const validateAmountForSave = amountuserCheck > newAmountSendConverted;
    switch (Inputcurrency) {
      case 'CAD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactiondonationUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              currencyData,
              categoryTransation,
              newAmountConvertedTaxe,
              Title,
              userCurrency,
              userIdSend,
              userIdRecev,
              donation,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactiondonationUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              currencyData,
              categoryTransation,
              newAmountConvertedTaxe,
              Title,
              userCurrency,
              userIdSend,
              userIdRecev,
              donation,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'GBP':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactiondonationUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              currencyData,
              categoryTransation,
              newAmountConvertedTaxe,
              Title,
              userCurrency,
              userIdSend,
              userIdRecev,
              donation,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'USD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactiondonationUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              currencyData,
              categoryTransation,
              newAmountConvertedTaxe,
              Title,
              userCurrency,
              userIdSend,
              userIdRecev,
              donation,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XAF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactiondonationUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              currencyData,
              categoryTransation,
              newAmountConvertedTaxe,
              Title,
              userCurrency,
              userIdSend,
              userIdRecev,
              donation,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XOF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactiondonationUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              currencyData,
              categoryTransation,
              newAmountConvertedTaxe,
              Title,
              userCurrency,
              userIdSend,
              userIdRecev,
              donation,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      } // accolade ajoutée
    }
  };

  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

module.exports = {
  anountTransfertdonationUser,
  anountTraitementdonationUser,
};
