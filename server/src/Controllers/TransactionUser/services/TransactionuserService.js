const models = require('../../../../models');
const rechargeService = require('./TransactionuserServicesTraitement');
const { TAXE_TRANSACTION_SIMPLE } = require('../../../../helper/taxetransaction');

const anountTraitementUser = async (
  req,
  res,
  categoryTransation,
  Title,
  Total,
  userCurrency,
  userIdSend,
  userIdRecev,
  userSend,
  userRecev,
) => {
  /** Ici je commence a verifier si l'utilisateur a un solde positif ou > 0 */
  const amountuserCheck = await models.amountuser.sum('amountUser', { where: { userId: userIdSend } });
  const newAmountSend = Total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();

  const getCurrenciesTocheck = async (currencyData) => {
    // Ici je convertis le montant que doit recevoir l'utilisateur
    const newAmountConverted = (newAmountSend / currencyData.currencyNumber);
    // Ici je calcule les taxes 2 corespond au pourcentage des taxe
    const newAmountConvertedTaxe = (newAmountConverted * TAXE_TRANSACTION_SIMPLE) / 100;
    // Cette ligne verify la conversion de la somme en €
    const newAmountSendConverted = categoryTransation === 4
      ? newAmountConverted : (newAmountConverted + newAmountConvertedTaxe);
    const newAmountRecevConverted = categoryTransation === 4
      ? (newAmountConverted - newAmountConvertedTaxe) : newAmountConverted;
    const validateAmountForSave = amountuserCheck > newAmountSendConverted;
    switch (Inputcurrency) {
      case 'CAD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionUser(
              req,
              res,
              newAmountConvertedTaxe,
              newAmountRecevConverted,
              newAmountSendConverted,
              categoryTransation,
              Title,
              currencyData,
              userCurrency,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionUser(
              req,
              res,
              newAmountConvertedTaxe,
              newAmountRecevConverted,
              newAmountSendConverted,
              categoryTransation,
              Title,
              currencyData,
              userCurrency,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'GBP':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionUser(
              req,
              res,
              newAmountConvertedTaxe,
              newAmountRecevConverted,
              newAmountSendConverted,
              categoryTransation,
              Title,
              currencyData,
              userCurrency,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'USD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionUser(
              req,
              res,
              newAmountConvertedTaxe,
              newAmountRecevConverted,
              newAmountSendConverted,
              categoryTransation,
              Title,
              currencyData,
              userCurrency,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XAF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionUser(
              req,
              res,
              newAmountConvertedTaxe,
              newAmountRecevConverted,
              newAmountSendConverted,
              categoryTransation,
              Title,
              currencyData,
              userCurrency,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XOF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargeService.transactionUser(
              req,
              res,
              newAmountConvertedTaxe,
              newAmountRecevConverted,
              newAmountSendConverted,
              categoryTransation,
              Title,
              currencyData,
              userCurrency,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      } // accolade ajoutée
    }
  };

  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

module.exports = {
  anountTraitementUser,
};
