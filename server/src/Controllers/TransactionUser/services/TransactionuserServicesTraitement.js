const models = require('../../../../models');
const TransactionTransfertMail = require('../../../Mail/TransactionTransfertMail');
const { makeSluginNumber, makeSluginID, makeParseIp } = require('../../../../helper/utils');

const transactionUser = async (
  req,
  res,
  newAmountConvertedTaxe,
  newAmountRecevConverted,
  newAmountSendConverted,
  categoryTransation,
  Title,
  currencyData,
  userCurrency,
  userIdSend,
  userIdRecev,
  userSend,
  userRecev,
) => {
  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transaction = await models.transaction.create(itemSend);
  if (!transaction) {
    return res.status(400).json({ message: 'Invalid data' });
  }
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transaction.invoiceNumber,
  };
  const transactionRecev = await models.transaction.create(itemRecev);
  if (!transactionRecev) {
    return res.status(400).json({ message: 'Invalid data' });
  }

  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    taxeTransaction: (newAmountConvertedTaxe * currencyData.currencyNumber),
    totalNoTaxe: ((newAmountSendConverted - newAmountConvertedTaxe) * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transaction.id,
    ip: makeParseIp(req),
  };
  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    totalNoTaxe: (newAmountRecevConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: transactionRecev.id,
    ip: makeParseIp(req),
  };

  const amount = await models.amount.create(itemAmount);
  await models.amount.create(itemAmountRecev);
  // Le - va alle soustraire le solde dans le conte de la personne qui envoi
  const newAmountuserSend = -newAmountSendConverted;
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserSend = {
    amountUser: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
    transactionId: amount.transactionId,
    ip: makeParseIp(req),
  }; // Cette ligne c'est pour sauvegarder l'envoie
  const itemAmountUserRecev = {
    amountUser: newAmountuserRecev,
    amountId: amount.id,
    userId: userIdRecev,
    transactionId: amount.transactionId,
    ip: makeParseIp(req),
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseSend = await models.amountuser.create(itemAmountUserSend);
  if (!responseSend) { return res.status(400).json({ message: 'Invalid data' }); }
  await models.amountuser.create(itemAmountUserRecev);

  TransactionTransfertMail.transactionUserMail(userSend, userRecev, transaction, itemAmountRecev);
  return res.status(200).json({
    message: 'Data save successfully',
    transaction,
  });
};

module.exports = {
  transactionUser,
};
