const Validator = require('fastest-validator');
const { Sequelize } = require('sequelize');
const bcrypt = require('bcryptjs');
const models = require('../../models');
const { makeSluginID, makeParseIp } = require('../../helper/utils');

const index = async (req, res) => {
  try {
    const applications = await models.application.findAll({
      include: [{ model: models.user, attributes: ['firstName', 'lastName', 'slugin', 'email'] }],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(applications);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const applicationuser = async (req, res) => {
  try {
    const applications = await models.application.findAll({
      where: { statusExistApplication: true },
      include: [
        {
          model: models.user,
          where: { slugin: req.params.user },
          attributes: ['firstName', 'lastName', 'slugin', 'avatar'],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(applications);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const { name, content } = req.body;
  const { userId, slugin } = req.AuthUser;
  // const newapplication = await models.application.findOne({ where: { name } });
  // if (newapplication) {
  //   return res.status(400).json({ message: 'Ce non est déjà prise' });
  // }
  const userInfo = await models.user.findOne({
    where: { slugin },
    include: [
      {
        model: models.profile,
        attributes: ['slugin'],
        required: true,
        include: [
          { model: models.country, attributes: ['name', 'slug', 'code'] },
          { model: models.currency, attributes: ['name', 'symbol', 'code'] },
        ],
      },
    ],
  });
  // console.log('userInfo =>', userInfo.profile.currency.code)

  const item = {
    name,
    content,
    userId,
    ip: makeParseIp(req),
    clientId: makeSluginID(60),
    slugin: makeSluginID(20),
    status: false,
  };

  const schema = {
    name: 'string|required|min:3|max:200',
    status: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    return res.status(400).json({ errors: validationResponse });
  }

  try {
    const application = await models.application.create(item);
    if (application) {
      const hashedPsw = await bcrypt.hash('000000000', 12);
      await models.applicationsecretekey.create({
        secretKey: makeSluginID(60),
        applicationId: application.id,
      });
      // await models.developersecretekey.create({
      //    secretKey: makeSluginID(100),
      //    statusApplication: true,
      //    applicationId: application.id
      // });

      await models.amountservice.create({
        amountServiceBeyer: 10000,
        statusApplication: false,
        userId: userInfo.id,
        applicationId: application.id,
      });
      /** Developer personal */
      await models.developeruser.create({
        firstName: 'John',
        lastName: 'Doe',
        email: `${makeSluginID(8)}@acheteur.com`,
        username: 'John-doe',
        slugin: makeSluginID(15),
        statusProfile: true,
        password: hashedPsw,
        applicationId: application.id,
      });
    }
    return res.status(200).json(application);
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const application = await models.application.findOne({
      where: { slugin: req.params.application },
      include: [
        { model: models.developeruser, attributes: ['applicationId', 'email', 'firstName', 'avatar'] },
        { model: models.applicationsecretekey, attributes: ['applicationId', 'secretKey'] },
        {
          model: models.user,
          attributes: ['email', 'firstName', 'avatar'],
          include: [
            {
              model: models.profile,
              attributes: ['slugin'],
              include: [{ model: models.currency, required: true, attributes: ['code', 'symbol', 'currencyNumber'] }],
            },
          ],
        },
        {
          model: models.amountservice,
          attributes: [
            'applicationId',
            [Sequelize.fn('sum', Sequelize.col('amountService')), 'totalAmountservice'],
            [Sequelize.fn('sum', Sequelize.col('amountServiceTest')), 'totalAmountserviceTest'],
            [Sequelize.fn('sum', Sequelize.col('amountServiceBeyer')), 'totalAmountserviceBeyer'],
          ],
          group: ['applicationId'],
          limit: 999999999999999,
        },
        {
          model: models.transactionservice,
          where: { statusApplication: false }, // ici c'est pour filtrer des application online
          include: [{ model: models.amount, attributes: ['total', 'totalNoTaxe', 'taxeTransaction', 'currency'] }],
          order: [['createdAt', 'DESC']],
          limit: 999999999999999,
        },
        {
          model: models.transactionservice,
          as: 'transactionserviceOnline',
          where: { statusApplication: true },
          include: [{ model: models.amount, attributes: ['total', 'totalNoTaxe', 'taxeTransaction', 'currency'] }],
          order: [['createdAt', 'DESC']],
          limit: 999999999999999,
        },
      ],
    });
    res.status(200).json(application);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const dataUpdate = {
    name: req.body.name,
    content: req.body.content,
    ip: makeParseIp(req),
  };

  const schema = {
    name: 'string|required|min:3|max:200',
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    await models.application.update(dataUpdate, { where: { slugin: req.params.application } });
    res.status(200).json({
      message: 'Data update successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const statusChange = async (req, res) => {
  try {
    const application = await models.application.findOne(
      { where: { slugin: req.params.application } },
    );
    if (!application) {
      return res.status(400).json({ message: "Application don't exist" });
    }

    const dataUpdate = { statusApplication: !application.statusApplication };

    await application.update(dataUpdate);

    return res.status(200).json({ application });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};

const changestatus = async (req, res) => {
  const dataUpdate = { statusExistApplication: false, ip: makeParseIp(req) };
  try {
    await models.application.update(dataUpdate, { where: { slugin: req.params.application } });

    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  update,
  statusChange,
  applicationuser,
  changestatus,
};
