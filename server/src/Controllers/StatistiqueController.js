const { Sequelize } = require('sequelize');
const models = require('../../models');

/** Ici je Get l'utilisateur avec ses donner liés la relation */
const transactionsSite = async (req, res) => {
  try {
    const users = await models.user.findAndCountAll({
      attributes: ['id', 'email', 'firstName', 'lastName', 'slugin', 'avatar', 'createdAt'],
      include: [
        {
          model: models.profile,
          attributes: ['slugin'],
          required: true,
          include: [{ model: models.currency, required: true, attributes: ['code', 'symbol', 'currencyNumber'] }],
        },
      ],
      limit: 4,
      order: [['createdAt', 'DESC']],
    });
    const transactions = await models.transaction.findAndCountAll({
      attributes: ['id', 'statusSend', 'slugin', 'categoryId', 'userId', 'createdAt'],
      include: [
        { model: models.user, as: 'userto', attributes: ['id', 'email', 'firstName', 'lastName', 'slugin', 'avatar'] },
        { model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] },
        { model: models.category, attributes: ['id', 'name', 'slugin', 'slugin'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      limit: 6,
      order: [['createdAt', 'DESC']],
    });
    const transactioncagnotes = await models.transactioncagnote.findAndCountAll({
      attributes: ['id', 'statusSend', 'slugin', 'categoryId', 'userId', 'createdAt'],
      include: [
        { model: models.user, as: 'userto', attributes: ['id', 'email', 'firstName', 'lastName', 'slugin', 'avatar'] },
        { model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] },
        { model: models.category, attributes: ['id', 'name', 'slugin', 'slugin'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      limit: 4,
      order: [['createdAt', 'DESC']],
    });
    const transactiondonation = await models.transactiondonation.findAndCountAll({
      attributes: ['id', 'statusSend', 'slugin', 'categoryId', 'userId', 'createdAt'],
      include: [
        { model: models.user, as: 'userto', attributes: ['id', 'email', 'firstName', 'lastName', 'slugin', 'avatar'] },
        { model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] },
        { model: models.category, attributes: ['id', 'name', 'slugin', 'slugin'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      limit: 4,
      order: [['createdAt', 'DESC']],
    });
    const reclamations = await models.reclamation.findAndCountAll({
      limit: 4,
      attributes: ['id', 'title', 'slugin', 'userId', 'createdAt'],
      include: [{ model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] }],
      order: [['createdAt', 'DESC']],
    });
    const donations = await models.donation.findAndCountAll({
      where: { isDelete: false },
      limit: 4,
      attributes: ['id', 'title', 'slugin', 'userId', 'createdAt'],
      include: [{ model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] }],
      order: [['createdAt', 'DESC']],
    });
    const contributes = await models.contribute.findAndCountAll({
      limit: 4,
      attributes: ['total', 'currency', 'amountContribute', 'userId', 'createdAt'],
      include: [{ model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] }],
      order: [['createdAt', 'DESC']],
    });
    const cagnotes = await models.cagnote.findAndCountAll({
      where: { isDelete: false },
      limit: 4,
      attributes: ['id', 'title', 'slugin', 'userId', 'createdAt'],
      include: [
        { model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] },
        { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber', 'name'] },
        {
          model: models.contribute,
          attributes: ['cagnoteId',
            [Sequelize.fn('COUNT', Sequelize.col('cagnoteId')), 'contributes_count'],
            [Sequelize.fn('sum', Sequelize.col('amountContribute')), 'totalAmountcontribute'],
          ],
          group: ['cagnoteId'],
          limit: 999999999999999,
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json({
      users,
      reclamations,
      transactions,
      transactioncagnotes,
      transactiondonation,
      donations,
      contributes,
      cagnotes,
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  transactionsSite,
};
