const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');
const transactioncagnoteService = require('./services/TransactioncagnoteService');
const { currencyValidate } = require('../../../helper/validatorvalue');

const cagnotecontribute = async (req, res) => {
  const cagnote = await models.cagnote.findOne({
    where: { slug: req.params.cagnote, isDelete: false },
  });
  const userIdSend = req.AuthUser.userId;
  // let userId = req.AuthUser.userId;
  const Title = `Contribution cagnote - ${cagnote.title}`;
  const userIdRecev = cagnote.userId;
  const userCurrency = req.body.currency;
  const categoryTransation = 5; // Ici c'est la category de la transaction

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactioncagnoteService.anountTraitementcagnoteUser(
        req,
        res,
        categoryTransation,
        Title,
        userCurrency,
        userIdRecev,
        userIdSend,
        cagnote,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Dans cette partie dcu code je transfert toutes les momtant de la cagnote */
const transfertcagnote = async (req, res) => {
  const Title = `${req.body.title}`;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = req.AuthUser.userId;
  const userSend = req.AuthUser;
  const userRecev = req.AuthUser;
  const userCurrency = req.body.currency;
  const categoryTransation = 3; // Id tranfert

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactioncagnoteService.anountTransfertcagnoteUser(
        req,
        res,
        categoryTransation,
        Title,
        userCurrency,
        userIdSend,
        userIdRecev,
        userSend,
        userRecev,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Get tous les transctions faites ou recus par l'utilisateur */
const showTransactionuser = async (req, res) => {
  try {
    const transactioncagnotes = await models.transactioncagnote.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: req.params.user },
          attributes: ['id', 'firstName', 'slugin', 'avatar'],
        },
        { model: models.user, as: 'userto', attributes: ['firstName', 'email', 'slugin', 'avatar'] },
        { model: models.amount, attributes: ['total', 'currency'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactioncagnotes);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  showTransactionuser,
  cagnotecontribute,
  transfertcagnote,
};
