const Validator = require('fastest-validator');
const { Sequelize } = require('sequelize');
const mySlug = require('slug');
const models = require('../../models');
const { makeSluginID, makeSluginNumber, makeParseIp } = require('../../helper/utils');

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    const donations = await models.donation.findAll({
      where: { isDelete: false },
      attributes: ['id', 'title', 'slug', 'slugin', 'userId', 'createdAt', 'updatedAt'],
      include: [
        {
          model: models.user,
          attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'],
          include: [
            {
              model: models.profile,
              attributes: ['slugin'],
              include: [{ model: models.currency, required: true, attributes: ['code', 'symbol', 'currencyNumber'] }],
            },
          ],
        },
        {
          model: models.contribute,
          attributes: [
            'donationId',
            [Sequelize.fn('COUNT', Sequelize.col('donationId')), 'contributes_count']],
          group: ['donationId'],
          // Bon ici c'est pour corriger le bug de sequelize pour calculer bien
          limit: 999999999999999,
        },
        {
          model: models.amountdonation,
          attributes: [
            'donationId',
            [Sequelize.fn('sum', Sequelize.col('amountDonation')), 'totalAmountdonation'],
            [Sequelize.fn('sum', Sequelize.col('amountDonationNoTaxe')), 'totalAmountDonationNoTaxe'],
          ],
          group: ['donationId'],
          // Bon ici c'est pour corriger le bug de sequelize pour calculer bien
          limit: 999999999999999,
        },

      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(donations);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const { title, content } = req.body;
  const { userId } = req.AuthUser;
  const item = {
    title,
    content,
    userId,
    slug: `${mySlug(title)}-${makeSluginNumber(4)}`,
    ip: makeParseIp(req),
    slugin: makeSluginID(20),
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const donation = await models.donation.create(item);
      if (donation) {
        await models.contribute.create({
          userId,
          donationId: donation.id,
        });
        await models.amountdonation.create({
          userId,
          donationId: donation.id,
        });
      }
      res.status(200).json({
        message: 'Data save successfully',
        donation,
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const donation = await models.donation.findOne({
      where: { slugin: req.params.slugin, isDelete: false },
      include: [
        {
          model: models.user,
          where: { slugin: req.params.userslugin },
          attributes: ['id', 'firstName', 'lastName', 'slugin'],
          include: [
            {
              model: models.profile,
              attributes: ['slugin'],
              include: [{ model: models.currency, required: true, attributes: ['code', 'symbol', 'currencyNumber'] }],
            },
          ],
        },
        {
          model: models.contribute,
          attributes: ['slugin', 'total', 'userId', 'currency', 'statusTotal', 'content', 'donationId', 'transactiondonationId', 'amountContribute', 'taxeContribute', 'createdAt'],
          include: [
            { model: models.user, attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'] },
            { model: models.transactiondonation, attributes: ['id', 'slugin'] },
          ],

          order: [['createdAt', 'DESC']],
          // Bon ici c'est pour corriger le bug de sequelize pour calculer bien
          limit: 999999999999999,
        },
        {
          model: models.contribute,
          as: 'contributecount',
          attributes: [
            'donationId',
            [Sequelize.fn('COUNT', Sequelize.col('donationId')), 'contributes_count']],
          group: ['donationId'],
          limit: 999999999999999,
        },
        {
          model: models.amountdonation,
          attributes: [
            'donationId',
            [Sequelize.fn('sum', Sequelize.col('amountDonation')), 'totalAmountdonation'],
            [Sequelize.fn('sum', Sequelize.col('amountDonationNoTaxe')), 'totalAmountdonationNoTaxe'],
          ],
          group: ['donationId'],
          limit: 999999999999999,
        },

      ],
    });
    res.status(200).json(donation);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const showPublic = async (req, res) => {
  try {
    const donation = await models.donation.findOne({
      where: { slugin: req.params.slugin, isDelete: false },
      include: [
        {
          model: models.user,
          attributes: ['id', 'firstName', 'lastName', 'slugin'],
        },

      ],
    });
    res.status(200).json(donation);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const showDonationuser = async (req, res) => {
  /** Bon a savoir ici !!! le count ne marche jamais
   * sans son model don attention a ne pas l'oublier */
  try {
    const donations = await models.donation.findAll({
      where: { isDelete: false },
      include: [
        {
          model: models.user,
          where: { slugin: req.params.userslugin },
          attributes: ['id', 'firstName', 'lastName', 'slugin'],
          include: [{
            model: models.profile, attributes: ['id', 'slugin'], required: true, include: [{ model: models.currency, required: true, attributes: ['code', 'symbol', 'currencyNumber', 'name'] }],
          }],
        },
        {
          model: models.contribute,
          attributes: [
            'donationId',
            [Sequelize.fn('COUNT', Sequelize.col('donationId')), 'contributes_count']],
          group: ['donationId'],
          limit: 999999999999999,
        },
        {
          model: models.amountdonation,
          attributes: [
            'donationId',
            [Sequelize.fn('sum', Sequelize.col('amountDonation')), 'totalAmountdonation'],
          ],
          group: ['donationId'],
          limit: 999999999999999,
        },
      ],
      order: [['updatedAt', 'DESC']],
    });

    res.status(200).json(donations);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};
/** Ici je Update la donne */
const update = async (req, res) => {
  const { title, content } = req.body;
  const dataUpdate = {
    title,
    content,
    ip: makeParseIp(req),
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.donation.update(dataUpdate, { where: { slugin: req.params.slugin } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};
const changestatus = async (req, res) => {
  const dataUpdate = { isDelete: true };
  try {
    await models.donation.update(dataUpdate, { where: { slugin: req.params.slugin } });

    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  showDonationuser,
  showPublic,
  update,
  changestatus,
};
