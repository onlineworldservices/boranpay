const index = (rep, res) => {
  res.render('index', { title: 'Hey', message: 'Hello there!' });
};

function checktoken(rep, res) {
  res.sendStatus(200);
}

module.exports = {
  index,
  checktoken,
};
