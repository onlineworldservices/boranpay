// const Validator = require('fastest-validator');
const paypal = require('paypal-rest-sdk');
const Stripe = require('stripe');
const models = require('../../models');
const { makeSluginID, makeSluginNumber } = require('../../helper/utils');
// const rechargeService = require('./TransactionUser/services/TransactionuserServicesTraitement');
const TransactionMail = require('../Mail/TransactionMail');
// const { currencyValidate } = require('../../helper/validatorvalue');

const stripe = new Stripe(process.env.STRIPE_PRIVATE_KEY);

paypal.configure({
  mode: process.env.PAYPAL_ACCESS_MODE, // sandbox or live
  clientId: process.env.PAYPAL_ACCESS_KEY_ID,
  clientSecret: process.env.PAYPAL_SECRET_ACCESS_KEY,
});

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const recharges = await models.recharge.findAll({
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(recharges);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
// const store = async (req, res) => {
//   const Title = req.body.title;
//   const Total = req.body.total;
//   const userIdRecev = req.body.userId;
//   const { userId } = req.AuthUser;
//   const { couponNumber } = req.body;
//   const categoryTransation = 1; // Id recharge
//   const userCurrency = req.body.currency;
//   const itemRecev = {
//     title: Title,
//     userId: userIdRecev,
//     usertoId: userIdRecev,
//     content: req.body.content,
//     categoryId: categoryTransation,
//     currency: userCurrency,
//     statusSend: true,
//     slugin: makeSluginID(30),
//   };
//
//   const schema = {
//     userId: { type: 'number', positive: true, integer: true },
//     currency: { type: 'enum', values: currencyValidate },
//     statusSend: 'boolean', // short-hand def
//   };
//
//   const v = new Validator();
//   const validationResponse = v.validate(itemRecev, schema);
//
//   if (validationResponse !== true) {
//     res.status(400).json({ errors: validationResponse });
//   }
//
//   try {
//     if (validationResponse === true) {
//       const newAmountSave = Total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
//       /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
//       const Inputcurrency = userCurrency;
//       const currencies = await models.currency.findAll();
//
//       const getCurrenciesTocheck = async (currencyData) => {
//         // Cette ligne verify la conversion de la somme en €
//         const newAmountSaveConverted = (newAmountSave / currencyData.currencyNumber);
//
//         switch (Inputcurrency) {
//           case 'CAD':
//             if (Inputcurrency === currencyData.code) {
//               rechargeService.rechargeForUser(
//                 req,
//                 res,
//                 newAmountSave,
//                 newAmountSaveConverted,
//                 Title,
//                 userCurrency,
//                 userIdRecev,
//                 categoryTransation,
//                 userId,
//                 couponNumber,
//               );
//             }
//             break;
//           case 'EUR':
//             if (Inputcurrency === currencyData.code) {
//               rechargeService.rechargeForUser(
//                 req,
//                 res,
//                 newAmountSave,
//                 newAmountSaveConverted,
//                 Title,
//                 userCurrency,
//                 userIdRecev,
//                 categoryTransation,
//                 userId,
//                 couponNumber,
//               );
//             }
//             break;
//           case 'GBP':
//             if (Inputcurrency === currencyData.code) {
//               rechargeService.rechargeForUser(
//                 req,
//                 res,
//                 newAmountSave,
//                 newAmountSaveConverted,
//                 Title,
//                 userCurrency,
//                 userIdRecev,
//                 categoryTransation,
//                 userId,
//                 couponNumber,
//               );
//             }
//             break;
//           case 'USD':
//             if (Inputcurrency === currencyData.code) {
//               rechargeService.rechargeForUser(
//                 req,
//                 res,
//                 newAmountSave,
//                 newAmountSaveConverted,
//                 Title,
//                 userCurrency,
//                 userIdRecev,
//                 categoryTransation,
//                 userId,
//                 couponNumber,
//               );
//             }
//             break;
//           case 'XAF':
//             if (Inputcurrency === currencyData.code) {
//               rechargeService.rechargeForUser(
//                 req,
//                 res,
//                 newAmountSave,
//                 newAmountSaveConverted,
//                 Title,
//                 userCurrency,
//                 userIdRecev,
//                 categoryTransation,
//                 userId,
//                 couponNumber,
//               );
//             }
//             break;
//           case 'XOF':
//             if (Inputcurrency === currencyData.code) {
//               rechargeService.rechargeForUser(
//                 req,
//                 res,
//                 newAmountSave,
//                 newAmountSaveConverted,
//                 Title,
//                 userCurrency,
//                 userIdRecev,
//                 categoryTransation,
//                 userId,
//                 couponNumber,
//               );
//             }
//             break;
//           default:
//           { // accolade ajoutée
//             console.log('Aucune action reçue.');
//             break;
//           }
//         }
//       };
//
//       for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
//     } else {
//       res.status(400).json({
//         errors: validationResponse,
//       });
//     }
//   } catch (error) {
//     res.status(500).json({
//       message: 'Something went wrong',
//       error,
//     });
//   }
// };

const paypalcharge = async (req, res) => {
  const { inputs } = req.body;

  const payReq = {
    intent: 'sale',
    payer: {
      paymentMethod: 'paypal',
    },
    // eslint-disable-next-line camelcase
    redirect_urls: {
      returnUrl: 'http://localhost:8080/api/success',
      cancelUrl: 'http://localhost:3000/cancel',
    },
    transactions: [{
      amount: {
        currency: inputs.currency,
        total: inputs.total,
      },
      description: '',
    }],
  };

  paypal.payment.create(payReq, async (error, payment) => {
    if (error) {
      throw error;
    } else {
      for (let i = 0; i < payment.links.length; i++) {
        if (payment.links[i].rel === 'approval_url') {
          res.redirect(payment.links[i].href);
        }
      }
    }
  });
};

const paypalsucces = async (req, res) => {
  const { paymentId } = req.query;
  const payerId = { payerId: req.query.PayerID };

  try {
    paypal.payment.execute(paymentId, payerId, async (err, payment) => {
      if (err) {
        console.log(err.response);
        throw err;
      } else {
        if (!payment) {
          return res.status(400).json({ message: 'Identifiants incorrecte' });
        }
        /** Ici je sauvegade les donner dans la base de donne */
        const userRecev = req.AuthUser.userId;
        const newAmountSave = payment.transactions[0].amount.total;
        const itemRecev = {
          title: 'Paypal recharge',
          userId: userRecev,
          currency: payment.transactions[0].amount.currency,
          invoceNumer: payerId,
          tokenTransaction: req.query.token,
          paymentId,
          statusSend: true,
          slugin: makeSluginID(30),
        };
        const transaction = await models.transaction.create(itemRecev);
        if (!transaction) {
          return res.status(400).json({ message: 'Données incorrecte' });
        }

        const newAmountuserRecev = +newAmountSave;
        const itemAmountRecev = {
          total: newAmountSave,
          currency: payment.transactions[0].amount.currency,
          transactionId: transaction.id,
        }; // Le + va alle ajouter le solde dans le conte de la personne qui envoi
        const amount = await models.amount.create(itemAmountRecev);
        const itemAmountUserRecev = {
          amountUser: newAmountuserRecev,
          amountId: amount.id,
          userId: userRecev,
        }; // Cette ligne c'est pour sauvegarder la reception

        await models.amountuser.create(itemAmountUserRecev);

        return res.status(200).json({
          transaction,
        });
      }
    });
  } catch (error) {
    res.status(500).json({
      error,
    });
  }
};

const stripecharge = async (req, res) => {
  const { inputs, paymentMethod } = req.body;

  try {
    const charge = await stripe.paymentIntents.create({
      amount: inputs.total * 100, // 25
      currency: inputs.currency,
      description: 'Stripe charge',
      // eslint-disable-next-line camelcase
      payment_method: paymentMethod.id,
      confirm: true,
    });

    if (!charge) {
      res.status(400).json({ message: 'Identifiants incorrecte' });
    }
    /** Ici je sauvegade les donner dans la base de donne **/

    const userIdRecev = req.AuthUser.userId;
    const userRecev = req.AuthUser;
    const categoryTransation = 1; // Id recharge
    const newAmountSave = (charge.amount / 100);
    const itemRecev = {
      title: charge.description,
      userId: userIdRecev,
      usertoId: userIdRecev,
      currency: charge.currency,
      categoryId: categoryTransation,
      statusSend: true,
      slugin: makeSluginID(30),
      invoiceNumber: makeSluginNumber(12),
      tokenTransaction: makeSluginID(60),
    };
    const transaction = await models.transaction.create(itemRecev);
    if (!transaction) {
      res.status(400).json({ message: 'Données incorrecte' });
    }
    // Le + va alle ajouter le solde dans le conte de la personne qui envoi
    const newAmountuserRecev = +newAmountSave;
    const itemAmountRecev = {
      total: newAmountSave,
      currency: inputs.currency,
      transactionId: transaction.id,
    };
    const amount = await models.amount.create(itemAmountRecev);
    // Cette ligne c'est pour sauvegarder la reception
    const itemAmountUserRecev = {
      amountUser: newAmountuserRecev,
      amountId: amount.id,
      userId: userIdRecev,
    };

    await models.amountuser.create(itemAmountUserRecev);

    await TransactionMail.transactionCouponMail(userRecev, transaction, itemAmountRecev);
    res.status(200).json({
      transaction,
      success: true,
    });
  } catch (error) {
    res.status(500).json({
      error,
    });
  }
};

module.exports = {
  index,
  // store,
  paypalcharge,
  paypalsucces,
  stripecharge,
};
