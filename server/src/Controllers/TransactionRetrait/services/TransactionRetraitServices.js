const models = require('../../../../models');
const rechargeService = require('./TransactionRetraitServicesTraitement');

const anountTraitementretraitUser = async (
  req,
  res,
  payementmethod,
  payementmethoduserItem,
  categoryTransation,
  countryRetrait,
  Title,
  userCurrency,
  userSend,
  userIdSend,
) => {
  // const taxeTransaction = (req.body.total * 10) / 100
  // Ici c'est pour calculer le pourcentage de retrait
  /** Ici je commence a verifier si l'utilisateur a un solde positif ou > 0 */
  const amountuserCheck = await models.amountuser.sum('amountUser', { where: { userId: userIdSend } });
  const newAmountSave = req.body.total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();

  const getCurrenciesTocheck = async (currencyData) => {
    const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
    const taxeTransaction = (newAmountConverted * payementmethod.taxeTransaction) / 100;
    const newAmountSaveConverted = (newAmountConverted + taxeTransaction);
    // let newAmountSendConverted = newAmountConverted;
    // let newAmountRecevConverted = newAmountConvertedTaxeTotal;
    // let validateAmountForSave = amountuserCheck > newAmountSaveConverted;

    const minAmountRetrait = 2; // C'est le montant minimum de retrait en €
    const validateAmountForSend = newAmountSaveConverted > minAmountRetrait;
    const validateAmountForSave = amountuserCheck > newAmountSaveConverted;
    switch (Inputcurrency) {
      case 'CAD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSend) {
            if (validateAmountForSave) {
              rechargeService.transactionretraitUser(
                req,
                res,
                payementmethoduserItem,
                userSend,
                userIdSend,
                newAmountConverted,
                newAmountSaveConverted,
                categoryTransation,
                taxeTransaction,
                countryRetrait,
                Title,
                userCurrency,
                currencyData,
              );
            } else {
              res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
            }
          } else {
            res.status(400).json({ message: `Le montant de retrait doit etre supérieur ou égal à ${minAmountRetrait * currencyData.currencyNumber} ${currencyData.code}` });
          }
        }
        break;
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSend) {
            if (validateAmountForSave) {
              rechargeService.transactionretraitUser(
                req,
                res,
                payementmethoduserItem,
                userSend,
                userIdSend,
                newAmountConverted,
                newAmountSaveConverted,
                categoryTransation,
                taxeTransaction,
                countryRetrait,
                Title,
                userCurrency,
                currencyData,
              );
            } else {
              res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
            }
          } else {
            res.status(400).json({ message: `Le montant de retrait doit etre supérieur ou égal à ${minAmountRetrait * currencyData.currencyNumber} ${currencyData.code}` });
          }
        }
        break;
      case 'GBP':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSend) {
            if (validateAmountForSave) {
              rechargeService.transactionretraitUser(
                req,
                res,
                payementmethoduserItem,
                userSend,
                userIdSend,
                newAmountConverted,
                newAmountSaveConverted,
                categoryTransation,
                taxeTransaction,
                countryRetrait,
                Title,
                userCurrency,
                currencyData,
              );
            } else {
              res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
            }
          } else {
            res.status(400).json({ message: `Le montant de retrait doit etre supérieur ou égal à ${minAmountRetrait * currencyData.currencyNumber} ${currencyData.code}` });
          }
        }
        break;
      case 'USD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSend) {
            if (validateAmountForSave) {
              rechargeService.transactionretraitUser(
                req,
                res,
                payementmethoduserItem,
                userSend,
                userIdSend,
                newAmountConverted,
                newAmountSaveConverted,
                categoryTransation,
                taxeTransaction,
                countryRetrait,
                Title,
                userCurrency,
                currencyData,
              );
            } else {
              res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
            }
          } else {
            res.status(400).json({ message: `Le montant de retrait doit etre supérieur ou égal à ${minAmountRetrait * currencyData.currencyNumber} ${currencyData.code}` });
          }
        }
        break;
      case 'XAF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSend) {
            if (validateAmountForSave) {
              rechargeService.transactionretraitUser(
                req,
                res,
                payementmethoduserItem,
                userSend,
                userIdSend,
                newAmountConverted,
                newAmountSaveConverted,
                categoryTransation,
                taxeTransaction,
                countryRetrait,
                Title,
                userCurrency,
                currencyData,
              );
            } else {
              res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
            }
          } else {
            res.status(400).json({ message: `Le montant de retrait doit etre supérieur ou égal à ${minAmountRetrait * currencyData.currencyNumber} ${currencyData.code}` });
          }
        }
        break;
      case 'XOF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSend) {
            if (validateAmountForSave) {
              rechargeService.transactionretraitUser(
                req,
                res,
                payementmethoduserItem,
                userSend,
                userIdSend,
                newAmountConverted,
                newAmountSaveConverted,
                categoryTransation,
                taxeTransaction,
                countryRetrait,
                Title,
                userCurrency,
                currencyData,
              );
            } else {
              res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
            }
          } else {
            res.status(400).json({ message: `Le montant de retrait doit etre supérieur ou égal à ${minAmountRetrait * currencyData.currencyNumber} ${currencyData.code}` });
          }
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      } // accolade ajoutée
    }
  };
  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

module.exports = {
  anountTraitementretraitUser,
};
