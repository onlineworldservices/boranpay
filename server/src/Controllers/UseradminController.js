const { Op } = require('sequelize');
const models = require('../../models');
const NewAdminUserMail = require('../Mail/Auth/NewAdminUserMail');

const search = async (req, res) => {
  const { fileQuery } = req.query;
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    const users = await models.user.findAll({
      where: {
        [Op.or]: [
          { email: { [Op.like]: `%${fileQuery}%` } },
          { firstName: { [Op.like]: `%${fileQuery}%` } },
          { lastName: { [Op.like]: `%${fileQuery}%` } },
          { '$profile.country.name$': { [Op.like]: `%${fileQuery}%` } },
        ],
      },
      include: [
        {
          model: models.profile,
          attributes: ['slugin'],
          required: true,
          include: [
            { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'], required: true },
            {
              model: models.country,
              attributes: ['name', 'code', 'capital'],
              required: true,
              include: [{ model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'], required: true }],
            },
          ],
        },
        {
          model: models.ability,
          as: 'ability',
          attributes: ['action', 'subject'],
          required: true,
        },
      ],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const allusers = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    const users = await models.user.findAll({
      include: [
        {
          model: models.profile,
          attributes: ['slugin'],
          required: true,
          include: [
            { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'], required: true },
            {
              model: models.country,
              attributes: ['name', 'code', 'capital'],
              // required: true,
              include: [{ model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'], required: true }],
            },
          ],
        },
        {
          model: models.ability,
          as: 'ability',
          attributes: ['action', 'subject'],
          required: true,
        },
      ],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const alluseradmins = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    const users = await models.useradmin.findAll({
      include: [
        { model: models.organisation, attributes: ['name'] },
        { model: models.country, attributes: ['name', 'code', 'capital'], required: true },
        { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'], required: true },
        {
          model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
        },
      ],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const adminsshow = async (req, res) => {
  try {
    const groupe = await models.useradmin.findOne({
      where: { slugin: req.params.useradmin },
      include: [
        { model: models.country, attributes: ['name', 'code', 'capital'], required: true },
        { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'], required: true },
        {
          model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
        },
      ],
    });
    res.status(200).json(groupe);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const adminsdelete = async (req, res) => {
  try {
    await models.useradmin.destroy({ where: { slugin: req.params.useradmin } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const adminssendemail = async (req, res) => {
  try {
    const userRecev = await models.useradmin.findOne({ where: { slugin: req.params.useradmin } });
    NewAdminUserMail.newadminuserMail(userRecev);
    res.status(200).json({
      message: 'Data send successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  search,
  allusers,
  alluseradmins,
  adminsshow,
  adminssendemail,
  adminsdelete,
};
