const Validator = require('fastest-validator');
const mySlug = require('slug');
const { makeSluginID } = require('../../helper/utils');
const models = require('../../models');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const categories = await models.category.findAll({
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(categories);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
async function store(req, res) {
  const item = {
    name: req.body.name,
    label: req.body.label,
    slugin: makeSluginID(30),
    slug: mySlug(req.body.name),
    status: false,
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
    status: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  await models.category.create(item).then((result) => {
    res.status(200).json({
      message: 'Data save successfully',
      category: result,
    });
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

/** Ici je Show une donner les donners de la base de donner **/
async function show(req, res) {
  const item = req.params.slugin;

  await models.category.findOne({ where: { slugin: item } }).then((result) => {
    res.status(200).json(result);
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

/** Ici je Update la donne */
async function update(req, res) {
  const item = req.params.slugin;

  const dataUpdate = {
    name: req.body.name,
    label: req.body.label,
    slug: mySlug(req.body.name),
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  await models.category.update(dataUpdate, { where: { slugin: item } }).then(() => {
    res.status(200).json({
      message: 'Data update successfully',
    });
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

module.exports = {
  index,
  store,
  show,
  update,
};
