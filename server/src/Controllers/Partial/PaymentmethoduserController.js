const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const payementmethodusers = await models.payementmethoduser.findAndCountAll({
      where: { isDelete: false },
      include: [
        {
          model: models.user,
          where: { slugin: req.params.user },
          attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        { model: models.payementmethod, attributes: ['region', 'name', 'countryId'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(payementmethodusers);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const store = async (req, res) => {
  const {
    fullName, countryName, payementmethodId, contactRecev,
  } = req.body;

  const item = {
    fullName,
    countryName,
    payementmethodId,
    contactRecev,
    userId: req.AuthUser.userId,
    slugin: makeSluginID(15),
  };

  const schema = {
    contactRecev: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const payementmethoduser = await models.payementmethoduser.create(item);
    res.status(200).json({ payementmethoduser });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const update = async (req, res) => {
  const {
    fullName, countryName, payementmethodId, contactRecev,
  } = req.body;

  const dataUpdate = {
    fullName,
    countryName,
    payementmethodId,
    contactRecev,
  };

  const schema = {
    contactRecev: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.payementmethoduser.update(dataUpdate,
        { where: { slugin: req.params.payementmethoduser } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const payementmethoduser = await models.payementmethoduser.findOne({
      where: { slugin: req.params.payementmethoduser, isDelete: false },
      include: [
        { model: models.user, attributes: ['firstName', 'lastName', 'email', 'slugin', 'avatar'] },
        { model: models.payementmethod, attributes: ['region', 'name', 'countryId'] },
      ],
    });
    res.status(200).json(payementmethoduser);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const changestatus = async (req, res) => {
  const dataUpdate = { isDelete: true };
  try {
    await models.payementmethoduser.update(dataUpdate,
      { where: { slugin: req.params.payementmethoduser } });

    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  show,
  store,
  update,
  changestatus,
};
