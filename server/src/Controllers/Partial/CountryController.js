const Validator = require('fastest-validator');
/** const { Sequelize } = require('sequelize'); */
const mySlug = require('slug');
const models = require('../../../models');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const countries = await models.country.findAll({
      attributes: ['id', 'slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
      include: [
        {
          model: models.currency,
          attributes: ['code', 'name', 'symbol', 'currencyNumber'],
        },
      ],
      order: [['name', 'asc']],
    });
    res.status(200).json(countries);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const store = async (req, res) => {
  const country = await models.country.findOne({ where: { name: req.body.name } });
  if (country) {
    return res.status(400).json({ message: 'This country exist change' });
  }

  const item = {
    name: req.body.name,
    code: req.body.code,
    slug: mySlug(req.body.name),
    region: req.body.region,
    capital: req.body.capital,
    currencyId: req.body.currencyId,
    statusMovesold: req.body.statusMovesold,
    phoneCode: req.body.phoneCode,
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
    region: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const response = await models.country.create(item);
    return res.status(200).json({ response });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const update = async (req, res) => {
  const dataUpdate = {
    name: req.body.name,
    code: req.body.code,
    slug: mySlug(req.body.name),
    region: req.body.region,
    capital: req.body.capital,
    currencyId: req.body.currencyId,
    statusMovesold: req.body.statusMovesold,
    phoneCode: req.body.phoneCode,
  };

  const schema = {
    name: { type: 'string', min: 3, max: 200 },
    region: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.country.update(dataUpdate, { where: { slug: req.params.country } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const country = await models.country.findOne({
      where: { slug: req.params.country },
      attributes: ['id', 'currencyId', 'slug', 'code', 'name', 'region', 'capital', 'statusMovesold', 'statusRechargesold'],
      include: [
        {
          model: models.currency,
          attributes: ['code', 'name', 'symbol', 'currencyNumber'],
        },
      ],
    });
    res.status(200).json(country);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je recuprer tous les pays d'ou l'on peux faire le retrait */
const countriesretrait = async (req, res) => {
  try {
    const countries = await models.country.findAll({
      where: { statusMovesold: true },
      attributes: ['id', 'slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
      include: [
        { model: models.currency, attributes: ['code', 'name', 'symbol', 'currencyNumber'] },
        /**
                 * {
                    model: models.retrait,
                    where: { status_movesold: false },
                    attributes: [
                        'country_id',
                        [Sequelize.fn('COUNT', Sequelize.col('countryId')), 'retraitsCount']],
                    group: ['countryId'],
                    limit: 999999999999999,
                    // Bon ici c'est pour corriger le bug de sequelize pour calculer bien
                },
                 */
      ],
      order: [['name', 'DESC']],
    });
    res.status(200).json(countries);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const destroy = async (req, res) => {
  try {
    await models.country.destroy({ where: { slug: req.params.country } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  show,
  store,
  update,
  destroy,
  countriesretrait,
};
