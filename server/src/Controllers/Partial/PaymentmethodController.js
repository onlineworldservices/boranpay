const Validator = require('fastest-validator');
const mySlug = require('slug');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');
const { typePayementValidate } = require('../../../helper/validatorvalue');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const payementmethods = await models.payementmethod.findAll({
      include: [
        {
          model: models.country,
          attributes: ['id', 'slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
          include: [
            {
              model: models.currency,
              attributes: ['code', 'name', 'symbol', 'currencyNumber'],
            },
          ],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(payementmethods);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const store = async (req, res) => {
  const {
    name, region, typePayement, taxeTransaction, countryId,
  } = req.body;

  const item = {
    name,
    slug: `${mySlug(name)}-${makeSluginID(6)}`,
    slugin: `${mySlug(name)}-${makeSluginID(8)}`,
    region,
    typePayement,
    taxeTransaction,
    countryId,
  };

  const schema = {
    typePayement: { type: 'enum', values: typePayementValidate },
    region: { type: 'string', min: 3, max: 200 },
    name: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const payementmethod = await models.payementmethod.create(item);
    res.status(200).json({ payementmethod });
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const update = async (req, res) => {
  const {
    name, region, typePayement, taxeTransaction, countryId,
  } = req.body;

  const dataUpdate = {
    name,
    slug: mySlug(name),
    region,
    typePayement,
    taxeTransaction,
    countryId,
  };

  const schema = {
    typePayement: { type: 'enum', values: typePayementValidate },
    region: { type: 'string', min: 3, max: 200 },
    name: { type: 'string', min: 3, max: 200 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await models.payementmethod.update(dataUpdate, { where: { slugin: req.params.slugin } });

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const payementmethod = await models.payementmethod.findOne({
      where: { slugin: req.params.slugin },
      include: [
        {
          model: models.country,
          attributes: ['id', 'slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
          include: [
            {
              model: models.currency,
              attributes: ['code', 'name', 'symbol', 'currencyNumber'],
            },
          ],
        },
      ],
    });
    res.status(200).json(payementmethod);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const payementmethodretrait = async (req, res) => {
  try {
    const payementmethods = await models.payementmethod.findAll({
      where: { typePayement: 'retrait' },
      include: [
        {
          model: models.country,
          attributes: ['id', 'slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
          include: [
            {
              model: models.currency,
              attributes: ['code', 'name', 'symbol', 'currencyNumber'],
            },
          ],
        },
      ],
      order: [['name', 'DESC']],
    });
    res.status(200).json(payementmethods);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const payementmethodrecharge = async (req, res) => {
  try {
    const payementmethods = await models.payementmethod.findAll({
      where: { typePayement: 'recharge' },
      attributes: ['name', 'slug', 'typePayement'],
      order: [['name', 'asc']],
    });
    res.status(200).json(payementmethods);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const destroy = async (req, res) => {
  try {
    await models.payementmethod.destroy({ where: { slugin: req.params.slugin } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  show,
  store,
  update,
  destroy,
  payementmethodretrait,
  payementmethodrecharge,
};
