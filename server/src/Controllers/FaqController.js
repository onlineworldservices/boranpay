const Validator = require('fastest-validator');
const mySlug = require('slug');
const { makeSluginID } = require('../../helper/utils');
const models = require('../../models');

/** Ici je Get tous les donners de la base de donner **/
const index = async (req, res) => {
  try {
    const faqs = await models.faq.findAll({
      order: [
        ['createdAt', 'DESC'],
      ],
    });
    res.status(200).json(faqs);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  const item = {
    title: req.body.title,
    content: req.body.content,
    slugin: makeSluginID(30),
    slug: mySlug(req.body.title),
    status: false,
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
    status: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const faq = await models.faq.create(item);
    return res.status(200).json({
      message: 'Data save successfully',
      faq,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const faq = await models.faq.findOne({ where: { slugin: req.params.slugin } });
    res.status(200).json(faq);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  const dataUpdate = {
    title: req.body.title,
    content: req.body.content,
    slug: mySlug(req.body.title),
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    await models.faq.update(dataUpdate, { where: { slugin: req.params.slugin } });
    return res.status(200).json({
      message: 'Data update successfully',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const statusChange = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    const faq = await models.faq.findOne({ where: { slugin: req.params.slugin } });
    if (!faq) {
      return res.status(400).json({ message: "FAQ don't exist" });
    }

    const dataUpdate = { status: !faq.status };

    await faq.update(dataUpdate);

    return res.status(200).json({ faq });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};

/** Ici je Delete  la donne */
const destroy = async (req, res) => {
  const userInfo = await models.useradmin.findOne({ where: { slugin: req.AuthUser.slugin } });
  if (!userInfo) {
    return res.status(400).json({ message: 'User not defined please check or contact administrator' });
  }

  try {
    await models.faq.destroy({ where: { slugin: req.params.slugin } });
    return res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  statusChange,
  update,
  destroy,
};
