const Stripe = require('stripe');
const models = require('../../../../models');
const { makeSluginID, makeSluginNumber, makeParseIp } = require('../../../../helper/utils');

const stripe = new Stripe(process.env.STRIPE_PRIVATE_KEY);

const stripedonationcharge = async (
  req,
  res,
  newAmountRecevConverted,
  currencyData,
  categoryTransation,
  newAmountConverted,
  newAmountConvertedTaxe,
  Title,
  userCurrency,
  userIdSend,
  userIdRecev,
  donation,
) => {
  const {
    content, statusUser, statusTotal, paymentMethod,
  } = req.body;
  const charge = await stripe.paymentIntents.create({
    amount: (newAmountConverted * currencyData.currencyNumber) * 100, // 25
    currency: userCurrency,
    description: 'Stripe contribution donation',
    // eslint-disable-next-line camelcase
    payment_method: paymentMethod.id,
    confirm: true,
  });

  if (!charge) {
    res.status(400).json({ message: 'Identifiants incorrecte' });
  }

  /** ces variable ci dessous c'est pour declarer les donner a sauvergarder */
  const myTaxTransaction = newAmountConvertedTaxe * currencyData.currencyNumber; // get taxe
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content,
    donationId: donation.id,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };
  const transactionRecev = await models.transactiondonation.create(itemRecev);

  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    taxeTransaction: myTaxTransaction,
    currency: userCurrency,
    transactiondonationId: transactionRecev.id,
    ip: makeParseIp(req),
  };

  const amount = await models.amount.create(itemAmountRecev);
  if (!amount) { res.status(400).json({ message: 'Invalid data' }); }
  // Le + va alle ajouter le solde dans le conte de la personne qui envoi
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserRecev = {
    amountDonation: newAmountuserRecev,
    amountDonationNoTaxe: myTaxTransaction,
    amountId: amount.id,
    userId: userIdRecev,
    donationId: donation.id,
    ip: makeParseIp(req),
  }; // Cette ligne c'est pour sauvegarder la reception

  const responseRecev = await models.amountdonation.create(itemAmountUserRecev);
  if (!responseRecev) { res.status(400).json({ message: 'Invalid data' }); }

  if (categoryTransation === 4) {
    const itemContribute = {
      userId: userIdSend,
      content,
      statusUser,
      statusTotal,
      taxeContribute: myTaxTransaction,
      amountContribute: responseRecev.amountDonation,
      donationId: donation.id,
      currency: userCurrency,
      transactiondonationId: transactionRecev.id,
      total: (((responseRecev.amountDonation) * currencyData.currencyNumber) + myTaxTransaction),
      slugin: makeSluginID(30),
      ip: makeParseIp(req),
    };
    const contribute = await models.contribute.create(itemContribute);
    if (!contribute) { res.status(400).json({ message: 'Invalid data' }); }
  }

  res.status(200).json({
    success: true,
    responseRecev,
  });
};

module.exports = {
  stripedonationcharge,
};
