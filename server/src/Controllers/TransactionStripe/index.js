const Validator = require('fastest-validator');
const { makeSluginID } = require('../../../helper/utils');
const models = require('../../../models');
const transactionService = require('./services/TransactionStripeService');
const { currencyValidate } = require('../../../helper/validatorvalue');

const stripechargedonation = async (req, res) => {
  const donation = await models.donation.findOne({ where: { slugin: req.params.donation } });
  const Title = `Contribution donation - ${donation.title}`;
  const TotalAmount = req.body.total;
  const userIdSend = req.AuthUser.userId;
  const userIdRecev = donation.userId;
  const userCurrency = req.body.currency;
  const categoryTransation = 4; // Ici c'est la category de la transaction

  const itemSend = {
    title: Title,
    userId: userIdSend,
    content: req.body.content,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
  };

  const schema = {
    userId: { type: 'string', positive: true, integer: true },
    currency: { type: 'enum', values: currencyValidate },
    statusSend: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(itemSend, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      transactionService.anountTraitementdonationUser(
        req,
        res,
        categoryTransation,
        Title,
        TotalAmount,
        userCurrency,
        userIdRecev,
        userIdSend,
        donation,
      );
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  stripechargedonation,
};
