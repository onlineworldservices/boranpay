/** const { Sequelize } = require('sequelize'); */
const models = require('../../models');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    const transactions = await models.transaction.findAll({
      include: [
        { model: models.user, as: 'userto', attributes: ['id', 'email', 'firstName', 'lastName', 'slugin', 'avatar'] },
        { model: models.user, attributes: ['id', 'email', 'firstName', 'lastName', 'avatar'] },
        { model: models.category, attributes: ['id', 'name', 'slugin', 'slugin'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Show une donner les donners de la base de donner **/
const show = async (req, res) => {
  try {
    const transaction = await models.transaction.findOne({
      where: { slugin: req.params.transaction },
      include: [
        { model: models.category, attributes: ['name', 'slug'] },
        {
          model: models.user,
          as: 'userto',
          required: true,
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar', 'lastName', 'phone'],
          include: [
            {
              model: models.profile,
              required: true,
              include: [
                { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
                { model: models.country, attributes: ['code', 'name'] },
              ],
            },
          ],
        },
        {
          model: models.user,
          required: true,
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar', 'lastName', 'phone'],
          include: [
            {
              model: models.profile,
              required: true,
              include: [
                { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
                { model: models.country, attributes: ['code', 'name'] },
              ],
            },
          ],
        },
        {
          model: models.amount,
          required: true,
          attributes: ['id', 'total', 'taxeTransaction', 'currency'],
          include: [{ model: models.amountuser, attributes: ['amountId', 'amountUser'] }],
        },
      ],
    });

    res.status(200).json(transaction);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const showTransauser = async (req, res) => {
  const item = req.params.user;

  try {
    const transactions = await models.transaction.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: item },
          attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'],
        },
        { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};
/** Ici je Get tous les transctions faites par l'utilisateur */
const showTransauserSend = async (req, res) => {
  const item = req.params.user;

  try {
    const transactions = await models.transaction.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: item },
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      where: { statusSend: false },
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};
/** Ici je Get tous les transctions faites par l'utilisateur */
const showTransauserSenduser = async (req, res) => {
  const userSend = req.params.usersend;
  const userRecev = req.params.userrecev;

  try {
    const transactions = await models.transaction.findAll({
      include: [
        {
          model: models.user,
          as: 'userto',
          where: { slugin: userRecev },
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        {
          model: models.user,
          where: { slugin: userSend },
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      // where: { statusSend: false },
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Get tous les transctions recus par l'utilisateur */
const showTransauserRecev = async (req, res) => {
  const item = req.params.user;

  try {
    const transactions = await models.transaction.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: item },
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      where: { statusSend: true },
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const showTransaCagnoteuserRecev = async (req, res) => {
  const item = req.params.user;
  try {
    const transactioncagnotes = await models.transactioncagnote.findAll({
      include: [
        {
          model: models.user,
          where: { slugin: item },
          attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'],
        },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },
      ],
      where: { statusSend: true },
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactioncagnotes);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Get tous les transctions des retaits faitent par l'utilisateur */
const showTransauserRetrait = async (req, res) => {
  const item = req.params.user;
  const categoryID = 2; // Ici c'est pour les retrais

  try {
    const transactions = await models.transaction.findAll({
      include: [
        {
          model: models.retrait,
          include: [
            {
              model: models.payementmethoduser,
              attributes: ['contactRecev', 'fullName', 'payementmethodId'],
              include: [{ model: models.payementmethod, attributes: ['name'] }],
            },
          ],
        },
        {
          model: models.user,
          where: { slugin: item },
          attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'],
        },
        {
          model: models.category,
          where: { id: categoryID },
          attributes: ['id', 'name'],
        },
        { model: models.amount, attributes: ['id', 'total', 'taxeTransaction', 'currency'] },

      ],
      where: { categoryId: categoryID },
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(transactions);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  showTransauserSend,
  showTransauserSenduser,
  showTransauserRecev,
  showTransaCagnoteuserRecev,
  showTransauser,
  showTransauserRetrait,
  show,
};
