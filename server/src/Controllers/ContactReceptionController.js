const models = require('../../models');
const { makeSluginID, makeParseIp } = require('../../helper/utils');

/** Ici je Post les donners de la base de donner **/
const store = async (req, res) => {
  const item = {
    title: req.body.title,
    content: req.body.content,
    slug: makeSluginID(30),
    ip: makeParseIp(req),
  };

  try {
    const contactreception = await models.contactreception.create(item);
    res.status(200).json(contactreception);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  const item = req.params.slug;

  try {
    const contactreception = await models.contactreception.findOne({ where: { slug: item } });
    res.status(200).json(contactreception);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je met à jours les données */
const update = async (req, res) => {
  const item = req.params.slug;

  const dataUpdate = {
    title: req.body.title,
    content: req.body.content,
  };

  try {
    await models.contactreception.update(dataUpdate, { where: { slug: item } });
    res.status(200).json({
      message: 'Data updated successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je supprime les données */
const destroy = async (req, res) => {
  try {
    await models.contactreception.destroy({ where: { slug: req.params.slug } });
    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  store,
  update,
  show,
  destroy,

};
