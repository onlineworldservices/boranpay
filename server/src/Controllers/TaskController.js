const Validator = require('fastest-validator');
const { v4: uuidv4 } = require('uuid');
const models = require('../../models');

/** Ici je Get tous les donners de la base de donner **/
async function index(req, res) {
  await models.task.findAll({
    include: [
      {
        model: models.user,
        attributes: ['id', 'firstName', 'avatar'],
      }],
    order: [['createdAt', 'DESC']],

  }).then((result) => {
    res.status(200).json(result);
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

/** Ici je Post les donners de la base de donner **/
async function store(req, res) {
  const item = {
    title: req.body.title,
    content: req.body.content,
    userId: req.AuthUser.userId,
    slugin: uuidv4(),
    status: false,
  };

  const schema = {
    title: 'string|required|min:3|max:200',
    content: 'string|required|min:3|max:5000',
    status: 'boolean', // short-hand def
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  await models.task.create(item).then((result) => {
    res.status(200).json({
      message: 'Data save successfully',
      task: result,
    });
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

/** Ici je Show une donner les donners de la base de donner **/
async function show(req, res) {
  const item = req.params.slugin;

  await models.task.findOne({ where: { slugin: item } }).then((result) => {
    res.status(200).json(result);
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

/** Ici je Update la donne */
async function update(req, res) {
  const item = req.params.slugin;

  const dataUpdate = {
    title: req.body.title,
    content: req.body.content,
  };

  const schema = {
    title: 'string|required|min:3|max:200',
    content: 'string|required|min:3|max:5000',
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  await models.task.update(dataUpdate, { where: { slugin: item } }).then(() => {
    res.status(200).json({
      message: 'Data update successfully',
    });
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

/** Ici je Delete  la donne */
async function destroy(req, res) {
  const item = req.params.slugin;

  await models.task.destroy({ where: { slugin: item } }).then(() => {
    res.status(200).json({
      message: 'Data delete successfully',
    });
  }).catch((error) => {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  });
}

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
};
