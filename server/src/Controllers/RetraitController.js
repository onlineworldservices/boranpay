const Validator = require('fastest-validator');
const models = require('../../models');

const index = async (req, res) => {
  try {
    const retraits = await models.retrait.findAll({
      include: [
        {
          model: models.country,
          attributes: ['slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
          include: [{ model: models.currency, attributes: ['code', 'name', 'symbol', 'currencyNumber'] }],
        },
        {
          model: models.payementmethod,
          required: true,
          attributes: ['name', 'region', 'slugin'],
        },
        {
          model: models.payementmethoduser,
          attributes: ['contactRecev', 'fullName', 'payementmethodId', 'countryName'],
          include: [
            { model: models.payementmethod, attributes: ['name', 'slugin'] },
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'avatar'] },
          ],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(retraits);
  } catch (error) {
    res.status(500).json({
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const retrait = await models.retrait.findOne({
      where: { slugin: req.params.retrait },
      include: [
        {
          model: models.transaction,
          attributes: ['statusSend', 'invoiceNumber', 'categoryId'],
          include: [
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'avatar'] },
            { model: models.category, attributes: ['name'] },
            { model: models.amount, attributes: ['total', 'currency'] },
          ],
        },
        {
          model: models.payementmethod,
          required: true,
          attributes: ['name', 'region', 'slugin'],
        },
        {
          model: models.payementmethoduser,
          attributes: ['contactRecev', 'fullName', 'payementmethodId', 'slugin', 'countryName'],
          include: [
            { model: models.payementmethod, attributes: ['name'] },
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'avatar'] },
          ],
        },
      ],
    });
    res.status(200).json(retrait);
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

/** Ici je Update la donne */
const update = async (req, res) => {
  const country = await models.country.findOne({ where: { name: req.body.country }, include: [{ model: models.currency, attributes: ['code'] }] });
  if (!country) {
    return res.status(400).json({ message: "Ce pays n'existe pas veuillez changer ou essayer plus tard" });
  }

  const dataUpdate = {
    contactRecev: req.body.contactRecev,
    payementmethodId: req.body.payementmethodId,
    countryId: country.id,
  };

  const schema = {
    contactRecev: { type: 'string', min: 3, max: 200 },
    payementmethodId: { type: 'number', positive: true, integer: true },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    await models.retrait.update(dataUpdate, { where: { slugin: req.params.retrait } });
    return res.status(200).json({
      message: 'Data update successfully',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const retraitscountryshow = async (req, res) => {
  try {
    const retraits = await models.retrait.findAll({
      include: [
        {
          model: models.payementmethoduser,
          attributes: ['contactRecev', 'fullName', 'payementmethodId', 'countryName'],
          include: [
            { model: models.payementmethod, attributes: ['name', 'slugin'] },
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'slugin', 'email', 'avatar'] },
          ],
        },
        {
          model: models.payementmethod,
          required: true,
          attributes: ['name', 'region', 'slugin'],
        },
        {
          model: models.country,
          where: { slug: req.params.countryslug },
          required: true,
          attributes: ['slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
          include: [{ model: models.currency, attributes: ['code', 'name', 'symbol', 'currencyNumber'] }],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(retraits);
  } catch (error) {
    res.status(500).json({
      error,
    });
  }
};

const retraitspmtshow = async (req, res) => {
  try {
    const retraits = await models.retrait.findAll({
      include: [
        {
          model: models.country,
          attributes: ['slug', 'code', 'name', 'region', 'statusMovesold', 'statusRechargesold'],
          include: [{ model: models.currency, attributes: ['code', 'name', 'symbol', 'currencyNumber'] }],
        },
        {
          model: models.payementmethod,
          where: { slugin: req.params.payementmethod },
          required: true,
          attributes: ['name', 'region', 'slugin'],
        },
        {
          model: models.payementmethoduser,
          attributes: ['contactRecev', 'fullName', 'payementmethodId', 'countryName'],
          include: [
            {
              model: models.payementmethod,
              where: { slugin: req.params.payementmethod },
              required: true,
              attributes: ['name', 'region', 'slugin'],
            },
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'avatar'] },
          ],
        },
      ],
      order: [['createdAt', 'DESC']],
    });
    res.status(200).json(retraits);
  } catch (error) {
    res.status(500).json({
      error,
    });
  }
};

module.exports = {
  index,
  update,
  show,
  retraitscountryshow,
  retraitspmtshow,
};
