const Validator = require('fastest-validator');
const { makeSluginID } = require('../../helper/utils');
const models = require('../../models');

const index = async (req, res) => {
  try {
    const litigerequests = await models.litigerequest.findAll({
      include: [
        {
          model: models.transaction,
          attributes: ['title', 'slugin', 'userId', 'usertoId', 'createdAt', 'updatedAt'],
          include: [
            { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'] },
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'] },
            { model: models.amount, attributes: ['total', 'totalNoTaxe', 'taxeTransaction', 'currency'] },
            { model: models.category, attributes: ['name', 'slug'] },
          ],
        },
      ],
      order: [
        ['createdAt', 'DESC'],
      ],
    });
    res.status(200).json(litigerequests);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const store = async (req, res) => {
  const { title, content, categorylitigeId } = req.body;
  const { userId } = req.AuthUser;

  const item = {
    title,
    content,
    categorylitigeId,
    userId,
    transactionId: 4,
    slugin: makeSluginID(20),
    statusTraitementLitige: false,
  };

  const schema = {
    title: { type: 'string', min: 3, max: 200 },
    content: { type: 'string', min: 3, max: 65000 },
    statusTraitementLitige: 'boolean',
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    const litigerequest = await models.litigerequest.create(item);
    return res.status(200).json({
      message: 'Data save successfully',
      litigerequest,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    const litigerequest = await models.litigerequest.findOne({
      where: { slugin: req.params.litigerequest },
      include: [
        {
          model: models.transaction,
          attributes: ['title', 'slugin', 'userId', 'usertoId', 'createdAt', 'updatedAt'],
          include: [
            { model: models.user, as: 'userto', attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'] },
            { model: models.user, attributes: ['id', 'firstName', 'lastName', 'slugin', 'avatar'] },
            { model: models.amount, attributes: ['total', 'totalNoTaxe', 'taxeTransaction', 'currency'] },
            { model: models.category, attributes: ['name', 'slug'] },
          ],
        },
        { model: models.user, attributes: ['id', 'firstName', 'slugin', 'avatar'] },
      ],
    });
    res.status(200).json(litigerequest);
  } catch (error) {
    res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
};
