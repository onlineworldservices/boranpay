const Validator = require('fastest-validator');
const bcrypt = require('bcryptjs');
const mySlug = require('slug');
const jwt = require('jsonwebtoken');
const models = require('../../../models');
const { makeSluginID } = require('../../../helper/utils');
const VerifyEmailUserMail = require('../../Mail/Auth/VerifyEmailUserMail');

/** Register fuction */
const register = async (req, res) => {
  const {
    lastName,
    firstName,
    email,
    sex,
    name,
    currencyId,
    countryId,
    password,
    confirmPassword,
  } = req.body;

  try {
    const user = await models.user.findOne({ where: { email } });
    const abilitypermission = await models.abilitypermission.findOne(
      { where: { name } },
    );
    if (user) {
      return res.status(400).json({ message: 'E-mail déjà existant' });
    }

    const item = {
      firstName,
      lastName,
      sex,
      email,
      password,
      confirmPassword,
      role: abilitypermission.name,
    };

    const schema = {
      firstName: { type: 'string', min: 3, max: 200 },
      email: { type: 'email', min: 8, max: 200 },
      password: { type: 'string', min: 3, max: 200 },
      sex: { type: 'enum', values: ['male', 'female', 'other'] },
      confirmPassword: { type: 'equal', field: 'password' },
    };

    const v = new Validator();
    const validationResponse = v.validate(item, schema);

    if (validationResponse !== true) {
      res.status(400).json({ errors: validationResponse });
    }

    if (validationResponse === true) {
      const hashedPsw = await bcrypt.hash(password, 12);
      const userRecev = await models.user.create({
        firstName,
        lastName,
        sex,
        countryId,
        username: `${mySlug(firstName + lastName)}-${makeSluginID(8)}`,
        email,
        password: hashedPsw,
        role: abilitypermission.name,
        providerToken: makeSluginID(35),
        slugin: makeSluginID(20),
      });
      if (userRecev) {
        await models.profile.create({
          slugin: userRecev.slugin,
          userId: userRecev.id,
          currencyId,
          countryId,
        });

        /** Default user */
        await models.ability.create({
          userId: userRecev.id,
          action: abilitypermission.action,
          subject: abilitypermission.subject,
        });

        /** Default affiliation */
        await models.affiliation.create({
          amount: 2,
          currency: 'EUR',
          userId: userRecev.id,
          link: makeSluginID(30),
          slugin: makeSluginID(15),
        });

        VerifyEmailUserMail.verifyemailuserMail(userRecev);
        return res.status(200).json({
          message: 'Inscription faite avec succès',
        });
      }
      return res.status(400).json({ message: 'Données incorrect' });
    }
    return res.status(400).json({
      errors: validationResponse,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error',
      error,
    });
  }
};

const usercheck = async (req, res) => {
  const { password } = req.body;

  try {
    const user = await models.user.findOne({
      where: { slugin: req.params.slugin },
      include: [{
        model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
      }],
    });
    if (!user) {
      res.status(400).json({ message: 'Utilisateur incorrect' });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      res.status(400).json({ message: 'Mot de passe incorrect' });
    }

    res.status(200).json({
      message: 'Identifiant correct',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};
/** Login function */
const login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await models.user.findOne({
      where: { email },
      include: [{
        model: models.ability, as: 'ability', attributes: ['action', 'subject'], required: true,
      }],
    });
    if (!user) {
      return res.status(400).json({ message: 'E-mail ou mot de passe incorrect' });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      return res.status(400).json({ message: 'E-mail ou mot de passe incorrect' });
    }

    const itemUser = {
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
      sex: user.sex,
      email: user.email,
      avatar: user.avatar,
      slugin: user.slugin,
      emailVerifiedStatus: user.emailVerifiedStatus,
      userId: user.id,
      role: user.role,
      ability: user.ability,
    };

    const token = jwt.sign(itemUser, process.env.JWT_KEY, {
      expiresIn: process.env.JWT_EXPIRE,
    });

    return res.status(200).json({ accessToken: token });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};

module.exports = {
  register,
  usercheck,
  login,
};
