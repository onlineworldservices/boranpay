const Validator = require('fastest-validator');
const bcrypt = require('bcryptjs');
const models = require('../../../models');
const ResetPasswordMail = require('../../Mail/Auth/ResetPasswordMail');
const { makeSluginID } = require('../../../helper/utils');

const passwordreset = async (req, res) => {
  const { email } = req.body;

  try {
    const user = await models.user.findOne({ where: { email } });
    if (!user) {
      return res.status(400).json({ message: "Aucun utilisateur n'a été trouvé avec cette adresse mail" });
    }

    const userRecev = await models.passwordreset.create({ email, token: makeSluginID(40) });
    if (!userRecev) {
      return res.status(400).json({ message: 'Une erreur est survenus' });
    }
    ResetPasswordMail.resetpasswordMail(userRecev);
    return res.status(200).json({
      message: 'Email send successfully',
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};
// usertoken
const passwordresetupdate = async (req, res) => {
  const { password, confirmPassword } = req.body;

  const userpasswordreset = await models.passwordreset.findOne({
    where: { token: req.params.usertoken },
  });
  if (!userpasswordreset) { return res.status(400).json({ message: "Ce lien est incorrect ou n'est plus valide" }); }
  const usercheck = await models.user.findOne({ where: { email: userpasswordreset.email } });

  // Ici ce sont les champs a mettre a jour
  const dataUpdate = {
    password,
    confirmPassword,
  };

  const schema = {
    password: { type: 'string', min: 3, max: 200 },
    confirmPassword: { type: 'equal', field: 'password' },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const hashedPsw = await bcrypt.hash(password, 12);
      const userUpdate = await models.user.update(
        { password: hashedPsw },
        { where: { email: usercheck.email } },
      );
      if (userUpdate) {
        await models.passwordreset.destroy({ where: { token: req.params.usertoken } });
      }
      return res.status(200).json({
        message: 'Data updated successfully',
      });
    }
    return res.status(400).json({ message: 'Données incorrect' });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};

const changeresetupdate = async (req, res) => {
  const { oldpassword, password, confirmPassword } = req.body;

  const user = await models.user.findOne({
    where: { slugin: req.params.user },
  });
  if (!user) {
    return res.status(400).json({ message: "Information de l'utilisateur incorrect" });
  }
  const isMatch = await bcrypt.compare(oldpassword, user.password);
  if (!isMatch) {
    return res.status(400).json({ message: "Information de l'utilisateur incorrect" });
  }

  // Ici ce sont les champs a mettre a jour
  const dataUpdate = {
    password,
    confirmPassword,
  };

  const schema = {
    password: { type: 'string', min: 3, max: 200 },
    confirmPassword: { type: 'equal', field: 'password' },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const hashedPsw = await bcrypt.hash(password, 12);
      await models.user.update({ password: hashedPsw }, { where: { slugin: req.params.user } });
      return res.status(200).json({
        message: 'Data updated successfully',
      });
    }
    return res.status(400).json({ message: 'Données incorrect' });
  } catch (error) {
    return res.status(500).json({
      message: 'Identifiant incorrect',
      error,
    });
  }
};

module.exports = {
  passwordreset,
  changeresetupdate,
  passwordresetupdate,
};
