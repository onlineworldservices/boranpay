const models = require('../../../../models');
const rechargemarketService = require('./TransactionmarketServicesTraitement');
const { TAXE_TRANSACTION_MARKETPLACE } = require('../../../../helper/taxetransaction');

/** Ici je check la tranfert de l'argent du conpte de la cagnote vert le copte principal */
const anountTransfertserviceUser = async (
  req,
  res,
  categoryTransation,
  Title,
  userCurrency,
  userIdSend,
  userIdRecev,
  userSend,
  userRecev,
) => {
  /** Ici je commence a verifier si l'utilisateur a un solde positif ou > 0 */
  const amountuserCheck = await models.amountservice.sum('amountService', { where: { userId: userIdSend } });
  const newAmountSave = req.body.total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();

  const getCurrenciesTocheck = async (currencyData) => {
    const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
    const newAmountSendConverted = newAmountConverted;
    const newAmountRecevConverted = newAmountConverted;
    const validateAmountForSave = amountuserCheck > newAmountSendConverted;
    switch (Inputcurrency) {
      case 'CAD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionviremseUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte marketplace bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionviremseUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte marketplace bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'GBP':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionviremseUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte marketplace bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'USD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionviremseUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte marketplace bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XAF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionviremseUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte marketplace bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XOF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionviremseUser(
              req,
              res,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              Title,
              userCurrency,
              currencyData,
              userIdSend,
              userIdRecev,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet dans votre compte marketplace bien vouloir recharger votre compte' });
          }
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      } // accolade ajoutée
    }
  };

  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

const anountTraitementserviceUser = async (
  req,
  res,
  categoryTransation,
  applicationVendeur,
  Title,
  Total,
  userCurrency,
  userSend,
  userRecev,
) => {
  /** Ici je commence a verifier si l'utilisateur a un solde positif ou > 0 */
  const amountuserCheck = applicationVendeur.statusApplication ? (
    await models.amountuser.sum('amountUser', { where: { userId: userSend.id } })
  ) : (
    await models.amountservice.sum('amountServiceBeyer', { where: { applicationId: applicationVendeur.id } })
  );
  const newAmountSave = Total; // Bon savoir! utiliser ligne pour gerre les taxe d'envoie
  /** Ici je veux sauvegarder toutes les transaction en EUR (Euro Union euro) */
  const Inputcurrency = userCurrency;
  const currencies = await models.currency.findAll();

  const getCurrenciesTocheck = async (currencyData) => {
    const newAmountConverted = (newAmountSave / currencyData.currencyNumber);
    const newAmountConvertedTaxe = (newAmountConverted * TAXE_TRANSACTION_MARKETPLACE) / 100;
    const newAmountConvertedTaxeTotal = (newAmountConverted - newAmountConvertedTaxe);
    const newAmountSendConverted = newAmountConverted;
    const newAmountRecevConverted = newAmountConvertedTaxeTotal;
    const validateAmountForSave = amountuserCheck > newAmountSendConverted;
    switch (Inputcurrency) {
      case 'CAD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionmarketUser(
              req,
              res,
              newAmountConverted,
              newAmountConvertedTaxe,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              applicationVendeur,
              Title,
              currencyData,
              userCurrency,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'EUR':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionmarketUser(
              req,
              res,
              newAmountConverted,
              newAmountConvertedTaxe,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              applicationVendeur,
              Title,
              currencyData,
              userCurrency,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'GBP':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionmarketUser(
              req,
              res,
              newAmountConverted,
              newAmountConvertedTaxe,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              applicationVendeur,
              Title,
              currencyData,
              userCurrency,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'USD':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionmarketUser(
              req,
              res,
              newAmountConverted,
              newAmountConvertedTaxe,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              applicationVendeur,
              Title,
              currencyData,
              userCurrency,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XAF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionmarketUser(
              req,
              res,
              newAmountConverted,
              newAmountConvertedTaxe,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              applicationVendeur,
              Title,
              currencyData,
              userCurrency,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      case 'XOF':
        if (Inputcurrency === currencyData.code) {
          if (validateAmountForSave) {
            rechargemarketService.transactionmarketUser(
              req,
              res,
              newAmountConverted,
              newAmountConvertedTaxe,
              newAmountSendConverted,
              newAmountRecevConverted,
              categoryTransation,
              applicationVendeur,
              Title,
              currencyData,
              userCurrency,
              userSend,
              userRecev,
            );
          } else {
            res.status(400).json({ message: 'Solde incomplet bien vouloir recharger votre compte' });
          }
        }
        break;
      default:
      { // accolade ajoutée
        console.log('Aucune action reçue.');
        break;
      }
    }
  };

  for (let i = 0; i < currencies.length; i++) { getCurrenciesTocheck(currencies[i]); }
};

module.exports = {
  anountTraitementserviceUser,
  anountTransfertserviceUser,
};
