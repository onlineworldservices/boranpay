const models = require('../../../../models');
const { makeSluginNumber, makeSluginID, makeParseIp } = require('../../../../helper/utils');
const TransactionTransfertMail = require('../../../Mail/TransactionTransfertMail');

/** Cette fuction gerre le virrement de cagnote vers le compte principal */
const transactionviremseUser = async (
  req,
  res,
  newAmountSendConverted,
  newAmountRecevConverted,
  categoryTransation,
  Title,
  userCurrency,
  currencyData,
  userIdSend,
  userIdRecev,
  userSend,
  userRecev,
) => {
  const itemSend = {
    title: Title,
    userId: userIdSend,
    usertoId: userIdRecev,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    statusApplication: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transactionservice = await models.transactionservice.create(itemSend);
  if (!transactionservice) {
    res.status(400).json({ message: 'Invalid data' });
  }
  const itemRecev = {
    title: Title,
    userId: userIdRecev,
    usertoId: userIdSend,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transactionservice.invoiceNumber,
  };
  const transaction = await models.transaction.create(itemRecev);

  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    currency: userCurrency,
    ip: makeParseIp(req),
    transactionserviceId: transactionservice.id,
  };

  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    currency: userCurrency,
    ip: makeParseIp(req),
    transactionId: transaction.id,
  };

  const amount = await models.amount.create(itemAmount);
  await models.amount.create(itemAmountRecev);

  const newAmountuserSend = -newAmountSendConverted;
  const newAmountuserRecev = +newAmountRecevConverted;
  const itemAmountUserSend = {
    amountService: newAmountuserSend,
    amountId: amount.id,
    userId: userIdSend,
    ip: makeParseIp(req),
  };
  const itemAmountUserRecev = {
    amountUser: newAmountuserRecev,
    amountId: amount.id,
    userId: userIdRecev,
    ip: makeParseIp(req),
  };

  const responseSend = await models.amountservice.create(itemAmountUserSend);
  if (!responseSend) { res.status(400).json({ message: 'Invalid data' }); }
  await models.amountuser.create(itemAmountUserRecev);

  TransactionTransfertMail.transactionUserMail(userSend, userRecev, transaction, itemAmountRecev);
  res.status(200).json({
    message: 'Data save successfully',
    transactionservice,
  });
};

const transactionmarketUser = async (
  req,
  res,
  newAmountConverted,
  newAmountConvertedTaxe,
  newAmountSendConverted,
  newAmountRecevConverted,
  categoryTransation,
  applicationVendeur,
  Title,
  currencyData,
  userCurrency,
  userSend,
  userRecev,
) => {
  const itemSend = {
    title: Title,
    userId: userSend.id,
    usertoId: userRecev.id,
    content: req.body.content,
    categoryId: categoryTransation,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const itemSendTest = {
    title: Title,
    userId: userSend.id,
    usertoId: userRecev.id,
    content: req.body.content,
    statusApplication: applicationVendeur.statusApplication,
    currency: userCurrency,
    statusSend: false,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: makeSluginNumber(12),
  };

  const transaction = applicationVendeur.statusApplication ? (
    await models.transaction.create(itemSend)
  ) : (
    await models.transactionservice.create(itemSendTest)
  );
  if (!transaction) {
    res.status(400).json({ message: 'Invalid data' });
  }
  const itemRecev = {
    title: Title,
    userId: userRecev.id,
    usertoId: userSend.id,
    content: req.body.content,
    applicationId: applicationVendeur.id,
    statusApplication: applicationVendeur.statusApplication,
    currency: userCurrency,
    statusSend: true,
    slugin: makeSluginID(30),
    ip: makeParseIp(req),
    tokenTransaction: makeSluginID(60),
    invoiceNumber: transaction.invoiceNumber,
  };
  const transactionRecev = await models.transactionservice.create(itemRecev);
  const myTaxeTransaction = (newAmountConvertedTaxe * currencyData.currencyNumber);
  const itemAmount = {
    total: (newAmountSendConverted * currencyData.currencyNumber),
    currency: userCurrency,
    transactionId: applicationVendeur.statusApplication ? transaction.id : null,
    ip: makeParseIp(req),
  };

  const itemAmountRecev = {
    total: (newAmountRecevConverted * currencyData.currencyNumber),
    totalNoTaxe: (newAmountConverted * currencyData.currencyNumber),
    taxeTransaction: myTaxeTransaction,
    currency: userCurrency,
    transactionserviceId: transactionRecev.id,
    ip: makeParseIp(req),
  };

  const amount = await models.amount.create(itemAmount);
  await models.amount.create(itemAmountRecev);

  const newAmountuserSend = -newAmountSendConverted;
  const newAmountuserRecev = +newAmountRecevConverted;
  // Cette ligne c'est pour sauvegarder l'envoie
  const itemAmountUserSend = {
    amountUser: newAmountuserSend,
    amountId: amount.id,
    userId: userSend.id,
    ip: makeParseIp(req),
  };
  /** Cette variable ci dessous nous permet de tester l'api */
  const itemAmountUserTestSend = {
    amountServiceBeyer: newAmountuserSend,
    amountServiceNoTaxe: myTaxeTransaction,
    amountId: amount.id,
    ip: makeParseIp(req),
    applicationId: applicationVendeur.id,
  };
  /** Cette ligne c'est pour sauvegarder la reception */
  const itemAmountUserRecev = {
    amountService: newAmountuserRecev,
    amountServiceNoTaxe: myTaxeTransaction,
    amountId: amount.id,
    userId: userRecev.id,
    ip: makeParseIp(req),
    applicationId: applicationVendeur.id,
  };

  const itemAmountUserTestRecev = {
    amountServiceTest: newAmountuserRecev,
    amountId: amount.id,
    ip: makeParseIp(req),
    applicationId: applicationVendeur.id,
  };

  const responseSend = applicationVendeur.statusApplication ? (
    await models.amountuser.create(itemAmountUserSend)
  ) : (
    await models.amountservice.create(itemAmountUserTestSend)
  );
  if (!responseSend) { return res.status(400).json({ message: 'Invalid data' }); }
  /** Information */
  const responseserviceSend = applicationVendeur.statusApplication ? (
    await models.amountservice.create(itemAmountUserRecev)
  ) : (
    await models.amountservice.create(itemAmountUserTestRecev)
  );
  if (!responseserviceSend) { return res.status(400).json({ message: 'Invalid data' }); }
  /** Cette reponse de la transaction serra envoyer a l'utilisateur */
  const responseTransaction = await models.transactionservice.findOne({
    where: { slugin: transactionRecev.slugin },
    include: [
      {
        model: models.user,
        as: 'userto',
        required: true,
        attributes: ['firstName', 'slugin', 'avatar', 'lastName'],
        include: [
          {
            model: models.profile,
            required: true,
            attributes: ['addresse', 'slugin', 'siteInternet'],
            include: [
              { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
              { model: models.country, attributes: ['code', 'name'] },
            ],
          },
        ],
      },
      {
        model: models.user,
        required: true,
        attributes: ['firstName', 'slugin', 'avatar', 'lastName'],
        include: [
          {
            model: models.profile,
            required: true,
            attributes: ['addresse', 'slugin', 'siteInternet'],
            include: [
              { model: models.currency, attributes: ['code', 'symbol', 'currencyNumber'] },
              { model: models.country, attributes: ['code', 'name'] },
            ],
          },
        ],
      },
      { model: models.amount, required: true, attributes: ['total', 'taxeTransaction', 'currency'] },
    ],
  });

  return res.status(200).json(responseTransaction);
};

module.exports = {
  transactionviremseUser,
  transactionmarketUser,
};
