const models = require('../../../../models');

const getOneAffiliationService = async (req, res) => {
  const affiliation = await models.affiliation.findOne({
    where: { slugin: req.params.affiliation },
    include: [
      { model: models.user, attributes: ['id', 'firstName', 'lastName', 'email', 'slugin', 'avatar'] },
    ],
  });
  return res.status(200).json(affiliation);
};

module.exports = { getOneAffiliationService };
