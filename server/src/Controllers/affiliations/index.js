const Validator = require('fastest-validator');
const models = require('../../../models');
const emailSend = require('./mail/send-affiliation-mail');
const { makeSluginID } = require('../../../helper/utils');
const { currencyValidate } = require('../../../helper/validatorvalue');
const { getAllAffiliationService } = require('./services/get-all-affiliations-service');
const { getOneAffiliationService } = require('./services/get-one-affiliations-service');

/** Ici je Get tous les donne de la base de donner **/
const index = async (req, res) => {
  try {
    return await getAllAffiliationService(req, res);
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const store = async (req, res) => {
  const {
    amount, code, email, currency,
  } = req.body;
  const user = await models.user.findOne({ where: { email } });
  if (!user) {
    return res.status(400).json({ message: `This user email ${email} don't exist` });
  }
  const item = {
    amount,
    code,
    currency,
    link: makeSluginID(30),
    slugin: makeSluginID(15),
    userId: user.id,
  };

  const schema = {
    link: { type: 'string', min: 3, max: 200 },
    code: { type: 'string', min: 3, max: 200 },
    currency: { type: 'enum', values: currencyValidate },
  };

  const v = new Validator();
  const validationResponse = v.validate(item, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      const affiliation = await models.affiliation.create(item);
      emailSend.sendAffiliationMail(user, affiliation);
      return res.status(200).json({ affiliation });
    }
    return res.status(400).json({
      errors: validationResponse,
    });
  } catch (error) {
    return res.status(500).json({
      message: 'Something wen wrong',
      error,
    });
  }
};

const show = async (req, res) => {
  try {
    return await getOneAffiliationService(req, res);
  } catch (error) {
    return res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const update = async (req, res) => {
  const { amount, code, currency } = req.body;
  const affiliation = await models.affiliation.findOne(
    { where: { slugin: req.params.affiliation } },
  );
  if (!affiliation) {
    return res.status(400).json({ message: 'This affiliation don\'t exist' });
  }
  const dataUpdate = {
    amount,
    code,
    currency,
  };

  const schema = {
    code: { type: 'string', min: 3, max: 200 },
    currency: { type: 'enum', values: currencyValidate },
  };

  const v = new Validator();
  const validationResponse = v.validate(dataUpdate, schema);

  if (validationResponse !== true) {
    res.status(400).json({ errors: validationResponse });
  }

  try {
    if (validationResponse === true) {
      await affiliation.update(dataUpdate);

      res.status(200).json({
        message: 'Data updated successfully',
      });
    } else {
      res.status(400).json({
        errors: validationResponse,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

const destroy = async (req, res) => {
  try {
    await models.affiliation.destroy({ where: { slugin: req.params.affiliation } });

    res.status(200).json({
      message: 'Data delete successfully',
    });
  } catch (error) {
    res.status(500).json({
      message: 'Something went wrong',
      error,
    });
  }
};

module.exports = {
  index,
  store,
  show,
  update,
  destroy,
};
