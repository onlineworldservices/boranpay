const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class transactionservice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await transactionservice.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transactionservice.belongsTo(models.user, {
        as: 'userto',
        foreignKey: 'usertoId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre l'amount faite par l'utilisateur */
      await transactionservice.hasOne(models.amount, {
        foreignKey: 'transactionserviceId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  transactionservice.init({
    ip: DataTypes.STRING,
    statusSend: DataTypes.BOOLEAN,
    slugin: DataTypes.STRING,
    description: DataTypes.STRING,
    paymentId: DataTypes.STRING,
    tokenTransaction: DataTypes.STRING,
    custom: DataTypes.STRING,
    invoiceNumber: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    statusApplication: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    applicationId: DataTypes.INTEGER.UNSIGNED,
    userId: DataTypes.INTEGER.UNSIGNED,
    usertoId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'transactionservice',
  });
  return transactionservice;
};
