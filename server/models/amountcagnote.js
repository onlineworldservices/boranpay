const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class amountcagnote extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await amountcagnote.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountcagnote.belongsTo(models.amount, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountcagnote.belongsTo(models.cagnote, {
        foreignKey: 'cagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  amountcagnote.init({
    amountCagnote: DataTypes.FLOAT,
    amountCagnoteNoTaxe: DataTypes.FLOAT,
    userId: DataTypes.INTEGER.UNSIGNED,
    ip: DataTypes.STRING,
    cagnoteId: DataTypes.INTEGER.UNSIGNED,
    amountId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'amountcagnote',
  });
  return amountcagnote;
};
