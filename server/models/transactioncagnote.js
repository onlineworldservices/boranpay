const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class transactioncagnote extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await transactioncagnote.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await transactioncagnote.belongsTo(models.category, {
        foreignKey: 'categoryId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
      await transactioncagnote.belongsTo(models.country, {
        foreignKey: 'countryId',
      });
      await transactioncagnote.belongsTo(models.user, {
        as: 'userto',
        foreignKey: 'usertoId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre l'amount faite par l'utilisateur */
      await transactioncagnote.hasOne(models.amount, {
        foreignKey: 'transactioncagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  transactioncagnote.init({
    ip: DataTypes.STRING,
    statusSend: DataTypes.BOOLEAN,
    slugin: DataTypes.STRING,
    paymentId: DataTypes.STRING,
    tokenTransaction: DataTypes.STRING,
    custom: DataTypes.STRING,
    invoiceNumber: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    userId: DataTypes.INTEGER.UNSIGNED,
    usertoId: DataTypes.INTEGER.UNSIGNED,
    countryId: DataTypes.INTEGER.UNSIGNED,
    categoryId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'transactioncagnote',
  });
  return transactioncagnote;
};
