const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class developersecretekey extends Model {
    /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
    static async associate(models) {
      // define association here
      await developersecretekey.belongsTo(models.application, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }

  developersecretekey.init({
    secretKey: DataTypes.TEXT,
    applicationId: DataTypes.INTEGER.UNSIGNED,
    statusApplication: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    sequelize,
    modelName: 'developersecretekey',
  });
  return developersecretekey;
};
