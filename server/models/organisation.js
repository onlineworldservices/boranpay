const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class organisation extends Model {
    /**
       * Helper method for defining associations.
       * This method is not a part of Sequelize lifecycle.
       * The `models/index` file will call this method automatically.
       */
    static async associate() {
      // define association here
    }
  }
  organisation.init({
    name: DataTypes.STRING,
    slugin: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'organisation',
  });
  return organisation;
};
