const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      /** Je recupre les task faite par l'utilisateur */
      await user.hasMany(models.task, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await user.hasMany(models.payementmethoduser, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre les transaction faite par l'utilisateur */
      await user.hasMany(models.transaction, {
        as: 'transactionrecevs',
        constraints: false,
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });

      await user.hasMany(models.transaction, {
        as: 'transactionsends',
        constraints: false,
        foreignKey: 'userId',
        onDelete: 'CASCADE',
      });

      await user.hasMany(models.transaction, {
        as: 'transactionsuserTo',
        constraints: false,
        foreignKey: 'usertoId',
        onDelete: 'CASCADE',
      });

      await user.hasMany(models.transactioncagnote, {
        as: 'transactioncagnoterecevs',
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await user.hasMany(models.transaction, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre les amountuser faite par l'utilisateur */
      await user.hasMany(models.amountuser, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre les amountservice de la donation faite par les utilisateurs */
      await user.hasMany(models.amountdonation, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await user.hasMany(models.amountservice, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await user.hasMany(models.amountcagnote, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre les currency faite par l'utilisateur */
      await user.hasMany(models.currencyuser, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre le profile de l'utilisateur */
      await user.hasOne(models.profile, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre les permissions de l'utilisateur */
      await user.hasMany(models.ability, {
        as: 'ability',
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  user.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    username: DataTypes.STRING,
    birstday: DataTypes.STRING,
    slugin: DataTypes.STRING,
    sex: DataTypes.STRING,
    role: DataTypes.STRING,
    providerToken: DataTypes.TEXT,
    phone: DataTypes.INTEGER.UNSIGNED,
    emailVerifiedStatus: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    statusAdmin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true,
    },
    avatar: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {
    indexes: [
      // Create a unique index on email
      {
        unique: true,
        fields: ['email'],
      },
    ],
    sequelize,
    modelName: 'user',
  });

  return user;
};
