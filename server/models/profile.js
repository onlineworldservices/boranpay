const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await profile.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await profile.belongsTo(models.currency, {
        foreignKey: 'currencyId',
      });

      await profile.belongsTo(models.country, {
        foreignKey: 'countryId',
      });
    }
  }
  profile.init({
    addresse: DataTypes.STRING,
    slugin: DataTypes.STRING,
    countryId: DataTypes.INTEGER.UNSIGNED,
    siteInternet: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
    currencyId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'profile',
  });
  return profile;
};
