const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class like extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await like.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await like.belongsTo(models.cagnote, {
        foreignKey: 'cagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  like.init({
    userId: DataTypes.INTEGER.UNSIGNED,
    cagnoteId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'like',
  });
  return like;
};
