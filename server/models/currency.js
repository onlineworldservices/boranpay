const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class currency extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await currency.hasMany(models.cagnote, {
        foreignKey: 'currencyId',
      });
    }
  }

  currency.init({
    code: DataTypes.STRING,
    name: DataTypes.STRING,
    symbol: DataTypes.STRING,
    currencyNumber: DataTypes.FLOAT,
  }, {
    sequelize,
    modelName: 'currency',
  });
  return currency;
};
