const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class amount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await amount.belongsTo(models.transaction, {
        foreignKey: 'transactionId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amount.belongsTo(models.transactioncagnote, {
        foreignKey: 'transactioncagnoteId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amount.belongsTo(models.transactionservice, {
        foreignKey: 'transactionserviceId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amount.belongsTo(models.transactiondonation, {
        foreignKey: 'transactiondonationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      /** Je recupre l'amountuser faite par l'utilisateur */
      await amount.hasOne(models.amountuser, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amount.hasOne(models.amountcagnote, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amount.hasOne(models.amountdonation, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  amount.init({
    total: DataTypes.FLOAT,
    totalNoTaxe: DataTypes.FLOAT,
    taxeTransaction: DataTypes.FLOAT,
    currency: DataTypes.STRING,
    ip: DataTypes.STRING,
    transactionserviceId: DataTypes.INTEGER.UNSIGNED,
    transactiondonationId: DataTypes.INTEGER.UNSIGNED,
    transactioncagnoteId: DataTypes.INTEGER.UNSIGNED,
    transactionId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'amount',
  });
  return amount;
};
