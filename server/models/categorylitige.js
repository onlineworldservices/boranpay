const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class categorylitige extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate() {
      // define association here
    }
  }

  categorylitige.init({
    name: DataTypes.STRING,
    ip: DataTypes.STRING,
    slugin: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    label: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'categorylitige',
  });
  return categorylitige;
};
