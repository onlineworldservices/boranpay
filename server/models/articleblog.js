const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class articleblog extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await articleblog.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  articleblog.init({
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    slugin: DataTypes.STRING,
    content: DataTypes.TEXT,
    userId: DataTypes.INTEGER.UNSIGNED,
    isDelete: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
  }, {
    sequelize,
    modelName: 'articleblog',
  });
  return articleblog;
};
