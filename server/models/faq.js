const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class faq extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here
      await faq.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  faq.init({
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
    slugin: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    content: DataTypes.TEXT,
  }, {
    sequelize,
    modelName: 'faq',
  });
  return faq;
};
