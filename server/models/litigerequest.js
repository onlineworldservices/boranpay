const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class litigerequest extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await litigerequest.belongsTo(models.transaction, {
        foreignKey: 'transactionId',
        constraints: false,
        // onDelete: 'CASCADE',
        // onUpdate: 'CASCADE',
      });

      await litigerequest.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  litigerequest.init({
    title: DataTypes.STRING,
    ip: DataTypes.STRING,
    slugin: DataTypes.STRING,
    content: DataTypes.TEXT,
    statusTraitementLitige: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0,
    },
    userId: DataTypes.INTEGER.UNSIGNED,
    transactionId: DataTypes.INTEGER.UNSIGNED,
    categorylitigeId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'litigerequest',
  });
  return litigerequest;
};
