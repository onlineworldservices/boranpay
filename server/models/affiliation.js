const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class affiliation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      /** Relation avec l'user du site */
      await affiliation.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  affiliation.init({
    amount: DataTypes.FLOAT,
    link: DataTypes.STRING,
    code: DataTypes.STRING,
    slugin: DataTypes.STRING,
    currency: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'affiliation',
  });
  return affiliation;
};
