const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class cagnote extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await cagnote.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await cagnote.belongsTo(models.currency, {
        foreignKey: 'currencyId',
        onDelete: 'CASCADE',
      });

      await cagnote.hasMany(models.contribute, {
        foreignKey: 'cagnoteId',
        onDelete: 'CASCADE',
      });

      await cagnote.hasMany(models.amountcagnote, {
        foreignKey: 'cagnoteId',
        onUpdate: 'CASCADE',
      });
    }
  }
  cagnote.init({
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    slugin: DataTypes.STRING,
    content: DataTypes.TEXT,
    ip: DataTypes.STRING,
    image: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
    total: DataTypes.FLOAT,
    isClosing: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    isDelete: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
    currencyId: DataTypes.INTEGER,
    countryId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'cagnote',
  });
  return cagnote;
};
