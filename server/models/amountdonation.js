const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class amountdonation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here

      await amountdonation.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountdonation.belongsTo(models.amount, {
        foreignKey: 'amountId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await amountdonation.belongsTo(models.donation, {
        foreignKey: 'donationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }
  amountdonation.init({
    amountDonation: DataTypes.FLOAT,
    amountDonationNoTaxe: DataTypes.FLOAT,
    ip: DataTypes.STRING,
    userId: DataTypes.INTEGER.UNSIGNED,
    donationId: DataTypes.INTEGER.UNSIGNED,
    amountId: DataTypes.INTEGER.UNSIGNED,
  }, {
    sequelize,
    modelName: 'amountdonation',
  });
  return amountdonation;
};
