const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class application extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static async associate(models) {
      // define association here
      await application.belongsTo(models.user, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await application.hasOne(models.applicationsecretekey, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await application.hasOne(models.developeruser, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      // Une application a plusieur amountservice
      await application.hasMany(models.amountservice, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      // Une transactionservice a plusieur amountservice
      await application.hasMany(models.transactionservice, {
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });

      await application.hasMany(models.transactionservice, {
        as: 'transactionserviceOnline',
        foreignKey: 'applicationId',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      });
    }
  }

  application.init({
    name: DataTypes.STRING,
    content: DataTypes.TEXT,
    slugin: DataTypes.STRING,
    ip: DataTypes.STRING,
    clientId: DataTypes.TEXT,
    userId: DataTypes.INTEGER.UNSIGNED,
    statusExistApplication: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    statusApplication: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: false },
  }, {
    sequelize,
    modelName: 'application',
  });
  return application;
};
