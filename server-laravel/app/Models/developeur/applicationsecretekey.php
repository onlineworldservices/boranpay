<?php

namespace App\Models\developeur;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class applicationsecretekey extends Model
{
    use HasFactory;

    public $timestamps = false;
}
