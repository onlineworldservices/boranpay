<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    use HasFactory;

    public $timestamps = false;


    protected static function boot()
    {
        parent::boot();

        static::created(function ($user){
            $taxe = mt_rand(2, 8);
            $Total = mt_rand(10, 100000);
            $TotalNoTax = $Total + (($Total * $taxe) / 100);
            $user->amount()->create([
                'total' => $Total,
                'taxeTransaction' => $taxe,
                'totalNoTaxe' => $TotalNoTax,
                'currency' => currency::inRandomOrder()->first()->code,
            ]);
        });

    }

    public function amount()
    {
        return $this->hasOne(amount::class,'transactionId');
    }
}
