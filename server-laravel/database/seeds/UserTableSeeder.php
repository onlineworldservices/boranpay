<?php

use App\Models\amountcagnote;
use App\Models\amountdonation;
use App\Models\amountuser;
use App\Models\cagnote;
use App\Models\contribute;
use App\Models\donation;
use App\Models\transaction;
use App\Models\transactioncagnote;
use App\Models\transactiondonation;
use App\Models\user;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(user::class, 13)->create();
        factory(transaction::class, 3000)->create();
        factory(amountuser::class, 3000)->create();
        factory(transactiondonation::class, 3000)->create();
        // factory(transactioncagnote::class, 3000)->create();
        // factory(cagnote::class, 100)->create();
        factory(donation::class, 100)->create();
        factory(amountdonation::class, 3000)->create();
        // factory(amountcagnote::class, 3000)->create();
        factory(contribute::class, 3000)->create();

        // Output
        $this->command->info('Test data added.');
    }
}
