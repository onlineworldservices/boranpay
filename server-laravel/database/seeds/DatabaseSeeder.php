<?php

use App\Models\useradmin;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);
        $this->call(CompileTableSeeder::class);
        if (config('app.env') !== 'production') {
            $this->call(UserTableSeeder::class);
        }
    }
}
