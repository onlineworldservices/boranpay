<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsergroupesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usergroupes', function (Blueprint $table) {
            $table->id();
            $table->string('email')->nullable();
            $table->string('slugin')->nullable();
            $table->float('total')->nullable();
            $table->string('currency')->nullable();
            $table->string('fullName')->nullable();
            $table->string('district')->nullable();
            $table->string('ip')->nullable();    
            $table->unsignedBigInteger('phone')->nullable();
            $table->unsignedBigInteger('directoryId')->nullable();
            $table->unsignedBigInteger('groupeId')->nullable();
            $table->unsignedBigInteger('numberCompte')->nullable();
            $table->unsignedBigInteger('countyId')->nullable();
            $table->unsignedBigInteger('userbyId')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usergroupes');
    }
}
