<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrganisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisations', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('slugin')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });

        $organisations = array(
            array('name' => 'BoranPay Cameroun','slugin' => 'KPcbdJ1z143X7Jn3ClEFwHcWojcTeryf-'),
            array('name' => 'BoranPay Gabon','slugin' => 'rajste1z143X7Jn3ClEFwHcWojcT29tUyf-'),
        );

        DB::table('organisations')->insert($organisations);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisations');
    }
}
