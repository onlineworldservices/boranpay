<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePayementmethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payementmethods', function (Blueprint $table) {
            $table->id();
            $table->string('region')->nullable();
            $table->string('typePayement')->nullable();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('type')->nullable(); // phone vs iban, email, contact
            $table->float('taxeTransaction')->nullable();
            $table->string('slugin')->nullable();
            $table->unsignedBigInteger('countryId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payementmethods');
    }
}
