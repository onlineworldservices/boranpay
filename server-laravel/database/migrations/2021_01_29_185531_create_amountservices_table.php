<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmountservicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amountservices', function (Blueprint $table) {
            $table->id();
            $table->float('amountService')->nullable();
            $table->float('amountServiceTest')->nullable();
            $table->float('amountServiceNoTaxe')->nullable();
            $table->float('amountServiceBeyer')->nullable();
            $table->boolean('statusApplication')->default(false);
            $table->unsignedBigInteger('userId')->nullable();
            $table->unsignedBigInteger('amountId')->nullable();
            $table->unsignedBigInteger('serviceId')->nullable();
            $table->unsignedBigInteger('applicationId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amountservices');
    }
}
