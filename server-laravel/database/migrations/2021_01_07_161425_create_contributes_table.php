<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributes', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('slugin')->nullable();
            $table->string('ip')->nullable();
            $table->string('invoiceNumber')->nullable();
            $table->float('total')->nullable();
            $table->float('taxeContribute')->nullable();
            $table->float('amountContribute')->nullable();
            $table->string('currency')->nullable();
            $table->boolean('statusUser')->default(false);
            $table->boolean('statusTotal')->default(false);
            $table->unsignedBigInteger('cagnoteId')->nullable();
            $table->unsignedBigInteger('donationId')->nullable();
            $table->unsignedBigInteger('transactioncagnoteId')->nullable();
            $table->unsignedBigInteger('transactiondonationId')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->longText('content')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributes');
    }
}
