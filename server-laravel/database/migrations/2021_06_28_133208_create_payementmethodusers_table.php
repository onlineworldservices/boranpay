<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayementmethodusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payementmethodusers', function (Blueprint $table) {
            $table->id();
            $table->string('slugin')->nullable();
            $table->string('countryName')->nullable();
            $table->string('contactRecev')->nullable();
            $table->string('fullName')->nullable();
            $table->boolean('isDelete')->default(false);
            $table->unsignedBigInteger('payementmethodId')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payementmethodusers');
    }
}
