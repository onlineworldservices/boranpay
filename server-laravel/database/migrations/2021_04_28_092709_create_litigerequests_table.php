<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLitigerequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('litigerequests', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->boolean('statusTraitementLitige')->default(false);
            $table->string('slugin')->nullable();
            $table->string('ip')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->unsignedBigInteger('categorylitigeId')->nullable();
            $table->unsignedBigInteger('transactionId')->nullable();
            $table->longText('content')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('litigerequests');
    }
}
