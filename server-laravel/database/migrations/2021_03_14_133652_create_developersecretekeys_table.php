<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopersecretekeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developersecretekeys', function (Blueprint $table) {
            $table->id();
            $table->text('secretKey')->nullable();
            $table->boolean('statusApplication')->default(true);
            $table->unsignedBigInteger('applicationId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developersecretekeys');
    }
}
