<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->nullable();
            $table->string('email')->unique();
            $table->string('role')->nullable();
            $table->string('slugin')->nullable();
            $table->longText('providerToken')->nullable();
            $table->unsignedBigInteger('phone')->nullable();
            $table->string('sex')->nullable();
            $table->boolean('statusAdmin')->default(true);
            $table->string('avatar')->nullable();
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->date('birstday')->nullable();
            $table->boolean('emailVerifiedStatus')->default(false);
            $table->string('password');
            $table->rememberToken(); 
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
