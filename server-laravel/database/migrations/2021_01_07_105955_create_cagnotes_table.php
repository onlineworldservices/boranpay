<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCagnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cagnotes', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('slugin')->nullable();
            $table->longText('content')->nullable();
            $table->float('total')->nullable();
            $table->string('image')->nullable();
            $table->string('currency')->nullable();
            $table->boolean('isDelete')->default(false);
            $table->boolean('isClosing')->default(false);
            $table->unsignedBigInteger('userId')->nullable();
            $table->unsignedBigInteger('countryId')->nullable();
            $table->unsignedBigInteger('currencyId')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cagnotes');
    }
}
