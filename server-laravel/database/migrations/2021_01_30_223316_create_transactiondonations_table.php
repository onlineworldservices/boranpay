<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactiondonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactiondonations', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('slugin')->nullable();
            $table->string('ip')->nullable();
            $table->string('custom')->nullable();
            $table->string('invoiceNumber')->nullable();
            $table->string('tokenTransaction')->nullable();
            $table->string('paymentId')->nullable();
            $table->boolean('statusSend')->default(false);
            $table->unsignedBigInteger('donationId')->nullable();
            $table->unsignedBigInteger('categoryId')->nullable();
            $table->unsignedBigInteger('userId')->nullable();
            $table->unsignedBigInteger('usertoId')->nullable();
            $table->longText('content')->nullable();
            $table->date('createdAt')->default(now());
            $table->date('updatedAt')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactiondonations');
    }
}
