<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\amount;
use App\Models\amountuser;
use App\Models\category;
use App\Models\transaction;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(amountuser::class, function (Faker $faker) {

    $users =  [
        'amountUser' => mt_rand(10, 10000),
        'amountId' => amount::inRandomOrder()->first()->id,
        'userId' => user::inRandomOrder()->first()->id,
    ];
    return $users;
});
