<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\amount;
use App\Models\amountcagnote;
use App\Models\amountuser;
use App\Models\cagnote;
use App\Models\category;
use App\Models\transaction;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(amountcagnote::class, function (Faker $faker) {

    $users =  [
        'amountCagnote' => mt_rand(10, 1000),
        'cagnoteId' => mt_rand(1, 100),
        'amountId' => mt_rand(1, 100),
        'userId' => user::inRandomOrder()->first()->id,
    ];
    return $users;
});
