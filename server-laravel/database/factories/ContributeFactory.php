<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\amount;
use App\Models\amountuser;
use App\Models\cagnote;
use App\Models\category;
use App\Models\contribute;
use App\Models\currency;
use App\Models\donation;
use App\Models\transaction;
use App\Models\user;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(contribute::class, function (Faker $faker) {
    $title = $faker->sentence(20);
    $users =  [
        'title' => $title,
        'total' => mt_rand(100, 100000),
        'slugin' => Str::uuid(),
        'invoiceNumber' => mt_rand(100, 1000),
        'content' => $faker->realText(rand(100, 1000)),
        'currency' => currency::inRandomOrder()->first()->code,
        // 'cagnoteId' => mt_rand(1, 100),
        'donationId' => mt_rand(1, 100),
        // 'transactioncagnoteId' => mt_rand(1, 600),
        'transactiondonationId' => mt_rand(1, 600),
        'userId' => user::inRandomOrder()->first()->id,
    ];
    return $users;
});
